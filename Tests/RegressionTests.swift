//
//  InputBufferTests.swift
//  programmers_calculator_coreTests
//
//  Created by Bradley Smith on 12/28/21.
//  Copyright © 2021 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core


public class RegressionTests: XCTestCase {
	public func testRevertCheckpoint() throws {
		let calculator: Calculator = CalculatorCore();

		try calculator.invoke("9");
		calculator.stack().checkPoint()
		try calculator.invoke("ent");
		calculator.stack().checkPoint()
		try calculator.invoke("8");
		calculator.stack().checkPoint()
		try calculator.invoke("*");
		calculator.stack().revertToCheckPoint()
		
		print(calculator.stack().getX())
		print(calculator.stack().currentInputBuffer().progressTextBuffer())
	}
}

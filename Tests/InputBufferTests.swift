
//
//  InputBufferTests.swift
//  programmers_calculator_coreTests
//
//  Created by Bradley Smith on 12/28/21.
//  Copyright © 2021 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core


public class InputBufferTests: XCTestCase {
	let inputBuffer: InputBuffer = InputBufferImpl();

	func testDefaultState() {
		assertBuffer(hasDecimal: false, isNegated: false, hasIntegerPortion: false, hasDecimalPortion: false, integralBuffer: [], operativeIntegralDigits: [],
								 decimalBuffer: [], operativeDecimalDigits: [], numericValue: NSDecimalNumber(0), formattedTextBuffer: "0", progressTextBuffer: "0");
	}
	
	func testSmall() {
		inputBuffer.appendDigit(value: 2)
		assertBuffer(integralBuffer: [2], numericValue: NSDecimalNumber(2));
		inputBuffer.appendDigit(value: 2)
		assertBuffer(integralBuffer: [2,2], numericValue: NSDecimalNumber(22));
	}

	func testSample() {
		inputBuffer.appendDigit(value: 1)
		assertBuffer(integralBuffer: [1], numericValue: NSDecimalNumber(1));
		inputBuffer.appendDigit(value: 0)
		assertBuffer(integralBuffer: [1, 0], numericValue: NSDecimalNumber(10));
		inputBuffer.appendDigit(value: 0)
		assertBuffer(integralBuffer: [1, 0, 0], numericValue: NSDecimalNumber(100));
		inputBuffer.appendDigit(value: 0)
		assertBuffer(integralBuffer: [1, 0, 0, 0], numericValue: NSDecimalNumber(1000));
		inputBuffer.applyDecimal();
		assertBuffer(integralBuffer: [1, 0, 0, 0], numericValue: NSDecimalNumber(1000), formattedTextBuffer: "1,000", progressTextBuffer: "1,000.");
	}

	/* When in the integral portion, leading 0's are ignored. */
	func testLeading0s() {
		inputBuffer.appendDigit(value: 0)
		assertBuffer(integralBuffer: []);
		inputBuffer.appendDigit(value: 0)
		assertBuffer(integralBuffer: []);
		inputBuffer.appendDigit(value: 0)
		assertBuffer(integralBuffer: []);

		inputBuffer.appendDigit(value: 1)
		assertBuffer(integralBuffer: [1]);
		inputBuffer.appendDigit(value: 0)
		assertBuffer(integralBuffer: [1,0]);
		inputBuffer.appendDigit(value: 0)
		assertBuffer(integralBuffer: [1,0,0]);
	}
	
	/* In decimal all 0's count. */
	func testOnlyDecimal() {
		inputBuffer.applyDecimal();
		assertBuffer(hasDecimal: true, integralBuffer: [], decimalBuffer: [], operativeDecimalDigits: [], progressTextBuffer: "0.");
		inputBuffer.appendDigit(value: 0)
		assertBuffer(hasDecimal: true, integralBuffer: [], decimalBuffer: [0], operativeDecimalDigits: [], progressTextBuffer: "0.0");
		inputBuffer.appendDigit(value: 1)
		assertBuffer(hasDecimal: true, integralBuffer: [], decimalBuffer: [0, 1], operativeDecimalDigits: [0, 1], progressTextBuffer: "0.01");
		inputBuffer.appendDigit(value: 4)
		assertBuffer(hasDecimal: true, integralBuffer: [], decimalBuffer: [0, 1, 4], operativeDecimalDigits: [0, 1, 4]);
		inputBuffer.appendDigit(value: 10)
		assertBuffer(hasDecimal: true, integralBuffer: [], decimalBuffer: [0, 1, 4, 10], operativeDecimalDigits: [0, 1, 4, 10]);
		inputBuffer.appendDigit(value: 0)
		assertBuffer(hasDecimal: true, integralBuffer: [], decimalBuffer: [0, 1, 4, 10, 0], operativeDecimalDigits: [0, 1, 4, 10]);
		inputBuffer.appendDigit(value: 0)
		assertBuffer(hasDecimal: true, integralBuffer: [], decimalBuffer: [0, 1, 4, 10, 0, 0], operativeDecimalDigits: [0, 1, 4, 10]);
		inputBuffer.appendDigit(value: 1)
		assertBuffer(hasDecimal: true, integralBuffer: [], decimalBuffer: [0, 1, 4, 10, 0, 0, 1], operativeDecimalDigits: [0, 1, 4, 10, 0, 0, 1]);
	}
	
	func testVariousInputs() {
		assertBuffer(formattedTextBuffer: "0", progressTextBuffer: "0");

		inputBuffer.negativeState(negative: true);
		assertBuffer(formattedTextBuffer: "0", progressTextBuffer: "0");
		inputBuffer.negativeState(negative: false);
		assertBuffer(formattedTextBuffer: "0", progressTextBuffer: "0");

		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "0", progressTextBuffer: "0");
		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "0", progressTextBuffer: "0");
		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "0", progressTextBuffer: "0");
		inputBuffer.appendDigit(value: 1);
		assertBuffer(formattedTextBuffer: "1", progressTextBuffer: "1");

		inputBuffer.negativeState(negative: true);
		assertBuffer(formattedTextBuffer: "-1", progressTextBuffer: "-1");
		inputBuffer.negativeState(negative: false);
		assertBuffer(formattedTextBuffer: "1", progressTextBuffer: "1");

		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "10", progressTextBuffer: "10");
		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "100", progressTextBuffer: "100");
		inputBuffer.applyDecimal();
		assertBuffer(formattedTextBuffer: "100", progressTextBuffer: "100.");
		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "100", progressTextBuffer: "100.0");
		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "100", progressTextBuffer: "100.00");
		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "100", progressTextBuffer: "100.000");
		inputBuffer.appendDigit(value: 2);
		assertBuffer(formattedTextBuffer: "100.0002", progressTextBuffer: "100.0002");
	}
	
	func testToNSDecimal() {
		assertBuffer(numericValue: NSDecimalNumber(0));
		inputBuffer.appendDigit(value: 2);
		assertBuffer(numericValue: NSDecimalNumber(2));
		inputBuffer.negativeState(negative: true);
		assertBuffer(numericValue: NSDecimalNumber(-2));
		inputBuffer.applyDecimal();
		assertBuffer(numericValue: NSDecimalNumber(-2));
		inputBuffer.appendDigit(value: 0);
		inputBuffer.appendDigit(value: 0);
		inputBuffer.appendDigit(value: 0);
		assertBuffer(numericValue: NSDecimalNumber(-2));
		inputBuffer.appendDigit(value: 9);
		assertBuffer(numericValue: NSDecimalNumber(-2.0009));
	}
	
	func testConvertFromBase10Truncates() {
		inputBuffer.appendDigit(value: 2);
		inputBuffer.applyDecimal();
		inputBuffer.appendDigit(value: 2);
		assertBuffer(hasDecimal: true, numericValue: NSDecimalNumber(2.2));
		inputBuffer.applyNumericBase(numericBase: NumericBase.base2)

		assertBuffer(hasDecimal: false, numericValue: NSDecimalNumber(2));
		inputBuffer.appendDigit(value: 0);
		assertBuffer(numericValue: NSDecimalNumber(4));

		inputBuffer.applyNumericBase(numericBase: NumericBase.base16)
		assertBuffer(numericValue: NSDecimalNumber(4));
		inputBuffer.appendDigit(value: 15);
		assertBuffer(numericValue: NSDecimalNumber(79));

		inputBuffer.applyNumericBase(numericBase: NumericBase.base10)
		assertBuffer(hasDecimal: false, numericValue: NSDecimalNumber(79));
		inputBuffer.appendDigit(value: 4);
		assertBuffer(hasDecimal: false, numericValue: NSDecimalNumber(794));
		inputBuffer.applyDecimal();
		inputBuffer.appendDigit(value: 9);
		assertBuffer(hasDecimal: true, numericValue: NSDecimalNumber(794.9));

		inputBuffer.applyNumericBase(numericBase: NumericBase.base8)
		assertBuffer(hasDecimal: false, integralBuffer: [1, 4, 3, 2], numericValue: NSDecimalNumber(794));
		inputBuffer.appendDigit(value: 2);
		assertBuffer(integralBuffer: [1, 4, 3, 2, 2], numericValue: NSDecimalNumber(6354));
	}

	func testBase10Separators() {
		inputBuffer.appendDigit(value: 2);
		assertBuffer(formattedTextBuffer: "2", applySeparators: true);
		inputBuffer.appendDigit(value: 3);
		assertBuffer(formattedTextBuffer: "23", applySeparators: true);
		inputBuffer.appendDigit(value: 4);
		assertBuffer(formattedTextBuffer: "234", applySeparators: true);
		inputBuffer.appendDigit(value: 1);
		assertBuffer(formattedTextBuffer: "2,341", applySeparators: true);
		inputBuffer.negativeState(negative: true);
		assertBuffer(formattedTextBuffer: "-2,341", applySeparators: true);
		inputBuffer.appendDigit(value: 7);
		assertBuffer(formattedTextBuffer: "-23,417", applySeparators: true);
		inputBuffer.appendDigit(value: 8);
		assertBuffer(formattedTextBuffer: "-234,178", applySeparators: true);
		inputBuffer.appendDigit(value: 9);
		assertBuffer(formattedTextBuffer: "-2,341,789", applySeparators: true);

		inputBuffer.applyDecimal();
		inputBuffer.appendDigit(value: 9);
		assertBuffer(formattedTextBuffer: "-2,341,789.9", applySeparators: true);
		inputBuffer.appendDigit(value: 8);
		inputBuffer.appendDigit(value: 7);
		inputBuffer.appendDigit(value: 6);
		assertBuffer(formattedTextBuffer: "-2,341,789.9876", applySeparators: true);
	}

	func testBase2Separators() {
		inputBuffer.applyNumericBase(numericBase: NumericBase.base2)
		inputBuffer.appendDigit(value: 1);
		assertBuffer(formattedTextBuffer: "1", applySeparators: true);
		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "10", applySeparators: true);
		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "100", applySeparators: true);
		inputBuffer.appendDigit(value: 1);
		assertBuffer(formattedTextBuffer: "1001", applySeparators: true);
		inputBuffer.negativeState(negative: true);
		assertBuffer(formattedTextBuffer: "-1001", applySeparators: true);
		inputBuffer.appendDigit(value: 1);
		assertBuffer(formattedTextBuffer: "-10011", applySeparators: true);
		inputBuffer.appendDigit(value: 1);
		assertBuffer(formattedTextBuffer: "-100111", applySeparators: true);
		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "-1001110", applySeparators: true);
		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "-10011100", applySeparators: true);
		inputBuffer.appendDigit(value: 1);
		assertBuffer(formattedTextBuffer: "-1,00111001", applySeparators: true);
	}

	func testBase8Separators() {
		inputBuffer.applyNumericBase(numericBase: NumericBase.base8)
		inputBuffer.appendDigit(value: 7);
		assertBuffer(formattedTextBuffer: "7", applySeparators: true);
		inputBuffer.appendDigit(value: 6);
		assertBuffer(formattedTextBuffer: "76", applySeparators: true);
		inputBuffer.appendDigit(value: 5);
		assertBuffer(formattedTextBuffer: "765", applySeparators: true);
		inputBuffer.appendDigit(value: 4);
		assertBuffer(formattedTextBuffer: "7654", applySeparators: true);
		inputBuffer.negativeState(negative: true);
		assertBuffer(formattedTextBuffer: "-7654", applySeparators: true);
		inputBuffer.appendDigit(value: 3);
		assertBuffer(formattedTextBuffer: "-7,6543", applySeparators: true);
		inputBuffer.appendDigit(value: 2);
		assertBuffer(formattedTextBuffer: "-76,5432", applySeparators: true);
		inputBuffer.appendDigit(value: 1);
		assertBuffer(formattedTextBuffer: "-765,4321", applySeparators: true);
		inputBuffer.appendDigit(value: 0);
		assertBuffer(formattedTextBuffer: "-7654,3210", applySeparators: true);
		inputBuffer.appendDigit(value: 7);
		assertBuffer(formattedTextBuffer: "-7,6543,2107", applySeparators: true);
	}

	func assertBuffer(
		hasDecimal: Bool? = nil,
		isNegated: Bool? = nil,
		hasIntegerPortion: Bool? = nil,
		hasDecimalPortion: Bool? = nil,
		integralBuffer: [UInt8]? = nil,
		operativeIntegralDigits: [UInt8]? = nil,
		decimalBuffer: [UInt8]? = nil,
		operativeDecimalDigits: [UInt8]? = nil,
		numericValue: NSDecimalNumber? = nil,
		formattedTextBuffer: String? = nil,
		progressTextBuffer: String? = nil,
		applySeparators: Bool = true
	) {
		if let _ = hasDecimal {
			XCTAssertTrue(inputBuffer.hasDecimal() == hasDecimal)
		}

		if let _ = isNegated {
			XCTAssertTrue(inputBuffer.isNegated() == isNegated)
		}

		if let _ = hasIntegerPortion {
			XCTAssertTrue(inputBuffer.hasIntegerPortion() == hasIntegerPortion)
		}

		if let _ = hasDecimalPortion {
			XCTAssertTrue(inputBuffer.hasDecimalPortion() == hasDecimalPortion)
		}

		if let buff = integralBuffer {
			XCTAssertTrue(inputBuffer.integralBuffer() == buff, "expected: " + buff.description + " actual: " + inputBuffer.integralBuffer().description)
		}

		if let buff = operativeIntegralDigits {
			XCTAssertTrue(inputBuffer.operativeIntegralDigits() == buff, "expected: " + buff.description + " actual: " + inputBuffer.operativeIntegralDigits().description)
		}

		if let _ = decimalBuffer {
			XCTAssertTrue(inputBuffer.decimalBuffer() == decimalBuffer)
		}

		if let buff = operativeDecimalDigits {
			XCTAssertTrue(inputBuffer.operativeDecimalDigits() == buff, "expected: " + buff.description + " actual: " + inputBuffer.operativeDecimalDigits().description)
		}

		if let nv = numericValue {
			let res = inputBuffer.numericValue();
			XCTAssertTrue(res == nv, "expected: " + nv.stringValue + " actual: " + res.stringValue)
		}

		if let buff = formattedTextBuffer {
			let l = inputBuffer.formattedTextBuffer(applySeparators: applySeparators);
			XCTAssertTrue(l == buff, "expected: " + buff + " actual: " + l)
		}

		if let buff = progressTextBuffer {
			let res = inputBuffer.progressTextBuffer(applySeparators: applySeparators);
			XCTAssertTrue(res == buff, "expected: " + buff + " actual: " + res)
		}
	}
}

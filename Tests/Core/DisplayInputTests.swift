//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class DisplayInputTests: TestSuperclass {
	let testData =
	[
		(input: ["chsx"], result: "0", number: NSDecimalNumber.zero),
		(input: ["."], result: "0.", number: NSDecimalNumber.zero),
		(input: [".", "chsx"], result: "-0.", number: NSDecimalNumber.zero),
		(input: ["0", "."], result: "0.", number: NSDecimalNumber.zero),
		(input: ["0", "0", "0", "0", "0"], result: "0", number: NSDecimalNumber.zero),
		(input: ["0", "0", "0", "0", "0", "."], result: "0.", number: NSDecimalNumber.zero),
		(input: ["0", "0", "0", "0", "0", ".", "0", "0"], result: "0.00", number: NSDecimalNumber.zero),
		(input: ["0", "0", "0", "0", "0", ".", "0", "0", "1"], result: "0.001", number: NSDecimalNumber(string: "0.001")),
		(input: [".", "0", "0", "0", "0", "1"], result: "0.00001", number: NSDecimalNumber(string: "0.00001")),
		(input: [".", "0", "0", "0", "0", "1", "ent"], result: "0.00001", number: NSDecimalNumber(string: "0.00001")),
		(input: [".", "0", "0", "0", "0", "0", "1"], result: "0.000001", number: NSDecimalNumber(string: "0")),
		(input: [".", "0", "0", "0", "0", "0", "1", "ent"], result: "0", number: NSDecimalNumber(string: "0")),
		(input: ["1", "2", "3", "4", ".", "6", "7", "8", "9"], result: "1,234.6789", number: NSDecimalNumber(string: "1234.6789"))
	]

	func testNumericStates() {
		for (index, test) in testData.enumerated()
		{
			calculator.reset();

			for operation in test.input
			{
				do {
					try calculator.invoke(operation);
				} catch _ {
					print(operation);
					XCTFail(operation);
				}
			}

			let _ = test.result;

			let number = calculator.stack().pop();

			XCTAssertEqual(test.number, number, "index: \(index)");
		}
	}
}

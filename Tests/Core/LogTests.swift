//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

/*Add a comment*/
class LogTests: TestSuperclass {
	func testEmptyLog10() {
		do {
			try calculator.invoke("log10x");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(1, calculator.stack().size());
	}

	func test1000Log10() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("log10x");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer: 3), calculator.stack().getX());
	}

	func testEmptyLog2() {
		do {
			try calculator.invoke("log2x");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
	}
	
	func test32Log2() {
		do {
			try calculator.invoke("3");
			try calculator.invoke("2");
			try calculator.invoke("log2x");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer: 5), calculator.stack().getX());
	}
	
	func testEmptyLn() {
		do {
			try calculator.invoke("lnx");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
	}
	
	func test32Ln() {
		do {
			calculator.stack().push(EOperation.E);
			try calculator.invoke("lnx");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber("0.9999999999999997952").decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()), calculator.stack().getX());
	}
}

//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class ThreeLiteralTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("3");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("3"));
	}

	func testMultipleDigits() {
		do {
			try calculator.invoke("3");
			try calculator.invoke("3");
			try calculator.invoke("3");
			try calculator.invoke("3");
			try calculator.invoke("3");
			try calculator.invoke("3");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("333333"));
	}
}

//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class FifteenLiteralTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("hex");
			
			try calculator.invoke("F");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 15));
	}

	func testMultipleDigits() {
		do {
			try calculator.invoke("hex");
			
			try calculator.invoke("F");
			try calculator.invoke("F");
			try calculator.invoke("F");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(digits: [0xf, 0xf, 0xf], radix: 16));
	}

	func testFFEmpty() {
		do {
			try calculator.invoke("hex");
			
			try calculator.invoke("FF");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 255));
	}
}

//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class AngleTests: TestSuperclass {
	func testSupplEmpty() {
		do {
			try calculator.invoke("suppl");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(makeNumber(integer: 180), calculator.stack().pop());
	}

	func testSuppl180() {
		do {
			stack.push(makeNumber(integer: 180));
			try calculator.invoke("suppl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 0), calculator.stack().pop());
	}

	func testSuppl0() {
		do {
			stack.push(makeNumber(integer: 0));
			try calculator.invoke("suppl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 180), calculator.stack().pop());
	}
	
	func testSuppl200() {
		do {
			stack.push(makeNumber(integer: 200));
			try calculator.invoke("suppl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 160), calculator.stack().pop());
	}

	func testSupplNeg200() {
		do {
			stack.push(makeNumber(integer: -200));
			try calculator.invoke("suppl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 160), calculator.stack().pop());
	}

	func testSupplFractional() {
		do {
			stack.push(makeNumber("0.5"));
			try calculator.invoke("suppl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber("179.5"), calculator.stack().pop());
	}

	func testComplEmpty() {
		do {
			try calculator.invoke("compl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 90), calculator.stack().pop());
	}
	
	func testCompl90() {
		do {
			stack.push(makeNumber(integer: 90));
			try calculator.invoke("compl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 0), calculator.stack().pop());
	}
	
	func testCompl0() {
		do {
			stack.push(makeNumber(integer: 0));
			try calculator.invoke("compl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 90), calculator.stack().pop());
	}
	
	func testCompl100() {
		do {
			stack.push(makeNumber(integer: 100));
			try calculator.invoke("compl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 80), calculator.stack().pop());
	}
	
	func testComplNeg100() {
		do {
			stack.push(makeNumber(integer: -100));
			try calculator.invoke("compl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 80), calculator.stack().pop());
	}
	
	func testComplFractional() {
		do {
			stack.push(makeNumber("0.5"));
			try calculator.invoke("compl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber("89.5"), calculator.stack().pop());
	}

	func testComplRadπRef() {
		do {
			try calculator.invoke("rad");
			stack.push(ComplementaryAngleOperation._π_reference);
			try calculator.invoke("compl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 0), calculator.stack().pop());
	}
	
	func testComplRad0() {
		do {
			stack.push(makeNumber(integer: 0));
			try calculator.invoke("rad");
			try calculator.invoke("compl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(ComplementaryAngleOperation._π_reference.decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()), calculator.stack().pop());
	}
	
	func testComplRad4() {
		do {
			stack.push(makeNumber(integer: 2));
			try calculator.invoke("rad");
			try calculator.invoke("compl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber("1.1416"), calculator.stack().pop());
	}
	
	func testComplRadNeg2() {
		do {
			stack.push(makeNumber(integer: -2));
			try calculator.invoke("rad");
			try calculator.invoke("compl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber("1.1416"), calculator.stack().pop());
	}

	func testComplRadFractional() {
		do {
			stack.push(makeNumber("0.5"));
			try calculator.invoke("rad");
			try calculator.invoke("compl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber("1.070796326794896896").decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()), calculator.stack().pop());
	}
	
	func testSupplRadπRef() {
		do {
			try calculator.invoke("rad");
			stack.push(SupplementaryAngleOperation._π_reference);
			try calculator.invoke("suppl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 0), calculator.stack().pop());
	}
	
	func testSupplRad0() {
		do {
			stack.push(makeNumber(integer: 0));
			try calculator.invoke("rad");
			try calculator.invoke("suppl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(SupplementaryAngleOperation._π_reference.decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()), calculator.stack().pop());
	}
	
	func testSupplRad4() {
		do {
			stack.push(makeNumber(integer: 2));
			try calculator.invoke("rad");
			try calculator.invoke("suppl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber("1.141592653589793792").decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()), calculator.stack().pop());
	}
	
	func testSupplRadNeg2() {
		do {
			stack.push(makeNumber(integer: -2));
			try calculator.invoke("rad");
			try calculator.invoke("suppl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber("1.141592653589793792").decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()), calculator.stack().pop());
	}

	func testSupplRadFractional() {
		do {
			stack.push(makeNumber("0.5"));
			try calculator.invoke("rad");
			try calculator.invoke("suppl");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber("2.641592653589793792").decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()), calculator.stack().pop());
	}
}

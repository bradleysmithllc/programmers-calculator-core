//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class TwoLiteralTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("2");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("2"));
	}

	func testMultipleZeros() {
		do {
			try calculator.invoke("2");
			try calculator.invoke("2");
			try calculator.invoke("2");
			try calculator.invoke("2");
			try calculator.invoke("2");
			try calculator.invoke("2");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("222222"));
	}
}

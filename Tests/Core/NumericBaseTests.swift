//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class NumericBaseTests: TestSuperclass {
	func testDefaultBase10() {
		XCTAssertEqual(NumericBase.base10, stack.getNumericBase());
	}

	func testHex() {
		do
		{
			try calculator.invoke("hex");
			XCTAssertEqual(NumericBase.base16, stack.getNumericBase());
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testOct() {
		do
		{
			try calculator.invoke("oct");
			XCTAssertEqual(NumericBase.base8, stack.getNumericBase());
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testDec() {
		do
		{
			// change from base 10 to make sure it changes
			try calculator.invoke("hex");
			try calculator.invoke("dec");
			XCTAssertEqual(NumericBase.base10, stack.getNumericBase());
		}
		catch
		{
			XCTFail();
		}
	}

	func testBin() {
		do
		{
			try calculator.invoke("bin");
			XCTAssertEqual(NumericBase.base2, stack.getNumericBase());
		}
		catch
		{
			XCTFail();
		}
	}
	
	/*I typed in the literal CAFEBABE in hex mode, signed 2-byte.  The result was CAFE,BABE in Y and -4542 in X.*/
	func testEntryBug()
	{
		stack.setNumericBehavior(NumericBehavior.SignedInteger);
		stack.setIntegerWordSize(IntegerWordSize.word_2);

		do
		{
			try calculator.invoke("hex");

			try calculator.invoke("C");
			try calculator.invoke("A");
			try calculator.invoke("F");
			try calculator.invoke("E");
			try calculator.invoke("B");
			try calculator.invoke("A");
			try calculator.invoke("B");
			try calculator.invoke("E");

			try calculator.invoke("ent");

			XCTAssertEqual(-0x4542, stack.getY());
			XCTAssertEqual(-0x4542, stack.getX());
		}
		catch
		{
			XCTFail();
		}
	}
}

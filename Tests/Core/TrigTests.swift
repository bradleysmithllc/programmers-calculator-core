//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class TrigTests: TestSuperclass {
	func testCalls() {
		calculator.stack().setX(NSDecimalNumber.zero);
		
		do {
			try calculator.invoke("sinx");
			try calculator.invoke("cosx");
			try calculator.invoke("secx");
			try calculator.invoke("cscx");
			try calculator.invoke("tanx");
			try calculator.invoke("cotx");
			try calculator.invoke("asinx");
			try calculator.invoke("acosx");
			try calculator.invoke("asecx");
			try calculator.invoke("acscx");
			try calculator.invoke("atanx");
			try calculator.invoke("acotx");
			try calculator.invoke("sinhx");
			try calculator.invoke("coshx");
			try calculator.invoke("sechx");
			try calculator.invoke("cschx");
			try calculator.invoke("tanhx");
			try calculator.invoke("cothx");
			try calculator.invoke("asinhx");
			try calculator.invoke("acoshx");
			try calculator.invoke("asechx");
			try calculator.invoke("acschx");
			try calculator.invoke("atanhx");
			try calculator.invoke("acothx");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
	}
	
	var testDegreesArray = [
		(operation: "sinx", input: NSDecimalNumber.zero, result: NSDecimalNumber.zero),
		(operation: "cosx", input: NSDecimalNumber.zero, result: NSDecimalNumber.one),
		(operation: "tanx", input: NSDecimalNumber.zero, result: NSDecimalNumber.zero),
		(operation: "cotx", input: NSDecimalNumber.zero, result: NSDecimalNumber.notANumber),
		(operation: "secx", input: NSDecimalNumber.zero, result: NSDecimalNumber.one),
		(operation: "cscx", input: NSDecimalNumber.zero, result: NSDecimalNumber.notANumber),
		
		(operation: "sinx", input: NSDecimalNumber(decimal: 90), result: NSDecimalNumber.one),
		(operation: "cosx", input: NSDecimalNumber(decimal: 90), result: NSDecimalNumber.zero),
		(operation: "tanx", input: NSDecimalNumber(decimal: 90), result: NSDecimalNumber(string: "16331239353195368.448")),
		(operation: "cotx", input: NSDecimalNumber(decimal: 90), result: NSDecimalNumber.zero),
		(operation: "secx", input: NSDecimalNumber(decimal: 90), result: NSDecimalNumber(string: "16331239353195368.21474")),
		(operation: "cscx", input: NSDecimalNumber(decimal: 90), result: NSDecimalNumber.one),
		
		(operation: "asinx", input: NSDecimalNumber.zero, result: NSDecimalNumber.zero),
		(operation: "asinx", input: NSDecimalNumber.one, result: NSDecimalNumber(decimal: 90)),

		(operation: "acosx", input: NSDecimalNumber.one, result: NSDecimalNumber.zero),
		(operation: "acosx", input: NSDecimalNumber.zero, result: NSDecimalNumber(decimal: 90)),
		
		(operation: "atanx", input: NSDecimalNumber.zero, result: NSDecimalNumber.zero),
		(operation: "atanx", input: NSDecimalNumber(string: "16331239353195368.448"), result: NSDecimalNumber(decimal: 90))
	];
	
	func testValues() {
		var count: Int = 0;
		
		for fun in testDegreesArray {
			calculator.reset();
			
			let function: String = fun.operation;
			let input: NSDecimalNumber = fun.input;
			let expected: NSDecimalNumber = fun.result;
			
			// push the input onto the stack
			calculator.stack().setX(input);
			
			// call the function
			do {
				try calculator.invoke(function);
			} catch {
				XCTFail();
			}
			
			// compare the results
			let size: Int = calculator.stack().size();
			
            XCTAssertEqual(size, 1, "Too many objects on the stack testIndex: \(String(describing: index)) - size: \(size)");
			
			let actual: NSDecimalNumber = calculator.stack().peek();
			
			XCTAssertEqual(actual, expected, "Results not as expected expected: \(expected) - actual: \(actual) - index: \(count)");
			
			count += 1;
		}
	}

	func testDegreesToRadians()
	{
		stack.push(makeNumber(integer: 180));

		do {
			try calculator.invoke("to_radians");
			try calculator.invoke("pi_constant");
			try calculator.invoke("-");
			try calculator.invoke("absx");

			let x = stack.getX().doubleValue;
			
			XCTAssertTrue(x < 0.00000000001);
		} catch {
			XCTFail();
		}
	}

	func testRadiansToDegrees()
	{
		do {
			try calculator.invoke("rad");
			try calculator.invoke("pi_constant");
			try calculator.invoke("to_degrees");
			try calculator.invoke("1");
			try calculator.invoke("8");
			try calculator.invoke("0");
			try calculator.invoke("-");
			try calculator.invoke("absx");

			let x = stack.getX();
			
			XCTAssertTrue(x.doubleValue < 0.001);
		} catch {
			XCTFail();
		}
	}

	fileprivate static let conversionsData =
	[
		(deg: NSDecimalNumber.zero, rad: NSDecimalNumber.zero, grad: NSDecimalNumber.zero),
		(deg: NSDecimalNumber(value: 90 as Int), rad: NSDecimalNumber(value: Double.pi / 2), grad: NSDecimalNumber(value: 100 as Int)),
		(deg: NSDecimalNumber(value: 180 as Int), rad: NSDecimalNumber(value: Double.pi), grad: NSDecimalNumber(value: 200 as Int)),
		(deg: NSDecimalNumber(value: 270 as Int), rad: NSDecimalNumber(value: (3 * Double.pi) / 2), grad: NSDecimalNumber(value: 300 as Int)),
		(deg: NSDecimalNumber(value: 360 as Int), rad: NSDecimalNumber(value: Double.pi * 2), grad: NSDecimalNumber(value: 400 as Int))
	];

	func testRadGradDeg()
	{
		do
		{
			for data in TrigTests.conversionsData
			{
				// degree perspective
				try calculator.invoke("deg");
				stack.push(data.deg);
				
				try calculator.invoke("to_radians");
				checkDiff(arg1: data.rad, arg2: stack.pop());

				stack.push(data.deg);
				try calculator.invoke("to_gradians");
				checkDiff(arg1: data.grad, arg2: stack.pop());
				
				stack.push(data.deg);
				try calculator.invoke("to_degrees");
				XCTAssertEqual(data.deg, stack.pop());

				// radians perspective
				try calculator.invoke("rad");

				stack.push(data.rad);
				try calculator.invoke("to_radians");
				checkDiff(arg1: data.rad, arg2: stack.pop());
				
				stack.push(data.rad);
				try calculator.invoke("to_gradians");
				checkDiff(arg1: data.grad, arg2: stack.pop());
				
				stack.push(data.rad);
				try calculator.invoke("to_degrees");
				checkDiff(arg1: data.deg, arg2: stack.pop());

				// gradians perspective
				try calculator.invoke("grad");
				
				stack.push(data.grad);
				try calculator.invoke("to_radians");
				checkDiff(arg1: data.rad, arg2: stack.pop());
				
				stack.push(data.grad);
				try calculator.invoke("to_gradians");
				XCTAssertEqual(data.grad, stack.pop());
				
				stack.push(data.grad);
				try calculator.invoke("to_degrees");
				XCTAssertEqual(data.deg, stack.pop());
			}
		}
		catch
		{
			XCTFail();
		}
	}
	
	func checkDiff(arg1: NSDecimalNumber, arg2: NSDecimalNumber, threshold: Double = 0.0001)
	{
		if abs(arg1.doubleValue - arg2.doubleValue) > threshold
		{
			print("abs(\(arg1) - \(arg2)) > \(threshold)");
		}
	}

	fileprivate let unitsTestData = [
		(function: "sinx", output: NSDecimalNumber.zero, radians_input: NSDecimalNumber.zero, degrees_input: NSDecimalNumber.zero, gradians_input: NSDecimalNumber.zero),
		(function: "sinx", output: NSDecimalNumber.one, radians_input: NSDecimalNumber(value: Double.pi / 2 as Double), degrees_input: NSDecimalNumber(value: 90 as Int), gradians_input: NSDecimalNumber(value: 100 as Int)),
		(function: "sinx", output: NSDecimalNumber.zero, radians_input: NSDecimalNumber(value: Double.pi as Double), degrees_input: NSDecimalNumber(value: 180 as Int), gradians_input: NSDecimalNumber(value: 200 as Int))
	];

	func testDifferentUnits()
	{
		do
		{
			for unitsTest in unitsTestData
			{
				stack.push(unitsTest.radians_input);
				try calculator.invoke("rad");
				try calculator.invoke(unitsTest.function);
				checkDiff(arg1: unitsTest.output, arg2: stack.pop());
				
				stack.push(unitsTest.degrees_input);
				try calculator.invoke("deg");
				try calculator.invoke(unitsTest.function);
				checkDiff(arg1: unitsTest.output, arg2: stack.pop());
				
				stack.push(unitsTest.gradians_input);
				try calculator.invoke("grad");
				try calculator.invoke(unitsTest.function);
				checkDiff(arg1: unitsTest.output, arg2: stack.pop());
			}
		}
		catch
		{
			XCTFail();
		}
	}
}

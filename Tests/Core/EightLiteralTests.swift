//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class EightLiteralTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("8");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("8"));
	}

	func testMultipleDigits() {
		do {
			try calculator.invoke("8");
			try calculator.invoke("8");
			try calculator.invoke("8");
			try calculator.invoke("8");
			try calculator.invoke("8");
			try calculator.invoke("8");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("888888"));
	}
}

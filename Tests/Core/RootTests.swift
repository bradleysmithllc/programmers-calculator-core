//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class RootTests: TestSuperclass {
	func testEmptySq() {
		do {
			try calculator.invoke("sqrtx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber("0"), calculator.stack().peek());
	}
	
	func testSqIntegrity() {
		var i: Double = 1.0
		
		repeat
		{
			// prepare the value
			let num = NSDecimalNumber(value: i as Double);
			
			stack.push(num);

			// square, then square root
			do {
				try calculator.invoke("x^2");
				try calculator.invoke("sqrtx");
			} catch is BadOperationState {
			} catch {
				XCTFail();
			}
			
			// must always equal itself
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(num, calculator.stack().pop());

			i += 0.25;
		} while(i <= 100.0)
	}
	
	func testCbIntegrity() {
		var i: Double = 1.0
		
		repeat
		{
			// prepare the value
			let num = NSDecimalNumber(value: i as Double);
			
			stack.push(num);
			
			// square, then square root
			do {
				try calculator.invoke("x^3");
				try calculator.invoke("cbrtx");
			} catch is BadOperationState {
			} catch {
				XCTFail();
			}
			
			// must always equal itself
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(num, calculator.stack().pop());
			
			i += 0.25;
		} while(i <= 100.0)
	}
	
	func testEmptyCb() {
		do {
			try calculator.invoke("cbrtx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber("0"), calculator.stack().peek());
	}
	
	func testEmptyY() {
		do {
			try calculator.invoke("yrtx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(NSDecimalNumber.notANumber, calculator.stack().peek());
	}
}

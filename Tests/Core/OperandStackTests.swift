//
//  CalculatorCoreTests.swift
//  CalculatorCoreTests
//
//  Created by Bradley Smith on 10/5/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class OperandStackTests: TestSuperclass {
	func testExtranneous0s() {
		do
		{
			XCTAssertEqual("0", calculator.stack().currentInputBuffer().progressTextBuffer());

			try calculator.stack().addDigitToNumericEntry(LiteralDigit._0);
			XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().currentInputNumericValue());

			try calculator.stack().addDigitToNumericEntry(LiteralDigit._0);
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._0);
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._0);
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._0);
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._0);
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._0);
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._0);

			XCTAssertEqual("0", calculator.stack().currentInputBuffer().progressTextBuffer());
			XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().currentInputNumericValue());
		}
		catch
		{
			XCTFail();
		}
	}

	func testPushAtEnd() {
		stack.pushAtEnd(NSDecimalNumber.one);
		XCTAssertEqual(stack.pop(), NSDecimalNumber.one);
	}

	func testPushes() {
		let two = NSDecimalNumber(value: 2 as Int);

		stack.push(NSDecimalNumber.one);
		stack.pushAtEnd(two);
		stack.push(two);

		XCTAssertEqual(stack.pop(), two);
		XCTAssertEqual(stack.pop(), NSDecimalNumber.one);
		XCTAssertEqual(stack.pop(), two);
	}

	func testPopEmpty() {
		XCTAssertEqual(stack.pop(), NSDecimalNumber.zero);
	}

	func testGetXEmpty() {
		XCTAssertEqual(stack.getX(), NSDecimalNumber.zero);
	}

	func testGetYEmpty() {
		XCTAssertEqual(stack.getY(), NSDecimalNumber.zero);
	}

	func testPushPop() {
		stack.push(makeNumber("1.0"));
		stack.push(makeNumber("1.5"));
		stack.push(makeNumber("2.0"));

		XCTAssertEqual(stack.pop(), makeNumber("2.0"));
		XCTAssertEqual(stack.pop(), makeNumber("1.5"));
		XCTAssertEqual(stack.pop(), makeNumber("1.0"));
	}

	func testPeek() {
		stack.push(makeNumber("1.0"));
		stack.push(makeNumber("1.5"));
		stack.push(makeNumber("2.0"));

		XCTAssertEqual(stack.peek(), makeNumber("2.0"));
		XCTAssertEqual(stack.peek(), makeNumber("2.0"));
		XCTAssertEqual(stack.peek(), makeNumber("2.0"));
	}

	func testClear() {
		stack.push(makeNumber("1.0"));
		stack.push(makeNumber("1.5"));
		stack.push(makeNumber("2.0"));

		stack.clear();

		XCTAssertEqual(stack.size(), 0);
		XCTAssertEqual(stack.getX(), NSDecimalNumber.zero);
	}

	func testX() {
		XCTAssertEqual(stack.getX(), NSDecimalNumber.zero);

		stack.push(makeNumber(integer: 1));

		XCTAssertEqual(stack.getX(), makeNumber(integer: 1));

		stack.push(makeNumber("1.5"));

		XCTAssertEqual(stack.getX(), makeNumber("1.5"));

		stack.push(makeNumber(integer: 2));
		
		XCTAssertEqual(stack.getX(), makeNumber(integer: 2));
	}
	
	func testY() {
		XCTAssertEqual(stack.getY(), NSDecimalNumber.zero);
		
		stack.push(makeNumber(integer: 1));
		
		XCTAssertEqual(stack.getY(), NSDecimalNumber.zero);
		
		stack.push(makeNumber("1.5"));
		
		XCTAssertEqual(stack.getY(), makeNumber(integer: 1));
		
		stack.push(makeNumber(integer: 2));
		
		XCTAssertEqual(stack.getY(), makeNumber("1.5"));
	}

	func testResetResetsIndex()
	{
		calculator.stack().setIndex(index: NSDecimalNumber.one);
		calculator.reset();
		XCTAssertEqual(calculator.stack().getIndex(), NSDecimalNumber.zero);
	}

	func testNumericEntry()
	{
		do
		{
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._1);
			XCTAssertEqual("1", calculator.stack().currentInputBuffer().progressTextBuffer());

			try calculator.stack().addDigitToNumericEntry(LiteralDigit._8);
			XCTAssertEqual("18", calculator.stack().currentInputBuffer().progressTextBuffer());
			
			try calculator.stack().addDigitToNumericEntry(LiteralDigit.decimalPoint);
			XCTAssertEqual("18.", calculator.stack().currentInputBuffer().progressTextBuffer());
			XCTAssert(calculator.stack().currentInputBufferIncludesDecimal());

			try calculator.stack().addDigitToNumericEntry(LiteralDigit._1);
			XCTAssertEqual("18.1", calculator.stack().currentInputBuffer().progressTextBuffer());
			
			calculator.stack().clearLastDigitInNumericEntry();
			XCTAssertEqual("18.", calculator.stack().currentInputBuffer().progressTextBuffer());
			XCTAssert(calculator.stack().currentInputBufferIncludesDecimal());
			
			calculator.stack().clearLastDigitInNumericEntry();
			XCTAssertEqual("18", calculator.stack().currentInputBuffer().progressTextBuffer());
			XCTAssert(!calculator.stack().currentInputBufferIncludesDecimal());
			
			calculator.stack().clearLastDigitInNumericEntry();
			XCTAssertEqual("1", calculator.stack().currentInputBuffer().progressTextBuffer());
			calculator.stack().clearLastDigitInNumericEntry();
			XCTAssertEqual("0", calculator.stack().currentInputBuffer().progressTextBuffer());
			calculator.stack().clearLastDigitInNumericEntry();
			calculator.stack().clearLastDigitInNumericEntry();
			calculator.stack().clearLastDigitInNumericEntry();
			XCTAssertEqual("0", calculator.stack().currentInputBuffer().progressTextBuffer());
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testNumericEntryNative()
	{
		do
		{
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._1);
			XCTAssertEqual(NSDecimalNumber.one, calculator.stack().currentInputNumericValue());
			
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._8);
			XCTAssertEqual(NSDecimalNumber(value: 18 as Int), calculator.stack().currentInputNumericValue());
			
			try calculator.stack().addDigitToNumericEntry(LiteralDigit.decimalPoint);
			XCTAssertEqual(NSDecimalNumber(value: 18 as Int), calculator.stack().currentInputNumericValue());
			
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._1);
			XCTAssertEqual(NSDecimalNumber(string: "18.1"), calculator.stack().currentInputNumericValue());
			
			calculator.stack().clearLastDigitInNumericEntry();
			XCTAssertEqual(NSDecimalNumber(value: 18 as Int), calculator.stack().currentInputNumericValue());
			
			calculator.stack().clearLastDigitInNumericEntry();
			XCTAssertEqual(NSDecimalNumber(value: 18 as Int), calculator.stack().currentInputNumericValue());
			
			calculator.stack().clearLastDigitInNumericEntry();
			XCTAssertEqual(NSDecimalNumber.one, calculator.stack().currentInputNumericValue());
			calculator.stack().clearLastDigitInNumericEntry();
			XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().currentInputNumericValue());
			calculator.stack().clearLastDigitInNumericEntry();
			calculator.stack().clearLastDigitInNumericEntry();
			calculator.stack().clearLastDigitInNumericEntry();
			XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().currentInputNumericValue());
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testDecimals()
	{
		do
		{
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._1);
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._1);
			try calculator.stack().addDigitToNumericEntry(LiteralDigit.decimalPoint);
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._9);
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._8);
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._7);
			try calculator.stack().addDigitToNumericEntry(LiteralDigit._3);

			XCTAssertEqual(NSDecimalNumber(string: "11.9873"), calculator.stack().currentInputNumericValue());
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testStackXYZT()
	{
		XCTAssertEqual(0, stack.getX());
		XCTAssertEqual(0, stack.getY());
		XCTAssertEqual(0, stack.getZ());
		XCTAssertEqual(0, stack.getT());

		stack.push(NSDecimalNumber.one);

		XCTAssertEqual(1, stack.getX());
		XCTAssertEqual(0, stack.getY());
		XCTAssertEqual(0, stack.getZ());
		XCTAssertEqual(0, stack.getT());
		
		stack.push(NSDecimalNumber(value: 2 as Int32));
		
		XCTAssertEqual(2, stack.getX());
		XCTAssertEqual(1, stack.getY());
		XCTAssertEqual(0, stack.getZ());
		XCTAssertEqual(0, stack.getT());
		
		stack.push(NSDecimalNumber(value: 3 as Int32));
		
		XCTAssertEqual(3, stack.getX());
		XCTAssertEqual(2, stack.getY());
		XCTAssertEqual(1, stack.getZ());
		XCTAssertEqual(0, stack.getT());
		
		stack.push(NSDecimalNumber(value: 4 as Int32));
		
		XCTAssertEqual(4, stack.getX());
		XCTAssertEqual(3, stack.getY());
		XCTAssertEqual(2, stack.getZ());
		XCTAssertEqual(1, stack.getT());
		
		let _ = stack.pop();
		
		XCTAssertEqual(3, stack.getX());
		XCTAssertEqual(2, stack.getY());
		XCTAssertEqual(1, stack.getZ());
		XCTAssertEqual(0, stack.getT());
	
		let _ = stack.pop();
		
		XCTAssertEqual(2, stack.getX());
		XCTAssertEqual(1, stack.getY());
		XCTAssertEqual(0, stack.getZ());
		XCTAssertEqual(0, stack.getT());
		
		let _ = stack.pop();
		
		XCTAssertEqual(1, stack.getX());
		XCTAssertEqual(0, stack.getY());
		XCTAssertEqual(0, stack.getZ());
		XCTAssertEqual(0, stack.getT());
		
		let _ = stack.pop();
		
		XCTAssertEqual(0, stack.getX());
		XCTAssertEqual(0, stack.getY());
		XCTAssertEqual(0, stack.getZ());
		XCTAssertEqual(0, stack.getT());
	}
}

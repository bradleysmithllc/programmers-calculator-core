//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class FactorialTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("x!");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 1));
	}
	
	func testOne() {
		stack.push(makeNumber(integer: 1));
		
		do {
			try calculator.invoke("x!");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 1));
	}
	
	func testTwo() {
		stack.push(makeNumber(integer: 2));
		
		do {
			try calculator.invoke("x!");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 2));
	}
	
	func testFifteen() {
		stack.push(makeNumber(integer: 15));
		
		do {
			try calculator.invoke("x!");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(long: 1307674368000));
	}
	
	func testRidonculous() {
		stack.push(makeNumber(integer: 1500));
		
		do {
			try calculator.invoke("x!");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.notANumber);
	}
	
	func testThree() {
		stack.push(makeNumber(integer: 3));
		
		do {
			try calculator.invoke("x!");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 6));
	}
	
	func testNegative() {
		stack.push(makeNumber(integer: -1));
		
		do {
			try calculator.invoke("x!");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.notANumber);
	}
}

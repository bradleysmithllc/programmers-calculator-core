//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class PercentTests: TestSuperclass {
	func testList() {
		for op in calculator.operationIds()
		{
			print(op);
		}
	}

	func testPercentEmpty() {
		do {
			try calculator.invoke("percent");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.zero);
	}

	func testPercentZero() {
		calculator.stack().setX(makeNumber(integer: 100));
		calculator.stack().push(NSDecimalNumber.zero);

		do {
			try calculator.invoke("percent");
		} catch _ {
			XCTFail();
		}

		XCTAssert(calculator.stack().size() == 2);

		XCTAssertEqual(makeNumber(integer: 100), calculator.stack().getY());
		XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().getX());
	}
	
	func testPercent110() {
		calculator.stack().setX(makeNumber(integer: 100));
		calculator.stack().push(makeNumber(integer: 110));
		
		do {
			try calculator.invoke("percent");
		} catch _ {
			XCTFail();
		}
		
		XCTAssert(calculator.stack().size() == 2);
		
		XCTAssertEqual(makeNumber(integer: 100), calculator.stack().getY());
		XCTAssertEqual(makeNumber(integer: 110), calculator.stack().getX());
	}

	func testDeltaPercentEmpty() {
		do {
			try calculator.invoke("delta-percent");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.notANumber);
	}

	func testDeltaPercentZero() {
		calculator.stack().push(NSDecimalNumber.zero);
		calculator.stack().setX(makeNumber(integer: 100));
		
		do {
			try calculator.invoke("delta-percent");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.notANumber);
	}

	func testDeltaPercentHPExample() {
		calculator.stack().push(makeNumber("15.76"));
		calculator.stack().push(makeNumber("14.12"));
		
		do {
			try calculator.invoke("delta-percent");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), makeNumber("-10.406091370558375634517766497461928934").decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()));
	}

	/** This is not a bug yet.  This is how the 15c operates.
	*/
	func tesDeltaPercentDoesntEatStack() {
		calculator.stack().push(makeNumber("15.76"));
		calculator.stack().push(makeNumber("14.12"));
		
		do {
			try calculator.invoke("delta-percent");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 2);
		XCTAssertEqual(calculator.stack().getX(), makeNumber("-10.406091370558375634517766497461928934"));
		XCTAssertEqual(calculator.stack().getY(), makeNumber("15.76"));
	}

	func testToPercent() {
		do {
			try calculator.invoke("to-percent");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(0, calculator.stack().peek());
	}
	
	func testToPercentActual() {
		calculator.stack().push(makeNumber("15.76"));

		do {
			try calculator.invoke("to-percent");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(makeNumber("0.1576"), calculator.stack().peek());
	}
}

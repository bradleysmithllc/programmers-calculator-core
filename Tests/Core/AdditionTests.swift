//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class AdditionTests: TestSuperclass {
	func testBasic() {
		calculator.stack().push(makeNumber("1.0"));
		calculator.stack().push(makeNumber("0.5"));

		do {
			try calculator.invoke("+");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(makeNumber("1.5"), calculator.stack().pop());
	}
}

//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class CompoundOperationTests: TestSuperclass {
	func testEmpty() {
		let co: CompoundOperation = CompoundOperation(id: "id", mnemonic: "DD", description: "", formula: nil);
		co.addOperation("1");
		co.addOperation("2");
		co.addOperation("3");
		co.addOperation("4");
		co.addOperation("ent");
		calculator.addOperation(co);

		do {
			try calculator.invoke("id");

			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(makeNumber(integer: 1234), stack.getX());
			XCTAssertEqual(makeNumber(integer: 1234), stack.getY());
		} catch _ {
			XCTFail();
		}
	}
}

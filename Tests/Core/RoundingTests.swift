//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class RoundingTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("rndy");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().pop());
	}

	func testTrunc() {
		stack.push(makeNumber("1.01"));
		stack.push(makeNumber(integer: 0));

		do {
			try calculator.invoke("rndy");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(NSDecimalNumber.one, calculator.stack().pop());
	}

	func testTrunc2() {
		stack.push(makeNumber("1.01035"));
		stack.push(makeNumber(integer: 4));

		do {
			try calculator.invoke("rndy");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber("1.0104"), calculator.stack().pop());
	}

	func testTruncGreaterThanDecimals() {
		stack.setDecimals(5);
		stack.push(makeNumber("1.01035667"));
		stack.push(makeNumber(integer: 6));

		do {
			try calculator.invoke("rndy");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber("1.01036"), calculator.stack().pop());
	}
}

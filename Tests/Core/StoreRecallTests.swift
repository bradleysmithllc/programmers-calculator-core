//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class StoreRecallTests: TestSuperclass {
	func testEmptyStore() {
		do {
			try calculator.invoke("store");
		} catch _ {
			XCTFail();
		}

		// memory slot 0 should have the value 0 stored in it
		XCTAssertEqual(calculator.stack().size(), 0);
		XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().memory("0"));
	}

	func testStoreForReal() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("ent");
			try calculator.invoke("2");
			try calculator.invoke("store");
		} catch _ {
			XCTFail();
		}
		
		// memory slot 2 should have the value 1 stored in it, and 1 left on the stack
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(NSDecimalNumber.one, calculator.stack().memory("2"));
	}
	
	func testStoreTruncatesDecimal() {
		do {
			try calculator.invoke("1");
			try calculator.invoke(".");
			try calculator.invoke("1");
			try calculator.invoke("1");
			try calculator.invoke("ent");
			try calculator.invoke("2");
			try calculator.invoke(".");
			try calculator.invoke("1");
			try calculator.invoke("store");
		} catch _ {
			XCTFail();
		}
		
		// memory slot 2 should have the value 1 stored in it, and 1 left on the stack
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(makeNumber("1.11"), calculator.stack().memory("2"));
	}
	
	func testRecallForReal() {
		let _ = stack.assignMemory("1", value: makeNumber(integer: 2));

		do {
			try calculator.invoke("1");
			try calculator.invoke("recall");
		} catch _ {
			XCTFail();
		}
		
		// memory slot 2 should have the value 1 stored in it, and 1 left on the stack
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(makeNumber(integer: 2), calculator.stack().memory("1"));
	}
	
	func testRecallTruncatesDecimal() {
		let _ = stack.assignMemory("1", value: makeNumber("2.765"));
		
		do {
			try calculator.invoke("1");
			try calculator.invoke(".");
			try calculator.invoke("5");
			try calculator.invoke("4");
			try calculator.invoke("6");
			try calculator.invoke("recall");
		} catch _ {
			XCTFail();
		}
		
		// memory slot 2 should have the value 1 stored in it, and 1 left on the stack
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(makeNumber("2.765"), calculator.stack().memory("1"));
	}
	
	func testEmptyRecall() {
		do {
			try calculator.invoke("recall");
		} catch _ {
			XCTFail();
		}
		
		// 0 will be pulled from empty memory slot 0
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().getX());
	}
	
	func testStorePlusEmpty() {
		do {
			try calculator.invoke("store_plus");
		} catch _ {
			XCTFail();
		}
		
		// 0 will be pulled from empty memory slot 0
		XCTAssertEqual(calculator.stack().size(), 0);
	}
	
	func testStoreMinusEmpty() {
		do {
			try calculator.invoke("store_minus");
		} catch _ {
			XCTFail();
		}
		
		// 0 will be pulled from empty memory slot 0
		XCTAssertEqual(calculator.stack().size(), 0);
	}
	
	func testStoreMinusScript() {
		stack.push(makeNumber("2.765"));
		stack.push(makeNumber(integer: 1));

		do {
			try calculator.invoke("store_minus");
			
			XCTAssertEqual(makeNumber("-2.765"), stack.memory("1"));

			stack.push(makeNumber(integer: 1));
			try calculator.invoke("store_plus");
			
			XCTAssertEqual(NSDecimalNumber.zero, stack.memory("1"));

			stack.push(makeNumber("6"));
			stack.push(makeNumber(integer: 1));
			try calculator.invoke("store");

			XCTAssertEqual(makeNumber(integer: 6), stack.memory("1"));
		} catch _ {
			XCTFail();
		}
	}
}

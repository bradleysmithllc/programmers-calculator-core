//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class XCubedTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("x^3");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}

	func testZero() {
		calculator.stack().push(NSDecimalNumber.zero);

		do {
			try calculator.invoke("x^3");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.zero);
	}

	func testBasic() {
		calculator.stack().push(makeNumber("4.0"));

		do {
			try calculator.invoke("x^3");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().peek(), makeNumber("64.0"));
	}

	func testDecimal() {
		calculator.stack().push(makeNumber("4.4"));

		do {
			try calculator.invoke("x^3");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().peek(), makeNumber("85.184"));
	}
}

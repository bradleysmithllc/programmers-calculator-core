//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class LCMTests: TestSuperclass {
	fileprivate static let primes: [(inputs: [NSDecimalNumber], result: NSDecimalNumber)] = [
		(inputs: [], result: 0),
		(inputs: [0], result: 0),
		(inputs: [12, 21, 33], result: 924),
		(inputs: [0, 12, 21, 33, 75, 64, 32], result: 0),
		(inputs: [3, 4], result: 12),
		(inputs: [1, 2], result: 2),
		(inputs: [1], result: 1),
		(inputs: [NSDecimalNumber.notANumber], result: NSDecimalNumber.notANumber)
		//(inputs: [1001], result: NSDecimalNumber.notANumber())
	];
	
	func testEmpty() {
		do {
			try calculator.invoke("lcm");
			
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(0, stack.pop());

		} catch _ {
			XCTFail();
		}
	}
	
	func testKnowns() {
		do {
			for (index, testValue) in LCMTests.primes.enumerated()
			{
				// push values onto the stack
				for input in testValue.inputs
				{
					stack.push(input);
				}

				try calculator.invoke("lcm");

				XCTAssertEqual(1, stack.size(), "\(index)");
				XCTAssertEqual(testValue.result, stack.pop(), "\(index)");
			}
		} catch _ {
			XCTFail();
		}
	}

	func testFactors()
	{
		let p = LeastCommonMultipleOperation.powerPrimes([2, 2, 2, 5, 3, 5]);

		XCTAssertEqual(3, p.count);
		XCTAssertEqual(3, p[2]);
		XCTAssertEqual(1, p[3]);
		XCTAssertEqual(2, p[5]);
	}
	
	func testPrimeFactors()
	{
		var pp = [NSDecimalNumber: NSDecimalNumber]();

		LeastCommonMultipleOperation.comparePrimeFactors([2, 2, 2, 5, 3, 5], primeFactors: &pp);

		XCTAssertEqual(3, pp.count);
		XCTAssertEqual(3, pp[2]);
		XCTAssertEqual(1, pp[3]);
		XCTAssertEqual(2, pp[5]);

		LeastCommonMultipleOperation.comparePrimeFactors([2, 2, 5, 5, 5], primeFactors: &pp);
		
		XCTAssertEqual(3, pp.count);
		XCTAssertEqual(3, pp[2]);
		XCTAssertEqual(1, pp[3]);
		XCTAssertEqual(3, pp[5]);
	}
}

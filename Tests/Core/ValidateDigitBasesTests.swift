//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class ValidateDigitBasesLiteralTests: TestSuperclass {
	fileprivate func assertValid(_ ops: [String])
	{
		for op in ops
		{
			XCTAssertTrue(calculator.operation(op)!.validateStack(), op);
		}
	}

	fileprivate func assertInvalid(_ ops: [String])
	{
		for op in ops
		{
			let operation = calculator.operation(op);
			XCTAssertFalse(operation!.validateStack(), op);
		}
	}

	func testHexBase2() {
		do {
			try calculator.invoke("bin");

			assertValid(["0", "1"]);
			assertInvalid([".", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"]);
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
	}

	func testHexBase8() {
		do {
			try calculator.invoke("oct");
			
			assertValid(["0", "1", "2", "3", "4", "5", "6", "7"]);
			assertInvalid([".", "8", "9", "A", "B", "C", "D", "E", "F"]);
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
	}
	
	func testHexBase10() {
		do {
			try calculator.invoke("dec");
			
			assertValid([".", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]);
			assertInvalid(["A", "B", "C", "D", "E", "F"]);
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
	}
	
	func testHexBase16() {
		do {
			try calculator.invoke("hex");
			
			assertValid(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"]);
			assertInvalid(["."]);
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
	}
}

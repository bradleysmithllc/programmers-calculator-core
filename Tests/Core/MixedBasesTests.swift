//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class MixedBasesLiteralTests: TestSuperclass {
	func testHexBase2() {
		do {
			try calculator.invoke("bin");
			try calculator.invoke("C");
			try calculator.invoke("A");
			try calculator.invoke("F");
			try calculator.invoke("E");
			try calculator.invoke("B");
			try calculator.invoke("A");
			try calculator.invoke("B");
			try calculator.invoke("E");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(digits: [0xc, 0xa, 0xf, 0xe, 0xb, 0xa, 0xb, 0xe], radix: 2));
	}

	func testHexBase8() {
		do {
			try calculator.invoke("oct");
			try calculator.invoke("C");
			try calculator.invoke("A");
			try calculator.invoke("F");
			try calculator.invoke("E");
			try calculator.invoke("B");
			try calculator.invoke("A");
			try calculator.invoke("B");
			try calculator.invoke("E");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(digits: [0xc, 0xa, 0xf, 0xe, 0xb, 0xa, 0xb, 0xe], radix: 8));
	}

	func testHexBase10() {
		do {
			try calculator.invoke("C");
			try calculator.invoke("A");
			try calculator.invoke("F");
			try calculator.invoke("E");
			try calculator.invoke("B");
			try calculator.invoke("A");
			try calculator.invoke("B");
			try calculator.invoke("E");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(digits: [0xc, 0xa, 0xf, 0xe, 0xb, 0xa, 0xb, 0xe], radix: 10));
	}

	func testHexBase16() {
		do {
			try calculator.invoke("hex");
			try calculator.invoke("C");
			try calculator.invoke("A");
			try calculator.invoke("F");
			try calculator.invoke("E");
			try calculator.invoke("B");
			try calculator.invoke("A");
			try calculator.invoke("B");
			try calculator.invoke("E");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(digits: [0xc, 0xa, 0xf, 0xe, 0xb, 0xa, 0xb, 0xe], radix: 16));
	}
	
	func testBinBase16() {
		do {
			try calculator.invoke("hex");
			try calculator.invoke("1");
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("1");
			try calculator.invoke("1");
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("8");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(digits: [0x1, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0, 0x8], radix: 16));
	}
}

//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class XYTranspositionTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("x<->y");
		} catch _ {
			XCTFail();
		}

		XCTAssert(calculator.stack().size() == 2);
	}

	func testBasic() {
		calculator.stack().setX(makeNumber("1.0"));
		calculator.stack().push(makeNumber("0.5"));

		do {
			try calculator.invoke("x<->y");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 2);

		XCTAssertEqual(calculator.stack().pop(), makeNumber("1.0"));
		XCTAssertEqual(calculator.stack().pop(), makeNumber("0.5"));
	}

	func testYNil() {
		calculator.stack().push(makeNumber("1.0"));

		do {
			try calculator.invoke("x<->y");
		} catch _ {
			XCTFail();
		}

		XCTAssert(calculator.stack().size() == 2);

		XCTAssertEqual(calculator.stack().pop(), makeNumber("0.0"));
	}
}

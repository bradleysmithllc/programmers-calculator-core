//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class DropXTests: TestSuperclass {
	func testValidAlways() {
		do {
			try calculator.invoke("dropx");
			try calculator.invoke("dropx");
			try calculator.invoke("dropx");
			try calculator.invoke("dropx");
		} catch _ {
			XCTFail();
		}
	}

	func testVarious() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("ent");
			try calculator.invoke("2");

			XCTAssertEqual(calculator.stack().size(), 2);
			try calculator.invoke("dropx");
			XCTAssertEqual(calculator.stack().size(), 1);
		} catch _ {
			XCTFail();
		}
	}
}

//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class ReduceFractionTests: TestSuperclass {
	func testZero() {
		let result = ReduceFractionOperation.factor(NSDecimalNumber.zero, mathHub: mathHub)

		XCTAssertEqual(0, result.factors.count);
		XCTAssertEqual(0, result.remainder);
	}

	func testOne() {
		let result = ReduceFractionOperation.factor(makeNumber(integer: 1), mathHub: mathHub)
		
		XCTAssertEqual(0, result.factors.count);
		XCTAssertEqual(1, result.remainder);
	}

	func testPrime() {
		let num = makeNumber(integer: 2);
		let result = ReduceFractionOperation.factor(num, mathHub: mathHub)
		
		XCTAssertEqual(1, result.factors.count);
		XCTAssertEqual(2, result.factors[0]);
		XCTAssertEqual(0, result.remainder);
	}
	
	func testComplex() {
		let num = makeNumber(integer: Int(Int32.max - 1));
		let result = ReduceFractionOperation.factor(num, mathHub: mathHub)

		XCTAssertEqual(8, result.factors.count);
		XCTAssertEqual(2, result.factors[0]);
		XCTAssertEqual(3, result.factors[1]);
		XCTAssertEqual(3, result.factors[2]);
		XCTAssertEqual(7, result.factors[3]);
		XCTAssertEqual(11, result.factors[4]);
		XCTAssertEqual(31, result.factors[5]);
		XCTAssertEqual(151, result.factors[6]);
		XCTAssertEqual(331, result.factors[7]);
		XCTAssertEqual(0, result.remainder);
	}
	
	func testBigPrime() {
		let num = makeNumber(integer: Int(Int32.max));
		let result = ReduceFractionOperation.factor(num, mathHub: mathHub)
		
		XCTAssertEqual(0, result.factors.count);
		XCTAssertEqual(num, result.remainder);
	}
	
	func testRepeating() {
		let num = makeNumber(integer: 44100);
		let result = ReduceFractionOperation.factor(num, mathHub: mathHub)
		
		XCTAssertEqual(8, result.factors.count);
		XCTAssertEqual(2, result.factors[0]);
		XCTAssertEqual(2, result.factors[1]);
		XCTAssertEqual(3, result.factors[2]);
		XCTAssertEqual(3, result.factors[3]);
		XCTAssertEqual(5, result.factors[4]);
		XCTAssertEqual(5, result.factors[5]);
		XCTAssertEqual(7, result.factors[6]);
		XCTAssertEqual(7, result.factors[7]);
		XCTAssertEqual(0, result.remainder);
	}
}

class ReduceFractionCalcTests: TestSuperclass {
	func testZeroDenom() {
		do
		{
			try calculator.invoke("reduce");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(NSDecimalNumber.notANumber, stack.pop());
			XCTAssertEqual(NSDecimalNumber.notANumber, stack.pop());
		}
		catch
		{
			XCTFail();
		}
	}

	func testZeroNum() {
		stack.push(NSDecimalNumber.zero);
		stack.push(NSDecimalNumber.one);

		do
		{
			try calculator.invoke("reduce");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(NSDecimalNumber.zero, stack.pop());
			XCTAssertEqual(NSDecimalNumber.zero, stack.pop());
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testNumModDenom() {
		stack.push(NSDecimalNumber(value: 2 as Int));
		stack.push(NSDecimalNumber(value: 4 as Int));
		
		do
		{
			try calculator.invoke("reduce");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(NSDecimalNumber(value: 2 as Int), stack.pop());
			XCTAssertEqual(NSDecimalNumber.one, stack.pop());
		}
		catch
		{
			XCTFail();
		}
	}

	func testDenomModNum() {
		stack.push(NSDecimalNumber(value: 4 as Int));
		stack.push(NSDecimalNumber(value: 2 as Int));
		
		do
		{
			try calculator.invoke("reduce");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(NSDecimalNumber.one, stack.pop());
			XCTAssertEqual(NSDecimalNumber(value: 2 as Int), stack.pop());
		}
		catch
		{
			XCTFail();
		}
	}

	func testBigPrime() {
		let num = makeNumber(integer: Int(Int32.max));

		stack.push(num);
		stack.push(NSDecimalNumber.one);
		
		do
		{
			try calculator.invoke("reduce");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(1, stack.pop());
			XCTAssertEqual(num, stack.pop());
		}
		catch
		{
			XCTFail();
		}
	}
	
	/*Various test cases to ensure that these are handled*/
	func testCase1() {
		stack.push(makeNumber(integer: 11));
		stack.push(makeNumber(integer: 22));
		
		do
		{
			try calculator.invoke("reduce");

			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(2, stack.pop());
			XCTAssertEqual(1, stack.pop());
		}
		catch
		{
			XCTFail();
		}
	}

	func testCase2() {
		stack.push(makeNumber(integer: 11));
		stack.push(makeNumber(integer: 11));
		
		do
		{
			try calculator.invoke("reduce");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(1, stack.pop());
			XCTAssertEqual(1, stack.pop());
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testCase3() {
		stack.push(makeNumber(integer: 1576));
		stack.push(makeNumber(integer: 1576));
		
		do
		{
			try calculator.invoke("reduce");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(1, stack.pop());
			XCTAssertEqual(1, stack.pop());
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testCase4() {
		stack.push(makeNumber(integer: Int(Int32.max)));
		stack.push(makeNumber(integer: Int(Int32.max)));
		
		do
		{
			try calculator.invoke("reduce");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(1, stack.pop());
			XCTAssertEqual(1, stack.pop());
		}
		catch
		{
			XCTFail();
		}
	}

	func testCase5() {
		stack.push(makeNumber(integer: 1003));
		stack.push(makeNumber(integer: 1003));
		
		do
		{
			try calculator.invoke("reduce");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(1, stack.pop());
			XCTAssertEqual(1, stack.pop());
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testCase6() {
		stack.push(makeNumber(integer: 1003));
		stack.push(makeNumber(integer: 2006));
		
		do
		{
			try calculator.invoke("reduce");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(2, stack.pop());
			XCTAssertEqual(1, stack.pop());
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testCase7() {
		stack.push(makeNumber(integer: 2006));
		stack.push(makeNumber(integer: 1003));
		
		do
		{
			try calculator.invoke("reduce");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(1, stack.pop());
			XCTAssertEqual(2, stack.pop());
		}
		catch
		{
			XCTFail();
		}
	}
}

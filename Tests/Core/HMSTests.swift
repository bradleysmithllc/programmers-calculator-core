//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class HMSTests: TestSuperclass {
	func testEmptyHour() {
		do {
			try calculator.invoke("to_hour");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().pop());
	}

	func testEmptyHms() {
		do {
			try calculator.invoke("to_hms");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().pop());
	}
	
	func testStackAffectHms() {
		do {
			stack.push(NSDecimalNumber.one);
			stack.push(NSDecimalNumber.one);
			stack.push(NSDecimalNumber.one);

			try calculator.invoke("to_hms");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(3, calculator.stack().size());
	}
	
	func testStackAffectHour() {
		do {
			stack.push(NSDecimalNumber.one);
			stack.push(NSDecimalNumber.one);
			stack.push(NSDecimalNumber.one);
			
			try calculator.invoke("to_hour");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(3, calculator.stack().size());
	}
	
	let samples = [
		["37.4217", "37.70472"],
		["89.1115", "89.1875"],
		["12.15", "12.25"],
		["33.3", "33.5"],
		["71.003", "71.00833"],
		["42.2453", "42.41472"],
		["38.4225", "38.70694"],
		["29.303", "29.50833"],
		["0.4949", "0.83028"],

		["75.15", "75.25"],
		["43.223", "43.375"],
		["9.3345", "9.5625"],
		["33.57522", "33.9645"],
		["21.3", "21.5"],
		["59.472112", "59.78919"],
		["65.110096", "65.18361"],
	];

	func testSamples() {
		do {
			for sample in samples
			{
				//print("Input \(sample[0])");

				stack.push(makeNumber(sample[0]));
				try calculator.invoke("to_hour");

				let result = calculator.stack().peek();//.decimalNumberRoundedToDigits(4)
			
				//print("Expected: \(sample[1]) Actual: \(result) Stack: \(stack.size())");

				XCTAssertEqual(1, calculator.stack().size());
				XCTAssertEqual(makeNumber(sample[1]), result);

				try calculator.invoke("to_hms");

				let rresult = calculator.stack().pop();
				
				//print("Expected: \(sample[0]) Actual: \(rresult) Stack: \(stack.size())");
				XCTAssertEqual(0, calculator.stack().size());
				XCTAssertTrue(
						mathHub.subtract(left: makeNumber(sample[0]), right: rresult).decimalNumberWithAbsoluteValue().decimalValue < 0.00001
				);
			}
		} catch _ {
			XCTFail();
		}
	}

	override func requiresConfiguration() -> Bool
	{
		return true;
	}
}

//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class PercentXYTests: TestSuperclass {
	func testPercentEmpty() {
		do {
			try calculator.invoke("xpercenty");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.notANumber);
	}

	func testPercent1() {
		stack.push(20);
		stack.push(10);

		do {
			try calculator.invoke("xpercenty");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), 50);
	}

	func testPercent200() {
		stack.push(10);
		stack.push(20);
		
		do {
			try calculator.invoke("xpercenty");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), 200);
	}

	func testPercent0() {
		stack.push(20);
		stack.push(0);
		
		do {
			try calculator.invoke("xpercenty");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), 0);
	}

	func testPercentNaN() {
		stack.push(0);
		stack.push(20);
		
		do {
			try calculator.invoke("xpercenty");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.notANumber);
	}

	func testPercentNegativeX() {
		stack.push(20);
		stack.push(-5);
		
		do {
			try calculator.invoke("xpercenty");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), -25);
	}

	func testPercentNegativeY() {
		stack.push(-20);
		stack.push(5);
		
		do {
			try calculator.invoke("xpercenty");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), -25);
	}

	func testPercentNegativeXY() {
		stack.push(-20);
		stack.push(-5);
		
		do {
			try calculator.invoke("xpercenty");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), 25);
	}
}

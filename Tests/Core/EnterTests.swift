//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class EnterTests: TestSuperclass {
	/* If X is NaN, enter should push 0.0 rather than pushing NaN up the stack. */
	func testNaN() {
		// first generate NaN with 1/0
		do {
			try calculator.invoke("1");
			try calculator.invoke("ent");
			try calculator.invoke("0");
			try calculator.invoke("/");
		} catch _ {
			XCTFail();
		}

		// verify NaN is on the stack
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.notANumber);

		// press enter.
		do {
			try calculator.invoke("ent");
		} catch _ {
			XCTFail();
		}

		// now the stack should have one entry of 0.0
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.zero);
	}

	/* If X is a message, enter should push 0.0 rather than pushing the message up the stack. */
	func testMessage() {
		// first generate NaN with 1/0
		do {
			try calculator.invoke("fact_quad");
		} catch _ {
			XCTFail();
		}

		// verify NaN is on the stack
		XCTAssertTrue(calculator.stack().getXElement().isMessage);

		// press enter.
		do {
			try calculator.invoke("ent");
		} catch _ {
			XCTFail();
		}

		// now the stack should have one entry of 0.0
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.zero);
	}
	
	func testMessages() {
		// first generate NaN with 1/0
		do {
			try calculator.invoke("pi_constant");
			try calculator.invoke("fact_quad");
		} catch _ {
			XCTFail();
		}

		// verify a message is on the stack
		XCTAssertTrue(calculator.stack().getXElement().isMessage);

		// press pi.
		do {
			// add some NaN
			stack.push(NSDecimalNumber.notANumber);
			stack.push(NSDecimalNumber.notANumber);
			stack.push(NSDecimalNumber.notANumber);

			try calculator.invoke("ent");
		} catch _ {
			XCTFail();
		}

		// now the stack should have one entry of pi
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.zero);
	}
	
	func testEmpty() {
		do {
			try calculator.invoke("ent");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 0);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.zero);
	}

	func testBasic() {
		calculator.stack().setX(makeNumber("1.0"));
		calculator.stack().push(makeNumber("0.5"));

		do {
			try calculator.invoke("ent");
		} catch _ {
			XCTFail();
		}

		XCTAssert(calculator.stack().size() == 3);

		XCTAssert(calculator.stack().pop() == makeNumber("0.5"));
		XCTAssert(calculator.stack().pop() == makeNumber("0.5"));
		XCTAssert(calculator.stack().pop() == makeNumber("1.0"));
	}

	func testLittleMoreComplex() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("ent");
			try calculator.invoke("2");

			XCTAssertEqual(calculator.stack().size(), 2);

			XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: 2));
			XCTAssertEqual(calculator.stack().getY(), makeNumber(integer: 1));
		} catch _ {
			XCTFail();
		}
	}
}

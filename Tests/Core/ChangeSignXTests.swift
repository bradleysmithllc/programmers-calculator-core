//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class ChangeSignXTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("chsx");
		} catch _ {
			XCTFail();
		}

		XCTAssert(calculator.stack().size() == 1);
	}

	func testWithData() {
		do {
			try calculator.invoke("5");

			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 5));

			try calculator.invoke("chsx");

			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: -5));

			try calculator.invoke("chsx");

			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 5));
		} catch _ {
			XCTFail();
		}
	}
	
	func testWithSeparartors() {
		do {
			try calculator.invoke("5");

			XCTAssertEqual(stack.currentInputBuffer().progressTextBuffer(), "5");
			XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 5));
			
			try calculator.invoke("5");
			XCTAssertEqual(stack.currentInputBuffer().progressTextBuffer(), "55");
			
			try calculator.invoke("5");
			XCTAssertEqual(stack.currentInputBuffer().progressTextBuffer(), "555");
			
			try calculator.invoke("5");
			XCTAssertEqual(stack.currentInputBuffer().progressTextBuffer(), "5,555");
		} catch _ {
			XCTFail();
		}
	}
}

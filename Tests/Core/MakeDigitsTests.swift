//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class MakeDigitsTests: TestSuperclass {
	func test987Dec() {
		XCTAssertEqual(makeNumber(integer: 987), makeNumber(digits: [9, 8, 7], radix: 10));
	}

	func testCAFEHex() {
		XCTAssertEqual(makeNumber(integer: 0xCAFE), makeNumber(digits: [12, 10, 15, 14], radix: 16));
	}
}

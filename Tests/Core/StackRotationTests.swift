//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class StackRotationTests: TestSuperclass {
	func testEmptyRotDown() {
		do {
			try calculator.invoke("rotdown");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 0);
	}

	func testBasicRotDown() {
		calculator.stack().setX(makeNumber("1.0"));
		calculator.stack().push(makeNumber("0.5"));
		calculator.stack().push(makeNumber("2.0"));

		do {
			try calculator.invoke("rotdown");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 3);

		XCTAssertEqual(calculator.stack().pop(), makeNumber("0.5"));
		XCTAssertEqual(calculator.stack().pop(), makeNumber("1.0"));
		XCTAssertEqual(calculator.stack().pop(), makeNumber("2.0"));
	}

	func testYNilRotDown() {
		calculator.stack().setX(makeNumber("1.0"));

		do {
			try calculator.invoke("rotdown");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);

		XCTAssertEqual(calculator.stack().getX(), makeNumber("1.0"));
	}

	func testEmptyRotUp() {
		do {
			try calculator.invoke("rotup");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 0);
	}
	
	func testBasicRotUp() {
		calculator.stack().push(makeNumber("1.0"));
		calculator.stack().push(makeNumber("0.5"));
		calculator.stack().push(makeNumber("2.0"));
		
		do {
			try calculator.invoke("rotup");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 3);
		
		XCTAssertEqual(calculator.stack().pop(), makeNumber("1.0"));
		XCTAssertEqual(calculator.stack().pop(), makeNumber("2.0"));
		XCTAssertEqual(calculator.stack().pop(), makeNumber("0.5"));
	}
	
	func testYNilRotUp() {
		calculator.stack().setX(makeNumber("1.0"));
		
		do {
			try calculator.invoke("rotup");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		
		XCTAssertEqual(calculator.stack().getX(), makeNumber("1.0"));
	}
}

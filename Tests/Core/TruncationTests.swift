//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class TruncationTests: TestSuperclass {
	func testEmptyIntx() {
		do {
			try calculator.invoke("intx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.zero);
	}

	func testIntx() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("0");
			try calculator.invoke("1");
			try calculator.invoke(".");
			try calculator.invoke("2");
			try calculator.invoke("5");
			try calculator.invoke("intx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: 101));
	}
	
	func testIntx2() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("0");
			try calculator.invoke("2");
			try calculator.invoke("intx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: 102));
	}
	
	func testIntx3() {
		do {
			try calculator.invoke("0");
			try calculator.invoke(".");
			try calculator.invoke("3");
			try calculator.invoke("intx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.zero);
	}

	func testEmptFracx() {
		do {
			try calculator.invoke("fracx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.zero);
	}

	func testFracX() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("0");
			try calculator.invoke("1");
			try calculator.invoke(".");
			try calculator.invoke("2");
			try calculator.invoke("5");
			try calculator.invoke("fracx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), makeNumber("0.25"));
	}
	
	func testFracx2() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("0");
			try calculator.invoke("2");
			try calculator.invoke("fracx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.zero);
	}
	
	func testFracx3() {
		do {
			try calculator.invoke("0");
			try calculator.invoke(".");
			try calculator.invoke("3");
			try calculator.invoke("fracx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().getX(), makeNumber("0.3"));
	}

	func testFracDontPopY() {
		stack.push(makeNumber(integer: 1));
		stack.push(makeNumber("101.25"));

		do {
			try calculator.invoke("fracx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 2);
		XCTAssertEqual(calculator.stack().getX(), makeNumber("0.25"));
	}
	
	func testIntDontPopY() {
		stack.push(makeNumber(integer: 1));
		stack.push(makeNumber("101.25"));
		
		do {
			try calculator.invoke("intx");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 2);
		XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: 101));
	}
}

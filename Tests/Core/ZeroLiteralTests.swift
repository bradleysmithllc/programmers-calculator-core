//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class ZeroLiteralTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("0");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.zero);
	}

	func testMultipleZeros() {
		do {
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("0");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.zero);
	}

	func testDoubleEmpty() {
		do {
			try calculator.invoke("00");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.zero);
	}

	func testMultipleDoubleZeros() {
		do {
			try calculator.invoke("00");
			try calculator.invoke("00");
			try calculator.invoke("00");
			try calculator.invoke("00");
			try calculator.invoke("00");
			try calculator.invoke("00");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.zero);
	}
	
	func testMultile() {
		do {
			try calculator.invoke("1");
			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.one);

			try calculator.invoke("0");
			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 10));

			try calculator.invoke("00");
			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 1000));
			
			try calculator.invoke(".");
			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 1000));
			
			try calculator.invoke("0");
			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 1000));
			
			try calculator.invoke("1");
			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber("1000.01"));
			
			try calculator.invoke("00");
			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber("1000.01"));
				
			try calculator.invoke("2");
			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber("1000.01002"));
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
	}
}

//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

// Conversions to UInt8 from Int64
public extension UInt64
{
	func print(_ places: UInt8) -> String
	{
		let text = "000000000000000000" + String(self);

        let beginIndex = text.index(text.endIndex, offsetBy: Int(places) * -1);
        
        return String(text[beginIndex...]);
        
        //return t.substring(with: t.index(t.endIndex, offsetBy: //Int(places) * -1) ..< t.endIndex);
	}
}

class MSToDurationTests: TestSuperclass {
	class time_interval
	{
		let hours: UInt64;
		let minutes: UInt64;
		let seconds: UInt64;
		let milliseconds: UInt64;
		let microseconds: UInt64;
		
		init(hours: UInt8)
		{
			self.hours = UInt64(hours);
			minutes = 0;
			seconds = 0;
			milliseconds = 0;
			microseconds = 0;
		}

		init(milliseconds: UInt16)
		{
			hours = 0;
			minutes = 0;
			seconds = 0;
			self.milliseconds = UInt64(milliseconds);
			microseconds = 0;
		}
		
		init(minutes: UInt8)
		{
			hours = 0;
			self.minutes = UInt64(minutes);
			seconds = 0;
			milliseconds = 0;
			microseconds = 0;
		}
		
		init(seconds: UInt8)
		{
			hours = 0;
			minutes = 0;
			self.seconds = UInt64(seconds);
			milliseconds = 0;
			microseconds = 0;
		}
		
		init(minutes: UInt8, seconds: UInt8)
		{
			hours = 0;
			self.minutes = UInt64(minutes);
			self.seconds = UInt64(seconds);
			milliseconds = 0;
			microseconds = 0;
		}
		
		init(hours: UInt8, minutes: UInt8, seconds: UInt8)
		{
			self.hours = UInt64(hours);
			self.minutes = UInt64(minutes);
			self.seconds = UInt64(seconds);
			milliseconds = 0;
			microseconds = 0;
		}
		
		init(hours: UInt8, minutes: UInt8, seconds: UInt8, milliseconds: UInt16)
		{
			self.hours = UInt64(hours);
			self.minutes = UInt64(minutes);
			self.seconds = UInt64(seconds);
			self.milliseconds = UInt64(milliseconds);
			microseconds = 0;
		}
		
		var id: String
		{
			get
			{
				return hours.print(2) + minutes.print(2) + seconds.print(2) + "." + milliseconds.print(3) + microseconds.print(6);
			}
		}
	}
	
	func testEmpty() {
		do {
			try calculator.invoke("milliseconds-to-duration");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}

	func testMillis() {
		stack.push(989);

		do {
			try calculator.invoke("milliseconds-to-duration");
			XCTAssertEqual(989, stack.memory("4"));
			XCTAssertEqual(0, stack.memory("1"));
			XCTAssertEqual(989, stack.memory("3"));
			XCTAssertEqual(0, stack.memory("7"));
			XCTAssertEqual(0, stack.memory("9"));
			XCTAssertEqual(0, stack.memory("10"));
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(0.989, stack.pop());
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}
	
	func testSeconds() {
		stack.push(59000);

		do {
			try calculator.invoke("milliseconds-to-duration");
			XCTAssertEqual(59000, stack.memory("4"));
			XCTAssertEqual(0, stack.memory("1"));
			XCTAssertEqual(0, stack.memory("3"));
			XCTAssertEqual(59, stack.memory("7"));
			XCTAssertEqual(0, stack.memory("9"));
			XCTAssertEqual(0, stack.memory("10"));
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(59, stack.pop());
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}
	
	func testMinutes() {
		stack.push(NSDecimalNumber(value: 59 * 60 * 1000 as Int32));

		do {
			try calculator.invoke("milliseconds-to-duration");
			XCTAssertEqual(3540000, stack.memory("4"));
			XCTAssertEqual(0, stack.memory("1"));
			XCTAssertEqual(0, stack.memory("3"));
			XCTAssertEqual(0, stack.memory("7"));
			XCTAssertEqual(59, stack.memory("9"));
			XCTAssertEqual(0, stack.memory("10"));
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(5900, stack.pop());
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}

	func testSpecial() {
		stack.push(2957396);
		
		do {
			try calculator.invoke("milliseconds-to-duration");
			XCTAssertEqual(2957396, stack.memory("4"));
			XCTAssertEqual(0, stack.memory("1"));
			XCTAssertEqual(396, stack.memory("3"));
			XCTAssertEqual(17, stack.memory("7"));
			XCTAssertEqual(49, stack.memory("9"));
			XCTAssertEqual(0, stack.memory("10"));
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(4917.396, stack.pop());
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}
	
	func testSpecial2() {
		stack.push(4685630);
		
		do {
			try calculator.invoke("milliseconds-to-duration");
			XCTAssertEqual(4685630, stack.memory("4"));
			XCTAssertEqual(0, stack.memory("1"));
			XCTAssertEqual(630, stack.memory("3"));
			XCTAssertEqual(5, stack.memory("7"));
			XCTAssertEqual(18, stack.memory("9"));
			XCTAssertEqual(1, stack.memory("10"));
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(11805.630, stack.pop());
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}
	
	func testHours() {
		let min: UInt32 = (1 * 60 * 60 * 1000);
		
		let other: UInt32 = (59 * 60 * 1000);

		stack.push(NSDecimalNumber(value: min + other));
		
		do {
			try calculator.invoke("milliseconds-to-duration");
			XCTAssertEqual(7140000, stack.memory("4"));
			XCTAssertEqual(0, stack.memory("1"));
			XCTAssertEqual(0, stack.memory("3"));
			XCTAssertEqual(0, stack.memory("7"));
			XCTAssertEqual(59, stack.memory("9"));
			XCTAssertEqual(1, stack.memory("10"));
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(15900, stack.pop());
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}
	
	func testDay() {
		stack.push(86400000);
		
		do {
			try calculator.invoke("milliseconds-to-duration");
			XCTAssertEqual(86400000, stack.memory("4"));
			XCTAssertEqual(0, stack.memory("1"));
			XCTAssertEqual(0, stack.memory("3"));
			XCTAssertEqual(0, stack.memory("7"));
			XCTAssertEqual(0, stack.memory("9"));
			XCTAssertEqual(24, stack.memory("10"));
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(240000, stack.pop());
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}
	
	func testWeek() {
		stack.push(604800000);
		
		do {
			try calculator.invoke("milliseconds-to-duration");
			XCTAssertEqual(604800000, stack.memory("4"));
			XCTAssertEqual(0, stack.memory("1"));
			XCTAssertEqual(0, stack.memory("3"));
			XCTAssertEqual(0, stack.memory("7"));
			XCTAssertEqual(0, stack.memory("9"));
			XCTAssertEqual(168, stack.memory("10"));
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(1680000, stack.pop());
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}
	
	func testYear() {
		stack.push(NSDecimalNumber(value: 31556908800 as Int64));
		
		do {
			try calculator.invoke("milliseconds-to-duration");
			XCTAssertEqual(NSDecimalNumber(value: 31556908800 as Int64), stack.memory("4"));
			XCTAssertEqual(0, stack.memory("1"));
			XCTAssertEqual(800, stack.memory("3"));
			XCTAssertEqual(28, stack.memory("7"));
			XCTAssertEqual(48, stack.memory("9"));
			XCTAssertEqual(8765, stack.memory("10"));
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(87654828.8, stack.pop());
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}
	
	let testdata: [time_interval] = [
		time_interval(hours: 0),
		time_interval(hours: 1),
		time_interval(milliseconds: 989),
		time_interval(minutes: 59),
		time_interval(seconds: 59),
		time_interval(minutes: 59, seconds: 32),
		time_interval(hours: 13, minutes: 59, seconds: 32),
		time_interval(hours: 26, minutes: 45, seconds: 19, milliseconds: 792),
		time_interval(hours: 1, minutes: 18, seconds: 05, milliseconds: 63)
	];
	
	func testMany() {
		for test in testdata
		{
			// calculate the long millisecond time
			let hours = test.hours * (60 * 60 * 1000);
			let minutes = test.minutes * (60 * 1000);
			let seconds = test.seconds * 1000;
			let ms: Int64 =
				Int64(hours +
				minutes +
				seconds +
				test.milliseconds);
		
			let nsms = NSDecimalNumber(value: ms as Int64);
			stack.push(nsms);
			do {
				try calculator.invoke("milliseconds-to-duration");
				// original input
				XCTAssertEqual(nsms, stack.memory("4"));
				// fractional milliseconds
				XCTAssertEqual(NSDecimalNumber(value: test.microseconds as UInt64), stack.memory("1"), test.id);
				// milliseconds
				XCTAssertEqual(NSDecimalNumber(value: test.milliseconds as UInt64), stack.memory("3"), test.id);
				// seconds
				XCTAssertEqual(NSDecimalNumber(value: test.seconds as UInt64), stack.memory("7"), test.id);
				// minutes
				XCTAssertEqual(NSDecimalNumber(value: test.minutes as UInt64), stack.memory("9"), test.id);
				// hours
				XCTAssertEqual(NSDecimalNumber(value: test.hours as UInt64), stack.memory("10"), test.id);
				XCTAssertEqual(1, stack.size());
				XCTAssertEqual(NSDecimalNumber(string: test.id), stack.pop(), test.id);
			} catch let unknownError {
				print("What the heck happened \(unknownError)?");
				XCTFail();
			}
		}
	}

	override func requiresConfiguration() -> Bool
	{
		return true;
	}
}

//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class ResetTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("rst");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 0);
	}

	func testStack() {
		calculator.stack().push(NSDecimalNumber.one);
		calculator.stack().push(NSDecimalNumber.one);

		do {
			try calculator.invoke("rst");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 0);
		XCTAssertEqual(calculator.stack().peek(0), NSDecimalNumber.zero);
	}

	func testProperty() {
		let _ = stack.setProperty("name", propertyValue: 1);

		do {
			try calculator.invoke("rst");
		} catch _ {
			XCTFail();
		}

		// There will always be at least one property for the numeric entry state
		XCTAssertEqual(stack.properties().count, 1);
	}

	func testNamedMemory() {
		var stack = calculator.stack();
		let _ = stack.assignMemory("name", value: NSDecimalNumber.one);

		do {
			try calculator.invoke("rst");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().memory().count, 0);
	}
}

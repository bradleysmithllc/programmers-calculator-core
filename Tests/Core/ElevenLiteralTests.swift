//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class ElevenLiteralTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("hex");

			try calculator.invoke("B");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("11"));
	}

	func testMultipleDigits() {
		do {
			try calculator.invoke("hex");

			try calculator.invoke("B");
			try calculator.invoke("B");
			try calculator.invoke("B");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(digits: [0xb, 0xb, 0xb], radix: 16));
	}
}

//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class BinaryTests: TestSuperclass {
	func testOrEmpty() {
		do {
			try calculator.invoke("or");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().pop());
	}

	func testAndEmpty() {
		do {
			try calculator.invoke("and");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().pop());
	}
	
	func testXorEmpty() {
		do {
			try calculator.invoke("xor");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().pop());
	}

	func testNotEmpty() {
		do {
			try calculator.invoke("not");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer: -1), calculator.stack().pop());
	}
	
	func testNot() {
		do {
			stack.push(makeNumber(integer: 1));
			try calculator.invoke("not");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer: -2), calculator.stack().pop());
	}

	func testOr() {
		calculator.stack().push(makeNumber("1.0"));
		calculator.stack().push(makeNumber("2.0"));

		do {
			try calculator.invoke("or");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber("3.0"), calculator.stack().pop());
		} catch _ {
			XCTFail();
		}
	}

	func testAnd() {
		calculator.stack().push(makeNumber("3.0"));
		calculator.stack().push(makeNumber("2.0"));
		
		do {
			try calculator.invoke("and");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber("2.0"), calculator.stack().pop());
		} catch _ {
			XCTFail();
		}
	}
	
	func testXor() {
		calculator.stack().push(makeNumber(integer: 7));
		calculator.stack().push(makeNumber(integer: 5));
		
		do {
			try calculator.invoke("xor");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber("2.0"), calculator.stack().pop());
		} catch _ {
			XCTFail();
		}
	}

	func testLShiftEmpty() {
		do {
			try calculator.invoke("lshift");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().pop());
		} catch _ {
			XCTFail();
		}
	}
	
	func testLShift() {
		calculator.stack().push(makeNumber(integer: 1));

		do {
			try calculator.invoke("lshift");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber(integer: 2), calculator.stack().peek());

			try calculator.invoke("lshift");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber(integer: 4), calculator.stack().peek());
			
			try calculator.invoke("lshift");
			try calculator.invoke("lshift");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber(integer: 16), calculator.stack().peek());
		} catch _ {
			XCTFail();
		}
	}
	
	func testRShiftEmpty() {
		do {
			try calculator.invoke("rshift");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().pop());
		} catch _ {
			XCTFail();
		}
	}

	func testRShift() {
		calculator.stack().push(makeNumber(integer: 16));
		
		do {
			try calculator.invoke("rshift");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber(integer: 8), calculator.stack().peek());
			
			try calculator.invoke("rshift");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber(integer: 4), calculator.stack().peek());
			
			try calculator.invoke("rshift");
			try calculator.invoke("rshift");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber(integer: 1), calculator.stack().peek());

			try calculator.invoke("rshift");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().peek());
		} catch _ {
			XCTFail();
		}
	}
	
	func testYRShiftX() {
		calculator.stack().push(makeNumber(integer: 16));
		
		do {
			try calculator.invoke("yrshiftx");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber(integer: 0), calculator.stack().peek());
		} catch _ {
			XCTFail();
		}
	}

	func testYRShiftXXTooBig() {
		calculator.stack().push(makeNumber(integer: 16));
		calculator.stack().push(makeNumber(integer: 64));
		
		do {
			try calculator.invoke("yrshiftx");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber(integer: 0), calculator.stack().peek());
		} catch _ {
			XCTFail();
		}
	}
	
	func testYRShiftXXLimit() {
		calculator.stack().push(makeNumber(integer: 16));
		calculator.stack().push(makeNumber(integer: 63));
		
		do {
			try calculator.invoke("yrshiftx");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber(integer: 0), calculator.stack().peek());
		} catch _ {
			XCTFail();
		}
	}
	
	func testYLShiftX() {
		calculator.stack().push(makeNumber(integer: 16));
		
		do {
			try calculator.invoke("ylshiftx");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber(integer: 0), calculator.stack().peek());
		} catch _ {
			XCTFail();
		}
	}
	
	func testYLShiftXXTooBig() {
		calculator.stack().push(makeNumber(integer: 16));
		calculator.stack().push(makeNumber(integer: 64));
		
		do {
			try calculator.invoke("ylshiftx");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber(integer: 0), calculator.stack().peek());
		} catch _ {
			XCTFail();
		}
	}
	
	func testYLShiftXXLimit() {
		calculator.stack().push(makeNumber(integer: 16));
		calculator.stack().push(makeNumber(integer: 63));
		
		do {
			try calculator.invoke("ylshiftx");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(makeNumber(integer: 0), calculator.stack().peek());
		} catch _ {
			XCTFail();
		}
	}
	
	func testNor() {
		stack.setNumericBehavior(.UnsignedInteger);
		stack.setIntegerWordSize(.byte);
		stack.setNumericBase(base: NumericBase.base16);

		calculator.stack().push(makeNumber(integer: 4));

		do {
			try calculator.invoke("nor");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(251, calculator.stack().peek());
		} catch _ {
			XCTFail();
		}
	}

	func testShiftNegative() {
		for op in ["ylshiftx", "yrshiftx"]
		{
		do {
			calculator.stack().push(makeNumber(integer: 1));
			calculator.stack().push(makeNumber(integer: -1));
			
			try calculator.invoke(op);
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(NSDecimalNumber.notANumber, calculator.stack().pop());
		} catch _ {
			XCTFail();
		}
		}
	}
}

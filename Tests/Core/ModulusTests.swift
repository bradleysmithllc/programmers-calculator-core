//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class ModulusTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("ymodx");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}

	func testMod0() {
		calculator.stack().setX(makeNumber("1.0"));
		calculator.stack().push(NSDecimalNumber.zero);

		do {
			try calculator.invoke("ymodx");
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.notANumber);
	}

	func testBasic() {
		calculator.stack().setX(makeNumber("17.0"));
		calculator.stack().push(makeNumber("2.0"));

		do {
			try calculator.invoke("ymodx");
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("1.0"));
	}

	func testDecimalBase() {
		calculator.stack().setX(makeNumber("17.7"));
		calculator.stack().push(makeNumber("2.0"));

		do {
			try calculator.invoke("ymodx");
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("1.7"));
	}

	func testDecimalMod() {
		calculator.stack().setX(makeNumber("17.0"));
		calculator.stack().push(makeNumber("2.5"));

		do {
			try calculator.invoke("ymodx");
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("2.0"));
	}

	func testDecimalBoth() {
		calculator.stack().setX(makeNumber("17.9"));
		calculator.stack().push(makeNumber("2.6"));

		do {
			try calculator.invoke("ymodx");
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("2.3"));
	}

	func testDecimal3() {
		calculator.stack().setX(makeNumber("1.0"));
		calculator.stack().push(makeNumber("3.0"));

		do {
			try calculator.invoke("ymodx");
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("1.0"));
	}
}

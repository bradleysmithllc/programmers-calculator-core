//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class ThirteenLiteralTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("hex");
			
			try calculator.invoke("D");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 13));
	}

	func testMultipleDigits() {
		do {
			try calculator.invoke("hex");
			
			try calculator.invoke("D");
			try calculator.invoke("D");
			try calculator.invoke("D");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(digits: [0xd, 0xd, 0xd], radix: 16));
	}
}

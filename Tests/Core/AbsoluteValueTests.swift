//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class AbsoluteValueTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("absx");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(makeNumber("0"), calculator.stack().pop());
	}

	func testPositive() {
		calculator.stack().push(makeNumber(integer: 1));
		
		do {
			try calculator.invoke("absx");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 1), calculator.stack().pop());
	}
	
	func testNegative() {
		calculator.stack().push(makeNumber(integer: -1));
		
		do {
			try calculator.invoke("absx");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(makeNumber(integer: 1), calculator.stack().pop());
	}
}

//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

/*Add a comment*/
class InvertXTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("1/x");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.notANumber);
	}

	func test10() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("0");
			try calculator.invoke("1/x");
			
			XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber(string: "0.10"));

			try calculator.invoke("1/x");
			XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber(value: 10 as Int));
		} catch _ {
			XCTFail();
		}
	}
}

//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class NumericStatesTests: TestSuperclass {
	func testDefaultState() {
		XCTAssertFalse(stack.inNumericEntry());
	}
	
	func testBeginNumericEntryAfterOperation() {
		do {
			try calculator.invoke("1");
			XCTAssertTrue(stack.inNumericEntry());
			try calculator.invoke("9");
			XCTAssertTrue(stack.inNumericEntry());
			try calculator.invoke("cosx");
			XCTAssertFalse(stack.inNumericEntry());

			try calculator.invoke("chsx");
			XCTAssertFalse(stack.inNumericEntry());

			XCTAssertEqual(1, calculator.stack().size());

			try calculator.invoke("9");
			XCTAssertEqual(2, calculator.stack().size());
			XCTAssertTrue(stack.inNumericEntry());
		} catch _ {
			XCTFail();
		}
	}

	func testBeginNumericEntry() {
		do {
			try calculator.invoke("1");
			XCTAssertTrue(stack.inNumericEntry());
			try calculator.invoke("9");
			XCTAssertTrue(stack.inNumericEntry());
			try calculator.invoke("chsx");
			XCTAssertTrue(stack.inNumericEntry());
		} catch _ {
			XCTFail();
		}
	}
	
	func testEnterEndsNumericEntry() {
		do {
			try calculator.invoke("1");
			XCTAssertTrue(stack.inNumericEntry());
			try calculator.invoke("9");
			XCTAssertTrue(stack.inNumericEntry());
			try calculator.invoke("ent");
			XCTAssertFalse(stack.inNumericEntry());
		} catch _ {
			XCTFail();
		}
	}

	func testChsInNumericEntry() {
		do {
			try calculator.invoke("1");
			XCTAssertEqual(1, calculator.stack().size());
			try calculator.invoke("2");
			XCTAssert(calculator.stack().size() == 1);
			try calculator.invoke("3");
			XCTAssertEqual(1, calculator.stack().size());
			try calculator.invoke("4");

			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual("1,234", stack.currentInputBuffer().progressTextBuffer());
			XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: 1234));

			try calculator.invoke("chsx");
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: -1234));

			try calculator.invoke("4");

			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: -12344));

			try calculator.invoke("chsx");
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: 12344));
		} catch _ {
			XCTFail();
		}
	}

	func testChsAfterOperation() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("2");
			try calculator.invoke("3");
			try calculator.invoke("4");
			
			try calculator.invoke("cosx");
			XCTAssertEqual(1, calculator.stack().size());

			let dec = calculator.stack().getX();

			try calculator.invoke("chsx");
			
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(calculator.stack().getX(), dec.decimalNumberByNegating());
			
			try calculator.invoke("chsx");
			XCTAssertEqual(1, calculator.stack().size());
			XCTAssertEqual(calculator.stack().getX(), dec);
		} catch _ {
			XCTFail();
		}
	}

	func testNumericAfterOperation() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("2");
			try calculator.invoke("3");
			try calculator.invoke("4");
			
			try calculator.invoke("cosx");
			XCTAssertEqual(1, calculator.stack().size());
			
			let dec = calculator.stack().getX();
			
			try calculator.invoke("9");
			
			XCTAssertEqual(2, calculator.stack().size());
			XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: 9));
			XCTAssertEqual(calculator.stack().getY(), dec);
		} catch _ {
			XCTFail();
		}
	}
	
	func testEnterLeavesXOpen() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("ent");
			try calculator.invoke("2");

			XCTAssertEqual(calculator.stack().size(), 2);

			XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: 2));
			XCTAssertEqual(calculator.stack().getY(), makeNumber(integer: 1));
		} catch _ {
			XCTFail();
		}
	}
	
	/*These tests cover the maximum number of digits allowed
	*/
	func testNoneEntered() {
		XCTAssertTrue(BaseLiteralOperation.validateStack(stack, digitsToAllow: 38, digitToCheck: LiteralDigit._0));
	}

	func testMaxEntered() {
		do {
			// fill the input buffer with max digits
			for _ in 1...38
			{
				try calculator.invoke("9");
			}

			var valid = BaseLiteralOperation.validateStack(stack, digitsToAllow: 38, digitToCheck: LiteralDigit._0);
			XCTAssertFalse(valid);
			
			stack.clearLastDigitInNumericEntry();
			valid = BaseLiteralOperation.validateStack(stack, digitsToAllow: 38, digitToCheck: LiteralDigit._0);
			XCTAssertTrue(valid);
		} catch _ {
			XCTFail();
		}
	}
	
	func testAlmostMax() {
		do {
			// fill the input buffer with max digits
			for _ in 1...37
			{
				try calculator.invoke("9");
			}
			
			XCTAssertTrue(BaseLiteralOperation.validateStack(stack, digitsToAllow: 38, digitToCheck: LiteralDigit._0));
		} catch _ {
			XCTFail();
		}
	}

	func testAlmostMaxWithDot() {
		do {
			try calculator.invoke(".");

			// fill the input buffer with near max digits
			for _ in 1...37
			{
				try calculator.invoke("9");
			}
			
			XCTAssertTrue(BaseLiteralOperation.validateStack(stack, digitsToAllow: 38, digitToCheck: LiteralDigit._0));

			try calculator.invoke("9");
			XCTAssertFalse(BaseLiteralOperation.validateStack(stack, digitsToAllow: 38, digitToCheck: LiteralDigit._0));
		} catch _ {
			XCTFail();
		}
	}
	
	func testAlmostMaxWithNegative() {
		do {
			// fill the input buffer with max digits
			for _ in 1...37
			{
				try calculator.invoke("9");
			}
			
			try calculator.invoke("chsx");
			
			//print("currentInputImage \(stack.currentInputImage())");
			XCTAssertTrue(BaseLiteralOperation.validateStack(stack, digitsToAllow: 38, digitToCheck: LiteralDigit._0));

			try calculator.invoke("9");
			XCTAssertFalse(BaseLiteralOperation.validateStack(stack, digitsToAllow: 38, digitToCheck: LiteralDigit._0));
		} catch _ {
			XCTFail();
		}
	}

	func testAlmostMaxWithNegativeAndDot() {
		do {
			// fill the input buffer with max digits
			try calculator.invoke(".");
			for _ in 1...37
			{
				try calculator.invoke("9");
			}
			try calculator.invoke("chsx");
			
			XCTAssertTrue(BaseLiteralOperation.validateStack(stack, digitsToAllow: 38, digitToCheck: LiteralDigit._0));
			
			try calculator.invoke("9");
			XCTAssertFalse(BaseLiteralOperation.validateStack(stack, digitsToAllow: 38, digitToCheck: LiteralDigit._0));
		} catch _ {
			XCTFail();
		}
	}

	func testDoubleDigit() {
		do {
			let op = calculator.operation("00")!;

			// fill the input buffer with max digits
			for _ in 1...36
			{
				try calculator.invoke("9");
			}
			
			XCTAssertTrue(op.validateStack());

			try calculator.invoke("9");
			XCTAssertFalse(op.validateStack());
			
			stack.clearLastDigitInNumericEntry();
			XCTAssertTrue(op.validateStack());
		} catch _ {
			XCTFail();
		}
	}
	
	func testDoubleDigitFF() {
		stack.setNumericBase(base: NumericBase.base16);
		stack.setNumericBehavior(.UnsignedInteger)
		stack.setIntegerWordSize(.word_8);

		do {
			let op = calculator.operation("FF")!;
			
			// fill the input buffer with max digits
			for _ in 1...14
			{
				try calculator.invoke("F");
			}

			XCTAssertTrue(op.validateStack());
			
			try calculator.invoke("F");
			XCTAssertFalse(op.validateStack());
			
			stack.clearLastDigitInNumericEntry();
			XCTAssertTrue(op.validateStack());
		} catch _ {
			XCTFail();
		}
	}
	
	func testHexWordSizesByte() {
		binSizes(NumericBase.base16, numericBehavior: .UnsignedInteger, integerWordSize: .byte, numBits: 1);
	}

	func testHexWordSizesWord2() {
		binSizes(NumericBase.base16, numericBehavior: .UnsignedInteger, integerWordSize: .word_2, numBits: 3);
	}

	func testHexWordSizesWord4() {
		binSizes(NumericBase.base16, numericBehavior: .UnsignedInteger, integerWordSize: .word_4, numBits: 7);
	}
	
	func testHexWordSizesWord8() {
		binSizes(NumericBase.base16, numericBehavior: .UnsignedInteger, integerWordSize: .word_8, numBits: 15);
	}

	func testBinWordSizesByte() {
		binSizes(NumericBase.base2, numericBehavior: .UnsignedInteger, integerWordSize: .byte, numBits: 7);
	}

	func testBinWordSizesWord2() {
		binSizes(NumericBase.base2, numericBehavior: .UnsignedInteger, integerWordSize: .word_2, numBits: 15);
	}
	
	func testBinWordSizesWord4() {
		binSizes(NumericBase.base2, numericBehavior: .UnsignedInteger, integerWordSize: .word_4, numBits: 31);
	}
	
	func testBinWordSizesWord8() {
		binSizes(NumericBase.base2, numericBehavior: .UnsignedInteger, integerWordSize: .word_8, numBits: 63);
	}
	
	func testOctWordSizesByte() {
		binSizes(NumericBase.base8, numericBehavior: .UnsignedInteger, integerWordSize: .byte, numBits: 2);
	}
	
	func testOctWordSizesWord2() {
		binSizes(NumericBase.base8, numericBehavior: .UnsignedInteger, integerWordSize: .word_2, numBits: 5);
	}
	
	func testOctWordSizesWord4() {
		binSizes(NumericBase.base8, numericBehavior: .UnsignedInteger, integerWordSize: .word_4, numBits: 10);
	}

	func testOctWordSizesWord8() {
		binSizes(NumericBase.base8, numericBehavior: .UnsignedInteger, integerWordSize: .word_8, numBits: 21);
	}
	
	func testDecWordSizesByte() {
		binSizes(NumericBase.base10, numericBehavior: .UnsignedInteger, integerWordSize: .byte, numBits: 2);
	}
	
	func testDecWordSizesWord2() {
		binSizes(NumericBase.base10, numericBehavior: .UnsignedInteger, integerWordSize: .word_2, numBits: 4);
	}
	
	func testDecWordSizesWord4() {
		binSizes(NumericBase.base10, numericBehavior: .UnsignedInteger, integerWordSize: .word_4, numBits: 9);
	}
	
	func testDecWordSizesWord8() {
		binSizes(NumericBase.base10, numericBehavior: .UnsignedInteger, integerWordSize: .word_8, numBits: 19);
	}

	func binSizes(
		_ numericBase: NumericBase,
		numericBehavior: NumericBehavior,
		integerWordSize: IntegerWordSize,
		numBits: UInt8
	) {
		stack.setNumericBase(base: numericBase);
		stack.setNumericBehavior(numericBehavior)
		stack.setIntegerWordSize(integerWordSize);
		
		do {
			let op = calculator.operation("1")!;
			
			for _ in 1...numBits
			{
				try calculator.invoke("1");
			}
			
			XCTAssertTrue(op.validateStack());
			
			try calculator.invoke("1");
			XCTAssertFalse(op.validateStack());
			
			stack.clearLastDigitInNumericEntry();
			XCTAssertTrue(op.validateStack());
		} catch _ {
			XCTFail();
		}
	}
}

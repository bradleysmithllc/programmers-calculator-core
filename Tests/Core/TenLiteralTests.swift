//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class TenLiteralTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("hex");
			
			try calculator.invoke("A");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("10"));
	}

	func testMultipleDigits() {
		do {
			try calculator.invoke("hex");
			
			try calculator.invoke("A");
			try calculator.invoke("A");
			try calculator.invoke("A");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(digits: [0xa, 0xa, 0xa], radix: 16));
	}
}

//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class CombinationsPermutationsTests: TestSuperclass {
	func testPermEmpty() {
		do {
			try calculator.invoke("py_x");
		} catch _ {
			XCTFail();
		}
	}

	func testPerm() {
		do {
			stack.push(makeNumber(integer: 10));
			stack.push(makeNumber(integer: 3));
			try calculator.invoke("py_x");

			XCTAssertEqual(720, stack.pop());
		} catch _ {
			XCTFail();
		}
	}
	
	func testComb() {
		do {
			stack.push(makeNumber(integer: 10));
			stack.push(makeNumber(integer: 3));
			try calculator.invoke("cy_x");
			
			XCTAssertEqual(120, stack.pop());
		} catch _ {
			XCTFail();
		}
	}
	
	func testCombEmpty() {
		do {
			try calculator.invoke("cy_x");
		} catch _ {
			XCTFail();
		}
	}
	
	override func requiresConfiguration() -> Bool
	{
		return true;
	}
}

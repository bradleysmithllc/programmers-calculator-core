//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class ReversePercentageTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("reverse-percentage");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(NSDecimalNumber.notANumber, calculator.stack().pop());
	}

	func testSample() {
		do {
			stack.push(makeNumber(integer: 100));
			stack.push(makeNumber(integer: 50));

			try calculator.invoke("reverse-percentage");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer: 200), calculator.stack().pop());
	}

	override func requiresConfiguration() -> Bool
	{
		return true;
	}
}

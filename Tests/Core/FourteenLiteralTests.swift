//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class FourteenLiteralTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("hex");
			
			try calculator.invoke("E");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 14));
	}

	func testMultipleDigits() {
		do {
			try calculator.invoke("hex");
			
			try calculator.invoke("E");
			try calculator.invoke("E");
			try calculator.invoke("E");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber(digits: [0xe, 0xe, 0xe], radix: 16));
	}
}

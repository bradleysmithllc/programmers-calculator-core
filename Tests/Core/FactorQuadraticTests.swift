//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class FactorQuadraticTests: TestSuperclass {
	func testZero() {
		do {
			try calculator.invoke("fact_quad");
			
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual("(0x + 0) · (0x + 0)", stack.popElement().message());

		} catch _ {
			XCTFail();
		}
	}
	
	func testNoSolution() {
		stack.push(makeNumber(integer: 99));
		stack.push(NSDecimalNumber.zero);
		stack.push(makeNumber(integer: -28));
		
		do {
			try calculator.invoke("fact_quad");
			
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual("No Solution", stack.popElement().message());
		} catch _ {
			XCTFail();
		}
	}

	func testInverseSquare() {
		stack.push(makeNumber(integer: 4));
		stack.push(NSDecimalNumber.zero);
		stack.push(makeNumber(integer: -1));

		do {
			try calculator.invoke("fact_quad");

			XCTAssertEqual(2, stack.size());
			XCTAssertEqual("(2x - 1) · (2x + 1)", stack.popElement().message());
			XCTAssertEqual("(-2x - 1) · (-2x + 1)", stack.popElement().message());
		} catch _ {
			XCTFail();
		}
	}

	func testSolution1() {
		stack.push(makeNumber(integer: -6));
		stack.push(25);
		stack.push(makeNumber(integer: -14));
		
		do {
			try calculator.invoke("fact_quad");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual("(-2x + 7) · (3x - 2)", stack.popElement().message());
			XCTAssertEqual("(-3x + 2) · (2x - 7)", stack.popElement().message());
		} catch _ {
			XCTFail();
		}
	}
	
	func testSolution2() {
		stack.push(makeNumber(integer: 81));
		stack.push(126);
		stack.push(makeNumber(integer: 49));
		
		do {
			try calculator.invoke("fact_quad");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual("(9x + 7) · (9x + 7)", stack.popElement().message());
			XCTAssertEqual("(-9x - 7) · (-9x - 7)", stack.popElement().message());
		} catch _ {
			XCTFail();
		}
	}
	
	func testOneX() {
		stack.push(makeNumber(integer: 1));
		stack.push(0);
		stack.push(makeNumber(integer: -1));
		
		do {
			try calculator.invoke("fact_quad");
			
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual("(x - 1) · (x + 1)", stack.popElement().message());
			XCTAssertEqual("(-x - 1) · (-x + 1)", stack.popElement().message());
		} catch _ {
			XCTFail();
		}
	}
}

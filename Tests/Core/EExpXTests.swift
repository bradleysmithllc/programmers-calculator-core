//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class EExpXTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("e^x");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}

	func testZeroExp() {
		calculator.stack().push(makeNumber("0.0"));

		do {
			try calculator.invoke("e^x");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.one);
	}

	func testOneExp() {
		calculator.stack().push(makeNumber("1"));

		do {
			try calculator.invoke("e^x");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), EOperation.E.decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()));
	}
	
	func testTwoExp() {
		calculator.stack().push(makeNumber("2"));
		
		do {
			try calculator.invoke("e^x");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), MathHub.mathHub().multiply(left: EOperation.E, right: EOperation.E).decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()));
	}
}

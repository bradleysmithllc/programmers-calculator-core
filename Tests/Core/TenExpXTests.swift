//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class TenExpXTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("10^x");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}
	
	func testZero() {
		stack.push(NSDecimalNumber.zero);
		
		do {
			try calculator.invoke("10^x");
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(NSDecimalNumber.one, stack.getX());
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}
	
	func testOne() {
		stack.push(NSDecimalNumber.one);
		
		do {
			try calculator.invoke("10^x");
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(makeNumber(integer: 10), stack.getX());
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}
	
	func testTwo() {
		stack.push(makeNumber(integer: 2));
		
		do {
			try calculator.invoke("10^x");
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(makeNumber(integer: 100), stack.getX());
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}
}

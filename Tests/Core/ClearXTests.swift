//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

/*Add a comment*/
class ClearXTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("clx");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(1, calculator.stack().size());
	}

	func testWithData() {
		do {
			try calculator.invoke("5");

			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 5));

			try calculator.invoke("clx");

			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.zero);
		} catch _ {
			XCTFail();
		}
	}
	
	func testResetState() {
		do {
			try calculator.invoke("5");
			
			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 5));
			
			try calculator.invoke("clx");
			
			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.zero);

			try calculator.invoke("7");
			
			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), makeNumber(integer: 7));
			
		} catch _ {
			XCTFail();
		}
	}

	func testWithMuchData() {
		do {
			try calculator.invoke("5");
			try calculator.invoke("2");
			try calculator.invoke("1");

			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: 521));

			try calculator.invoke("ent");

			try calculator.invoke("7");
			try calculator.invoke("2");
			try calculator.invoke("1");

			XCTAssertEqual(calculator.stack().size(), 2);
			XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: 721));

			try calculator.invoke("clx");

			XCTAssertEqual(calculator.stack().size(), 2);
			XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.zero);

			try calculator.invoke("dropx");

			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().getX(), makeNumber(integer: 521));
		} catch _ {
			XCTFail();
		}
	}
}

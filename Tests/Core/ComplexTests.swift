//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class ComplexTests: TestSuperclass {
	func testVerySmall() {
		calculator.stack().push(makeNumber("2000"));

		do {
			try calculator.invoke("1/x");

			print(stack.peek());
			try calculator.invoke("x^2");
			
			print(stack.peek());
			try calculator.invoke("x^2");

			print(stack.peek());
			try calculator.invoke("x^2");
			
			print(stack.peek());
			try calculator.invoke("x^2");

			print(stack.peek());
			try calculator.invoke("x^2");
			
			print(stack.peek());
			try calculator.invoke("x^2");
			
			print(stack.peek());
			try calculator.invoke("x^2");
			
			print(stack.peek());
			try calculator.invoke("x^2");
		} catch _ {
			XCTFail();
		}
	}
}

//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class DivisionTests: TestSuperclass {
	func testDivisionByZero() {
		calculator.stack().push(makeNumber("1.0"));
		calculator.stack().push(NSDecimalNumber.zero);

		do {
			try calculator.invoke("/");
		} catch _ {
			XCTFail();
		}

		// validate that NaN is pushed onto the stack
		let result: NSDecimalNumber! = calculator.stack().pop();

		XCTAssert(result == NSDecimalNumber.notANumber);
	}

	func testBasic() {
		calculator.stack().push(makeNumber("1.0"));
		calculator.stack().push(makeNumber("0.5"));

		do {
			try calculator.invoke("/");
		} catch _ {
			XCTFail();
		}

		XCTAssert(calculator.stack().pop() == makeNumber("2.0"));
	}
}

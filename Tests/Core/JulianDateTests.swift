//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class JulianDateTests: TestSuperclass {
	func testDTJDEmpty() {
		do {
			try calculator.invoke("to_julian_day");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer: 1721026), calculator.stack().pop());
	}

	func testReferenceIgnoresDecimal() {
		do {
			stack.push(makeNumber("20140101.091981"));
			try calculator.invoke("to_julian_day");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer: 2456659), calculator.stack().pop());
	}

	func testReference() {
		do {
			stack.push(makeNumber(integer: 20140101));
			try calculator.invoke("to_julian_day");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer: 2456659), calculator.stack().pop());
	}

	func testReference2() {
		do {
			stack.push(makeNumber(integer: 20000101));
			try calculator.invoke("to_julian_day");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer:  2451545), calculator.stack().pop());
	}
	
	func testDFJDEmpty() {
		do {
			try calculator.invoke("to_date");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer: 47130101), calculator.stack().pop());
	}
	
	func testDFJDReferenceIgnoresDecimal() {
		do {
			stack.push(makeNumber("2456659.091981"));
			try calculator.invoke("to_date");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer: 20140101), calculator.stack().pop());
	}
	
	func testDFJDReference() {
		do {
			stack.push(makeNumber(integer: 2456659));
			try calculator.invoke("to_date");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer: 20140101), calculator.stack().pop());
	}
	
	func testDFJDReference2() {
		do {
			stack.push(makeNumber(integer: 2451545));
			try calculator.invoke("to_date");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber(integer:  20000101), calculator.stack().pop());
	}
}

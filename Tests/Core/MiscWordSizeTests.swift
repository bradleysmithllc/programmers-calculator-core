//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class MiscWordSizeTests: TestSuperclass {
	func testBug1() {
		/*This bug happens when the sign mode is changed and the number in the register is too big for the type
		  Specifically, we set the mode to decimal, enter -1, set the mode to signed Int8, change to signed Int16, and 255
		  must be the result, not 65535.
		*/
		stack.setNumericBehavior(.Decimal);

		stack.push(-1);
		stack.setNumericBehavior(.SignedInteger);
		stack.setIntegerWordSize(.byte);

		XCTAssertEqual(-1, stack.peek());

		stack.setNumericBehavior(.UnsignedInteger);

		XCTAssertEqual(255, stack.peek());

		stack.setIntegerWordSize(.byte);

		XCTAssertEqual(255, stack.peek());
	}
}

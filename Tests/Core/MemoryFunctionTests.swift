//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class MemoryFunctionTests: TestSuperclass {
	func testClearMemoryEmpty() {
		do {
			try calculator.invoke("cm");
		} catch _ {
			XCTFail();
		}
	}
	
	func testMemoryStoreEmpty() {
		do {
			try calculator.invoke("sm");
		} catch _ {
			XCTFail();
		}
	}

	func testMemoryPlusEmpty() {
		do {
			try calculator.invoke("m+");
		} catch _ {
			XCTFail();
		}
	}
	
	func testMemoryMinusEmpty() {
		do {
			try calculator.invoke("m-");
		} catch _ {
			XCTFail();
		}
	}
	
	func testRecallMemoryEmpty() {
		do {
			try calculator.invoke("rm");
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(stack.getX(), NSDecimalNumber.zero);
		} catch _ {
			XCTFail();
		}
	}
	
	func testMemoryStack() {
		do {
			stack.push(makeNumber(integer: 5));
			stack.push(makeNumber(integer: 4));
			stack.push(makeNumber(integer: 3));
			stack.push(makeNumber(integer: 2));
			stack.push(makeNumber(integer: 1));
			stack.push(makeNumber(integer: 0));
			stack.push(makeNumber(integer: -1));
			stack.push(makeNumber(integer: -2));

			try calculator.invoke("m+");  // -2
			let _ = stack.pop();

			try calculator.invoke("m+");  // -1
			let _ = stack.pop();
			
			try calculator.invoke("m+");  // 0
			let _ = stack.pop();
			
			try calculator.invoke("m+");  // 1
			let _ = stack.pop();
			
			try calculator.invoke("m-");  // 2
			let _ = stack.pop();
			
			try calculator.invoke("m-");  // 3
			let _ = stack.pop();
			
			try calculator.invoke("m+");  // 4
			let _ = stack.pop();
			
			try calculator.invoke("m+");  // 5
			let _ = stack.pop();
			try calculator.invoke("rm");

			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(makeNumber(integer: 2), stack.getX());
		} catch _ {
			XCTFail();
		}
	}
	
	func testRecallMemoryScript() {
		do {
			stack.push(makeNumber(integer: 5));
			try check("m+", result: makeNumber(integer: 5));

			stack.push(makeNumber(integer: -1));
			try check("m+", result: makeNumber(integer: 4));

			stack.push(makeNumber(integer: 7));
			try check("m-", result: makeNumber(integer: -3));

			stack.push(NSDecimalNumber.zero);
			try check("cm", result: makeNumber(integer: 0));
		} catch _ {
			XCTFail();
		}
	}
	
	func testStoreMemoryScript() {
		do {
			stack.push(makeNumber(integer: 5));
			try check("sm", result: makeNumber(integer: 5));

			stack.push(makeNumber(integer: 7));
			try check("m+", result: makeNumber(integer: 12));
			
			stack.push(makeNumber(integer: 9));
			try check("sm", result: makeNumber(integer: 9));
		} catch _ {
			XCTFail();
		}
	}
	
	fileprivate func check(_ op: String, result: NSDecimalNumber) throws
	{
		try calculator.invoke(op);

		XCTAssertEqual(1, stack.size());
		let _ = stack.pop();

		try calculator.invoke("rm");
		XCTAssertEqual(stack.getX(), result);
		XCTAssertEqual(1, stack.size());
		let _ = stack.pop();
	}
}

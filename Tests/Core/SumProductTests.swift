//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class SumProductTests: TestSuperclass {
	func testBasic() {
		calculator.stack().push(makeNumber("1.0"));
		calculator.stack().push(makeNumber("0.5"));
		calculator.stack().push(makeNumber("0.25"));
		calculator.stack().push(makeNumber("0.125"));
		calculator.stack().push(makeNumber("2.0"));
		calculator.stack().push(makeNumber("3.0"));

		do {
			try calculator.invoke("sum-product");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(1, calculator.stack().size());
		XCTAssertEqual(makeNumber("6.53125"), calculator.stack().pop());
	}

	func testEmpty() {
		do {
			try calculator.invoke("sum-product");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(NSDecimalNumber.zero, calculator.stack().pop());
	}

	func testIgnoreOddDataPoint() {
		calculator.stack().push(makeNumber("0.25"));
		calculator.stack().push(makeNumber("0.5"));
		calculator.stack().push(makeNumber("1.0"));
		
		do {
			try calculator.invoke("sum-product");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(2, calculator.stack().size());
		XCTAssertEqual(makeNumber("0.5"), calculator.stack().pop());
		XCTAssertEqual(makeNumber("0.25"), calculator.stack().pop());
	}
}

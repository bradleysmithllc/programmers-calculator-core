//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class OneLiteralTests: TestSuperclass {
	func testMessages() {
		// first generate NaN with 1/0
		do {
			try calculator.invoke("pi_constant");
			try calculator.invoke("fact_quad");
		} catch _ {
			XCTFail();
		}

		// verify a message is on the stack
		XCTAssertTrue(calculator.stack().getXElement().isMessage);

		// press pi.
		do {
			// add some NaN
			stack.push(NSDecimalNumber.notANumber);
			stack.push(NSDecimalNumber.notANumber);
			stack.push(NSDecimalNumber.notANumber);

			try calculator.invoke("1");
		} catch _ {
			XCTFail();
		}

		// now the stack should have one entry of pi
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.one);
	}

	func testNaNInX() {
		stack.push(NSDecimalNumber.notANumber);

		do {
			try calculator.invoke("1");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.one);
	}

	func testEmpty() {
		do {
			try calculator.invoke("1");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.one);
	}

	func testMultipleZeros() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("1");
			try calculator.invoke("1");
			try calculator.invoke("1");
			try calculator.invoke("1");
			try calculator.invoke("1");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("111111"));
	}
}

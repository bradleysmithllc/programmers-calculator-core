//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class Base10LiteralTests: TestSuperclass {
	func testExtranneousZeroes() {
		do {
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("0");
			try calculator.invoke("1");
			try calculator.invoke("0");
			try calculator.invoke("2");
			try calculator.invoke("3");
			try calculator.invoke("4");
			try calculator.invoke("5");
			try calculator.invoke("6");
			try calculator.invoke("7");
			try calculator.invoke("8");
			try calculator.invoke("9");
			try calculator.invoke("0");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("10234567890"));
	}
	
	func testPositional() {
		do {
			try calculator.invoke("1");
			try calculator.invoke("0");
			try calculator.invoke("2");
			try calculator.invoke("0");
			try calculator.invoke("1");
			try calculator.invoke("2");
			try calculator.invoke("3");
			try calculator.invoke("4");
			try calculator.invoke("3");
			try calculator.invoke("9");
			try calculator.invoke("8");
			try calculator.invoke("7");
			try calculator.invoke("6");
			try calculator.invoke("5");
			try calculator.invoke("4");
			try calculator.invoke("3");
			try calculator.invoke("2");
			try calculator.invoke("1");
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("102012343987654321"));
	}
	
	func testDecimalState() {
		do {
			XCTAssertTrue(calculator.operations()["."]!.validateStack());
			try calculator.invoke(".");
			XCTAssertFalse(calculator.operations()["."]!.validateStack());
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
	}
	
	func testInputState() {
		do {
			XCTAssertEqual(calculator.stack().size(), 0);
			
			try calculator.invoke(".");
			
			XCTAssertEqual(calculator.stack().size(), 1);
			XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.zero);
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
	}
	
	func testInputStateSequential() {
		do {
			XCTAssertEqual(calculator.stack().size(), 0);
			
			let ops = [
				["0", NSDecimalNumber.zero],
				["1", NSDecimalNumber.one],
				["1", NSDecimalNumber(string: "11")],
				[".", NSDecimalNumber(string: "11")],
				["9", NSDecimalNumber(string: "11.9")],
				["8", NSDecimalNumber(string: "11.98")],
				["7", NSDecimalNumber(string: "11.987")],
				["3", NSDecimalNumber(string: "11.9873")]
			];
			
			for op in ops {
				let operName: String = op[0] as! String;
				try calculator.invoke(operName);
				
				let expected: NSDecimalNumber = op[1] as! NSDecimalNumber;
				let actual = calculator.stack().peek();
				
				print("Expected \(expected) actual \(actual)");
				
				XCTAssertEqual(calculator.stack().size(), 1);
				XCTAssertEqual(actual, expected);
			}
		} catch is BadOperationState {
		} catch {
			XCTFail();
		}
	}
	
	/*If a number is pushed into the X register by an operation, further numeric entry
	must push the previous value up the stack before entering.
	*/
	func testNumericInputAfterOperation()
	{
		// simulate value pushed on by an operation
		calculator.stack().push(makeNumber(integer: 12));
		calculator.stack().push(makeNumber(integer: 5));

		do
		{
			try calculator.invoke("*");
			try calculator.invoke("9");
			
			// now stack must have two entries - 60 in Y and 9 in X
			XCTAssertEqual(9, calculator.stack().getX());
			XCTAssertEqual(60, calculator.stack().peek(0));

			try calculator.invoke("1");
			
			// now stack must have two entries - 61 in Y and 91 in X
			XCTAssertEqual(91, calculator.stack().getX());
			XCTAssertEqual(60, calculator.stack().getY());
		}
		catch
		{
			XCTFail();
		}
	}

	func testChsDoesNotResetNumericEntry()
	{
		do
		{
			try calculator.invoke("9");
			
			// stack has 9
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(9, calculator.stack().getX());

			try calculator.invoke("chsx");
			
			// stack has -9
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(-9, calculator.stack().getX());

			try calculator.invoke("1");

			// stack has -91
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(-91, calculator.stack().getX());
			
			try calculator.invoke("chsx");
			
			// stack has 91
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(91, calculator.stack().getX());
		}
		catch
		{
			XCTFail();
		}
	}

	func testMultResetsNumericEntry()
	{
		do
		{
			try calculator.invoke("9");
			try calculator.invoke("ent");
			try calculator.invoke("7");
			
			// stack has 9 and 7
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(9, calculator.stack().getY());
			XCTAssertEqual(7, calculator.stack().getX());
			
			try calculator.invoke("*");

			// stack has 63
			XCTAssertEqual(1, stack.size());
			XCTAssertEqual(63, calculator.stack().getX());

			try calculator.invoke("7");
			
			// stack has 63 and 7
			XCTAssertEqual(2, stack.size());
			XCTAssertEqual(7, calculator.stack().getX());
			XCTAssertEqual(63, calculator.stack().getY());
		}
		catch
		{
			XCTFail();
		}
	}
}

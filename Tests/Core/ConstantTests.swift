//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class ConstantTests: TestSuperclass {
	/* If X is NaN, enter should push pi rather than pushing NaN up the stack. */
	func testNaN() {
		// first generate NaN with 1/0
		do {
			try calculator.invoke("1");
			try calculator.invoke("ent");
			try calculator.invoke("0");
			try calculator.invoke("/");
		} catch _ {
			XCTFail();
		}

		// verify NaN is on the stack
		XCTAssertEqual(calculator.stack().getX(), NSDecimalNumber.notANumber);

		// press pi.
		do {
			try calculator.invoke("pi_constant");
		} catch _ {
			XCTFail();
		}

		// now the stack should have one entry of pi
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), PiOperation.PI.decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()));
	}

	/* If X is a message, enter should push pi rather than pushing the message up the stack. */
	func testMessage() {
		// first generate NaN with 1/0
		do {
			try calculator.invoke("fact_quad");
		} catch _ {
			XCTFail();
		}

		// verify NaN is on the stack
		XCTAssertTrue(calculator.stack().getXElement().isMessage);

		// press pi.
		do {
			try calculator.invoke("pi_constant");
		} catch _ {
			XCTFail();
		}

		// now the stack should have one entry of pi
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), PiOperation.PI.decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()));
	}

	/* If X is a message, enter should push pi rather than pushing the message up the stack. */
	func testMessages() {
		// first generate NaN with 1/0
		do {
			try calculator.invoke("pi_constant");
			try calculator.invoke("fact_quad");
		} catch _ {
			XCTFail();
		}

		// verify a message is on the stack
		XCTAssertTrue(calculator.stack().getXElement().isMessage);

		// press pi.
		do {
			// add some NaN
			stack.push(NSDecimalNumber.notANumber);
			stack.push(NSDecimalNumber.notANumber);
			stack.push(NSDecimalNumber.notANumber);

			try calculator.invoke("pi_constant");
		} catch _ {
			XCTFail();
		}

		// now the stack should have one entry of pi
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), PiOperation.PI.decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()));
	}

	func testπ() {
		do {
			try calculator.invoke("pi_constant");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber(value: Double.pi).decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()));
	}

	func test2π() {
		do {
			try calculator.invoke("two_pi_constant");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber(value: Double.pi).multiplying(by: NSDecimalNumber(value: 2 as Int)).decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()));
	}

	func testE() {
		do {
			try calculator.invoke("e_constant");
		} catch _ {
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber(value: M_E as Double).decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()));
	}

	func testEuler() {
		do {
			try calculator.invoke("euler_constant");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber(string: "0.57721566490153286060651209008240243104").decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()));
	}

	func testGoldenRatio() {
		do {
			try calculator.invoke("gr_constant");
		} catch _ {
			XCTFail();
		}
		
		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber(string: "1.61803398874989484820458683436563811772030917980576").decimalNumberRoundedToDigits(withDecimals: stack.getDecimals()));
	}
}

//
//  ModulusTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class XExpYTests: TestSuperclass {
	func testEmpty() {
		do {
			try calculator.invoke("x^y");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}
	}

	func testZeroExp() {
		calculator.stack().push(makeNumber("0.0"));
		calculator.stack().push(makeNumber("19.6"));

		do {
			try calculator.invoke("x^y");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), NSDecimalNumber.one);
	}

	func testDecimalExp() {
		calculator.stack().push(makeNumber("1.3"));
		calculator.stack().push(makeNumber("19.6"));

		do {
			try calculator.invoke("x^y");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("47.85561"));
	}

	func testNegativeExp() {
		calculator.stack().push(makeNumber("-2.0"));
		calculator.stack().push(makeNumber("4.0"));

		do {
			try calculator.invoke("x^y");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("0.0625"));
	}

	func testFourthRoot() {
		calculator.stack().push(makeNumber("0.25"));
		calculator.stack().push(makeNumber("16.0"));

		do {
			try calculator.invoke("x^y");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("2.0"));
	}

	func testInverseFourthRoot() {
		calculator.stack().push(makeNumber("-0.25"));
		calculator.stack().push(makeNumber("16.0"));

		do {
			try calculator.invoke("x^y");
		} catch is BadOperationState {
		} catch let unknownError {
			print("What the heck happened \(unknownError)?");
			XCTFail();
		}

		XCTAssertEqual(calculator.stack().size(), 1);
		XCTAssertEqual(calculator.stack().peek(), makeNumber("0.5"));
	}
}

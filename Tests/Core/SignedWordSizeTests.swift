//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class SignedWordSizeTests: TestSuperclass {
	override func setUp2() {
		stack.setNumericBehavior(.SignedInteger);
	}

	func testInt8() {
		stack.setIntegerWordSize(.byte);

		stack.push(127);
		XCTAssertEqual(127, stack.pop());
		
		stack.push(128);
		XCTAssertEqual(-128, stack.pop());
		
		stack.push(255);
		XCTAssertEqual(-1, stack.pop());
		
		stack.push(256);
		XCTAssertEqual(0, stack.pop());
	}

	func testInt16() {
		stack.setIntegerWordSize(.word_2);

		stack.push(32767);
		XCTAssertEqual(32767, stack.pop());
		
		stack.push(32768);
		XCTAssertEqual(-32768, stack.pop());
		
		stack.push(65535);
		XCTAssertEqual(-1, stack.pop());
		
		stack.push(65536);
		XCTAssertEqual(0, stack.pop());
	}

	func testInt32() {
		stack.setIntegerWordSize(.word_4);
		
		stack.push(2147483647);
		XCTAssertEqual(2147483647, stack.pop());
		
		stack.push(NSDecimalNumber(value: 2147483648 as UInt32));
		XCTAssertEqual(-2147483648, stack.pop());
		
		stack.push(NSDecimalNumber(value: UInt32.max as UInt32));
		XCTAssertEqual(-1, stack.pop());
		
		stack.push(NSDecimalNumber(value: 4294967296 as Int64));
		XCTAssertEqual(0, stack.pop());
	}
	
	func testInt64() {
		stack.setIntegerWordSize(.word_8);
		
		stack.push(NSDecimalNumber(string: "9223372036854775807"));
		XCTAssertEqual(NSDecimalNumber(string: "9223372036854775807"), stack.pop());
		
		stack.push(NSDecimalNumber(string: "9223372036854775808"));
		XCTAssertEqual(NSDecimalNumber(string: "-9223372036854775808"), stack.pop());
		
		stack.push(NSDecimalNumber(string: "18446744073709551615"));
		XCTAssertEqual(NSDecimalNumber(string: "-1"), stack.pop());
		
		stack.push(NSDecimalNumber(string: "18446744073709551616"));
		XCTAssertEqual(0, stack.pop());
	}

	func testSizes()
	{
		stack.setIntegerWordSize(.byte);
			
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		XCTAssertEqual(-1, stack.pop());
			
		stack.setIntegerWordSize(.word_2);
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		XCTAssertEqual(-1, stack.pop());
			
		stack.setIntegerWordSize(.word_4);
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		XCTAssertEqual(-1, stack.pop());
	}
	
	func testStackConversion()
	{
		stack.setIntegerWordSize(.word_8);
		
		// these four should go in unchanged
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		
		// at this point, all four previous entries must be changed to 65535
		stack.setIntegerWordSize(.word_2);
		
		XCTAssertEqual(4, stack.size());
		
		XCTAssertEqual(-1, stack.pop());
		XCTAssertEqual(-1, stack.pop());
		XCTAssertEqual(-1, stack.pop());
		XCTAssertEqual(-1, stack.pop());
	}

	func testWideningConversion()
	{
		stack.setIntegerWordSize(.byte);
		
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		
		XCTAssertEqual(-1, stack.getX());
		XCTAssertEqual(-1, stack.getY());

		stack.setIntegerWordSize(.word_2);
		
		XCTAssertEqual(-1, stack.getX());
		XCTAssertEqual(-1, stack.getY());
		
		stack.setIntegerWordSize(.word_4);
		
		XCTAssertEqual(-1, stack.getX());
		XCTAssertEqual(-1, stack.getY());
		
		stack.setIntegerWordSize(.word_8);
		
		XCTAssertEqual(-1, stack.getX());
		XCTAssertEqual(-1, stack.getY());
	}
	
	func testNarrowingConversion()
	{
		stack.setIntegerWordSize(.word_8);
		
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		
		XCTAssertEqual(-1, stack.getX());
		XCTAssertEqual(-1, stack.getY());
		
		stack.setIntegerWordSize(.word_4);
		
		XCTAssertEqual(-1, stack.getX());
		XCTAssertEqual(-1, stack.getY());
		
		stack.setIntegerWordSize(.word_2);
		
		XCTAssertEqual(-1, stack.getX());
		XCTAssertEqual(-1, stack.getY());

		stack.setIntegerWordSize(.byte);
		
		XCTAssertEqual(-1, stack.getX());
		XCTAssertEqual(-1, stack.getY());
	}
}

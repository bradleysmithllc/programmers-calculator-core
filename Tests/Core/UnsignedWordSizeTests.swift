//
//  DivisionTests.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class UnsignedWordSizeTests: TestSuperclass {
	override func setUp2() {
		stack.setNumericBehavior(.UnsignedInteger);
	}

	func testUInt8() {
		stack.setIntegerWordSize(.byte);

		stack.push(127);
		XCTAssertEqual(127, stack.pop());
		
		stack.push(128);
		XCTAssertEqual(128, stack.pop());

		stack.push(255);
		XCTAssertEqual(255, stack.pop());
		
		stack.push(256);
		XCTAssertEqual(0, stack.pop());
}
	
	func testUInt16() {
		stack.setIntegerWordSize(.word_2);

		stack.push(32767);
		XCTAssertEqual(32767, stack.pop());
		
		stack.push(32768);
		XCTAssertEqual(32768, stack.pop());
		
		stack.push(65535);
		XCTAssertEqual(65535, stack.pop());
		
		stack.push(65536);
		XCTAssertEqual(0, stack.pop());
	}
	
	func testUInt32() {
		stack.setIntegerWordSize(.word_4);
		
		stack.push(2147483647);
		XCTAssertEqual(2147483647, stack.pop());
		
		stack.push(NSDecimalNumber(value: 2147483648 as UInt32));
		XCTAssertEqual(NSDecimalNumber(value: 2147483648 as UInt32), stack.pop());
		
		stack.push(NSDecimalNumber(value: 4294967295 as UInt32));
		XCTAssertEqual(NSDecimalNumber(value: 4294967295 as Int64), stack.pop());
		
		stack.push(NSDecimalNumber(value: 4294967296 as Int64));
		XCTAssertEqual(0, stack.pop());
	}

	func testUInt64() {
		stack.setIntegerWordSize(.word_8);
		
		stack.push(NSDecimalNumber(string: "9223372036854775807"));
		XCTAssertEqual(NSDecimalNumber(string: "9223372036854775807"), stack.pop());
		
		stack.push(NSDecimalNumber(string: "9223372036854775808"));
		XCTAssertEqual(NSDecimalNumber(string: "9223372036854775808"), stack.pop());
		
		stack.push(NSDecimalNumber(string: "18446744073709551615"));
		XCTAssertEqual(NSDecimalNumber(string: "18446744073709551615"), stack.pop());
		
		stack.push(NSDecimalNumber(string: "18446744073709551616"));
		XCTAssertEqual(0, stack.pop());
	}

	func testSizes()
	{
		stack.setIntegerWordSize(.byte);

		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		XCTAssertEqual(255, stack.pop());
		
		stack.setIntegerWordSize(.word_2);
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		XCTAssertEqual(65535, stack.pop());

		stack.setIntegerWordSize(.word_4);
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		XCTAssertEqual(NSDecimalNumber(value: 4294967295 as UInt32), stack.pop());
	}

	func testStackConversion()
	{
		stack.setIntegerWordSize(.word_8);
		
		// these four should go in unchanged
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));

		// at this point, all four previous entries must be changed to 65535
		stack.setIntegerWordSize(.word_2);

		XCTAssertEqual(4, stack.size());

		XCTAssertEqual(65535, stack.pop());
		XCTAssertEqual(65535, stack.pop());
		XCTAssertEqual(65535, stack.pop());
		XCTAssertEqual(65535, stack.pop());
	}
	
	func testWideningConversion()
	{
		stack.setIntegerWordSize(.byte);
		
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		
		XCTAssertEqual(255, stack.getX());
		XCTAssertEqual(255, stack.getY());
		
		stack.setIntegerWordSize(.word_2);
		
		XCTAssertEqual(255, stack.getX());
		XCTAssertEqual(255, stack.getY());
		
		stack.setIntegerWordSize(.word_4);
		
		XCTAssertEqual(255, stack.getX());
		XCTAssertEqual(255, stack.getY());
		
		stack.setIntegerWordSize(.word_8);
		
		XCTAssertEqual(255, stack.getX());
		XCTAssertEqual(255, stack.getY());
	}
	
	func testNarrowingConversion()
	{
		stack.setIntegerWordSize(.word_8);
		
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		stack.push(NSDecimalNumber(value: UInt64.max as UInt64));
		
		XCTAssertEqual(NSDecimalNumber(value: UInt64.max as UInt64), stack.getX());
		XCTAssertEqual(NSDecimalNumber(value: UInt64.max as UInt64), stack.getY());

		stack.setIntegerWordSize(.word_4);

		XCTAssertEqual(NSDecimalNumber(value: UInt32.max as UInt32), stack.getX());
		XCTAssertEqual(NSDecimalNumber(value: UInt32.max as UInt32), stack.getY());

		stack.setIntegerWordSize(.word_2);
		
		XCTAssertEqual(NSDecimalNumber(value: UInt16.max as UInt16), stack.getX());
		XCTAssertEqual(NSDecimalNumber(value: UInt16.max as UInt16), stack.getY());
		
		stack.setIntegerWordSize(.byte);
		
		XCTAssertEqual(NSDecimalNumber(value: UInt8.max as UInt8), stack.getX());
		XCTAssertEqual(NSDecimalNumber(value: UInt8.max as UInt8), stack.getY());
	}
}

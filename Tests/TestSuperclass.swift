//
//  TestSuperclass.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class TestSuperclass: XCTestCase {
	var calculator: Calculator! = CalculatorCore();
	var stack: Stack!;
	let mathHub = MathHub.mathHub();

	func makeRect(originX: Double, originY: Double, width: Double, height: Double) -> CGRect
	{
		let fixX = CGFloat(originX);
		let fixY = CGFloat(originY);
		let cPoint = CGPoint(x: fixX, y: fixY);
		
		return CGRect(origin: cPoint, size: CGSize(width: width, height: height));
	}
	
	func makeNumber(_ number: String) -> NSDecimalNumber {
		return NSDecimalNumber(string: number);
	}

	func makeNumber(integer: Int) -> NSDecimalNumber {
		return NSDecimalNumber(value: integer as Int);
	}

	func makeNumber(long: Int64) -> NSDecimalNumber {
		return NSDecimalNumber(value: long as Int64);
	}

	func makeNumber(digits: [Int], radix: Int) -> NSDecimalNumber {
		var power: Int64 = 1;
		var sum: Int64 = 0;

		let digitsRev = digits.reversed();
		
		for digit in digitsRev
		{
			sum += Int64(digit) * power;

			power = power * Int64(radix);
		}

		return NSDecimalNumber(value: sum as Int64);
	}

	func requiresConfiguration() -> Bool
	{
		return false;
	}

	func setUp2() {}

	final override func setUp() {
		if (requiresConfiguration())
		{
			calculator = CalculatorCore();
		}
		else
		{
			calculator = CalculatorCore();
		}

		stack = calculator.stack();
		
		setUp2();
	}
}

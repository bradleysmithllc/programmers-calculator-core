//
//  Programmers_CalculatorTests.swift
//  Programmers CalculatorTests
//
//  Created by Bradley Smith on 10/21/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core

class CalculatorOperationTests: TestSuperclass {
	func testOperationsEmptyStack() {
		do
		{
			for (operationId, _) in calculator.operations()
			{
				stack.clear();
				try calculator.invoke(operationId);
			}
		}
		catch
		{
			XCTFail();
		}
	}

	func testOperationsZeroStack() {
		do
		{
			for (operationId, _) in calculator.operations()
			{
				stack.clear();
				
				for _ in 0...10 {stack.push(NSDecimalNumber.zero);}

				try calculator.invoke(operationId);
			}
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testOperationsNegativeStack() {
		do
		{
			for (operationId, _) in calculator.operations()
			{
				stack.clear();
				
				for _ in 0...10 {stack.push(NSDecimalNumber(value: -1 as Int));}
				
				try calculator.invoke(operationId);
			}
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testOperationsNSMaxStack() {
		do
		{
			for (operationId, _) in calculator.operations()
			{
				stack.clear();
				
				for _ in 0...10 {stack.push(NSDecimalNumber.maximum);}
				
				try calculator.invoke(operationId);
			}
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testOperationsNSMinStack() {
		do
		{
			for (operationId, _) in calculator.operations()
			{
				stack.clear();

				for _ in 0...10 {stack.push(NSDecimalNumber.minimum);}
				
				try calculator.invoke(operationId);
			}
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testOperationsNaNStack() {
		do
		{
			for (operationId, _) in calculator.operations()
			{
				stack.clear();
				
				for _ in 0...10 {stack.push(NSDecimalNumber.notANumber);}
				
				try calculator.invoke(operationId);
			}
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testOperationsMessageStack() {
		do
		{
			for (operationId, _) in calculator.operations()
			{
				stack.clear();
				
				for _ in 0...10 {stack.push("Message");}
				
				try calculator.invoke(operationId);
			}
		}
		catch
		{
			XCTFail();
		}
	}
	
	func testMonkeyOperationsRandomly() {
		do
		{
			for _ in 0...50
			{
				stack.clear();

				let shuffled = (calculator.operations().keys).sorted(){_, _ in arc4random() % 2 == 0};
				
				for operationId in shuffled
				{
					try calculator.invoke(operationId);
				}
			}
		}
		catch
		{
			XCTFail();
		}
	}
}

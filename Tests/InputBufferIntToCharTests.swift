
//
//  InputBufferTests.swift
//  programmers_calculator_coreTests
//
//  Created by Bradley Smith on 12/28/21.
//  Copyright © 2021 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core


public class InputBufferIntToCharTests: XCTestCase {
	let inputBuffer: InputBufferImpl = InputBufferImpl()
	/* These tests are lame, but they make sure that digit conversion doesn't get messed up */
	func testBase2() throws {
		inputBuffer.applyNumericBase(numericBase: NumericBase.base2);

		let ret1 = try inputBuffer.intToChar(number: 0);
		XCTAssertTrue(ret1 == "0");
		let ret2 = try inputBuffer.intToChar(number: 1);
		XCTAssertTrue(ret2 == "1");
		
		do {
			_ = try inputBuffer.intToChar(number: 2);
			XCTAssertTrue(false);
		} catch (error: StackError.invalidDigitForNumericBase(let numericBase, let digit)) {
			XCTAssertTrue(digit == 2);
			XCTAssertTrue(numericBase == NumericBase.base2);
		}
	}

	func testBase8() throws {
		inputBuffer.applyNumericBase(numericBase: NumericBase.base8);

		for val in 0...7 {
			let ret1 = try inputBuffer.intToChar(number: UInt8(val));
			XCTAssertTrue(String(ret1) == String(val), "actual: " + String(ret1));
		}
		
		do {
			_ = try inputBuffer.intToChar(number: 8);
			XCTAssertTrue(false);
		} catch (error: StackError.invalidDigitForNumericBase(let numericBase, let digit)) {
			XCTAssertTrue(digit == 8);
			XCTAssertTrue(numericBase == NumericBase.base8);
		}
	}

	func testBase10() throws {
		inputBuffer.applyNumericBase(numericBase: NumericBase.base10);

		for val in 0...9 {
			let ret1 = try inputBuffer.intToChar(number: UInt8(val));
			XCTAssertTrue(String(ret1) == String(val), "actual: " + String(ret1));
		}
		
		do {
			_ = try inputBuffer.intToChar(number: 11);
			XCTAssertTrue(false);
		} catch (error: StackError.invalidDigitForNumericBase(let numericBase, let digit)) {
			XCTAssertTrue(digit == 11);
			XCTAssertTrue(numericBase == NumericBase.base10);
		}
	}

	func testBase16() throws {
		inputBuffer.applyNumericBase(numericBase: NumericBase.base16);

		for val in 0...9 {
			let ret1 = try inputBuffer.intToChar(number: UInt8(val));
			XCTAssertTrue(String(ret1) == String(val), "actual: " + String(ret1));
		}
		
		var ret1 = try inputBuffer.intToChar(number: 10);
		XCTAssertTrue(ret1 == "A", "actual: " + String(ret1));
		ret1 = try inputBuffer.intToChar(number: 11);
		XCTAssertTrue(ret1 == "B", "actual: " + String(ret1));
		ret1 = try inputBuffer.intToChar(number: 12);
		XCTAssertTrue(ret1 == "C", "actual: " + String(ret1));
		ret1 = try inputBuffer.intToChar(number: 13);
		XCTAssertTrue(ret1 == "D", "actual: " + String(ret1));
		ret1 = try inputBuffer.intToChar(number: 14);
		XCTAssertTrue(ret1 == "E", "actual: " + String(ret1));
		ret1 = try inputBuffer.intToChar(number: 15);
		XCTAssertTrue(ret1 == "F", "actual: " + String(ret1));

		do {
			_ = try inputBuffer.intToChar(number: 16);
			XCTAssertTrue(false);
		} catch (error: StackError.invalidDigitForNumericBase(let numericBase, let digit)) {
			XCTAssertTrue(digit == 16);
			XCTAssertTrue(numericBase == NumericBase.base16);
		}
	}

	func test1() throws {
		let ret = try inputBuffer.intToChar(number: 1);
		XCTAssertTrue(ret == "1");
	}

	func test2() throws {
		let ret = try inputBuffer.intToChar(number: 2);
		XCTAssertTrue(ret == "2");
	}

	func test3() throws {
		let ret = try inputBuffer.intToChar(number: 3);
		XCTAssertTrue(ret == "3");
	}

	func test4() throws {
		let ret = try inputBuffer.intToChar(number: 4);
		XCTAssertTrue(ret == "4");
	}

	func test5() throws {
		let ret = try inputBuffer.intToChar(number: 5);
		XCTAssertTrue(ret == "5");
	}

	func test6() throws {
		let ret = try inputBuffer.intToChar(number: 6);
		XCTAssertTrue(ret == "6");
	}

	func test7() throws {
		let ret = try inputBuffer.intToChar(number: 7);
		XCTAssertTrue(ret == "7");
	}

	func test8() throws {
		let ret = try inputBuffer.intToChar(number: 8);
		XCTAssertTrue(ret == "8");
	}

	func test9() throws {
		let ret = try inputBuffer.intToChar(number: 9);
		XCTAssertTrue(ret == "9");
	}
}

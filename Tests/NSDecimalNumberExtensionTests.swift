//
//  InputBufferTests.swift
//  programmers_calculator_coreTests
//
//  Created by Bradley Smith on 12/28/21.
//  Copyright © 2021 Bradley Smith, LLC. All rights reserved.
//

import XCTest
@testable import programmers_calculator_core


public class NSDecimalNumberExtensionTests: XCTestCase {
	public func testNegating() {
		XCTAssertEqual(NSDecimalNumber(12).decimalNumberByNegating(), NSDecimalNumber(-12))
		XCTAssertEqual(NSDecimalNumber(10000.000201).decimalNumberByNegating(), NSDecimalNumber(-10000.000201))
	}

	public func testTruncating() {
		XCTAssertEqual(NSDecimalNumber(12).decimalNumberTruncated(), NSDecimalNumber(12))
		XCTAssertEqual(NSDecimalNumber(10000.000201).decimalNumberTruncated(), NSDecimalNumber(10000))
	}

	public func testAbs() {
		XCTAssertEqual(NSDecimalNumber(12).decimalNumberWithAbsoluteValue(), NSDecimalNumber(12))
		XCTAssertEqual(NSDecimalNumber(-21).decimalNumberWithAbsoluteValue(), NSDecimalNumber(21))
		XCTAssertEqual(NSDecimalNumber(-10000.0002).decimalNumberWithAbsoluteValue(), NSDecimalNumber(10000.0002))
		XCTAssertEqual(NSDecimalNumber(10000.0002).decimalNumberWithAbsoluteValue(), NSDecimalNumber(10000.0002))
	}

	public func testRounding() {
		XCTAssertEqual(NSDecimalNumber(12).decimalNumberRoundedToDigits(withDecimals: 2), NSDecimalNumber(12))
		XCTAssertEqual(NSDecimalNumber(12.1212).decimalNumberRoundedToDigits(withDecimals: 2), NSDecimalNumber(12.12))
		XCTAssertEqual(NSDecimalNumber(-12).decimalNumberRoundedToDigits(withDecimals: 2), NSDecimalNumber(-12))
		XCTAssertEqual(NSDecimalNumber(-12.1212).decimalNumberRoundedToDigits(withDecimals: 2), NSDecimalNumber(-12.12))
	}

	public func testBase() {
		XCTAssertEqual(
			NSDecimalNumber(12).formattedNumberInBase(),
			"12"
		);

		XCTAssertEqual(
			NSDecimalNumber(12).formattedNumberInBase(numericBase: NumericBase.base2, numericBehavior: NumericBehavior.SignedInteger),
			"1100"
		);

		XCTAssertEqual(
			NSDecimalNumber(12).formattedNumberInBase(numericBase: NumericBase.base2, numericBehavior: NumericBehavior.UnsignedInteger),
			"1100"
		);

		XCTAssertEqual(
			NSDecimalNumber(12.1).formattedNumberInBase(numericBase: NumericBase.base16, numericBehavior: NumericBehavior.SignedInteger),
			"C"
		);

		XCTAssertEqual(
			NSDecimalNumber(12.1).formattedNumberInBase(numericBase: NumericBase.base16, numericBehavior: NumericBehavior.UnsignedInteger),
			"C"
		);

		XCTAssertEqual(
			NSDecimalNumber(12.15).formattedNumberInBase(numericBase: NumericBase.base8, numericBehavior: NumericBehavior.SignedInteger),
			"14"
		);

		XCTAssertEqual(
			NSDecimalNumber(12.15).formattedNumberInBase(numericBase: NumericBase.base8, numericBehavior: NumericBehavior.UnsignedInteger),
			"14"
		);
	}
	
	let numericTestArray: [(
		inputNumber: String,
		expectedResults: [(
			numericBase: NumericBase,
			behaviorResults: [(
				numericBehavior: NumericBehavior,
				result: String,
				separatedResult: String
			)]
		)]
	)] = [
		(
			inputNumber: "1897654321",
			expectedResults: [
				(
					numericBase: NumericBase.base2,
					behaviorResults: [
						(numericBehavior: NumericBehavior.SignedInteger, result: "1110001000110111110100000110001", separatedResult: "1110001,00011011,11101000,00110001"),
						(numericBehavior: NumericBehavior.UnsignedInteger, result: "1110001000110111110100000110001", separatedResult: "1110001,00011011,11101000,00110001")
					]
				),
				(
					numericBase: NumericBase.base8,
					behaviorResults: [
						(numericBehavior: NumericBehavior.SignedInteger, result: "16106764061", separatedResult: "161,0676,4061"),
						(numericBehavior: NumericBehavior.UnsignedInteger, result: "16106764061", separatedResult: "161,0676,4061")
					]
				),
				(
					numericBase: NumericBase.base10,
					behaviorResults: [
						(numericBehavior: NumericBehavior.Decimal, result: "1897654321", separatedResult: "1,897,654,321")
					]
				),
				(
					numericBase: NumericBase.base16,
					behaviorResults: [
						(numericBehavior: NumericBehavior.SignedInteger, result: "711BE831", separatedResult: "711B,E831"),
						(numericBehavior: NumericBehavior.UnsignedInteger, result: "711BE831", separatedResult: "711B,E831")
					]
				)
			]
		),
		(
			inputNumber: "-1897654321",
			expectedResults: [
				(
					numericBase: NumericBase.base2,
					behaviorResults: [
						(numericBehavior: NumericBehavior.SignedInteger, result: "-1110001000110111110100000110001", separatedResult: "-1110001,00011011,11101000,00110001"),
						(numericBehavior: NumericBehavior.UnsignedInteger, result: "1111111111111111111111111111111110001110111001000001011111001111", separatedResult: "11111111,11111111,11111111,11111111,10001110,11100100,00010111,11001111")
					]
				),
				(
					numericBase: NumericBase.base8,
					behaviorResults: [
						(numericBehavior: NumericBehavior.SignedInteger, result: "-16106764061", separatedResult: "-161,0676,4061"),
						(numericBehavior: NumericBehavior.UnsignedInteger, result: "1777777777761671013717", separatedResult: "17,7777,7777,7616,7101,3717")
					]
				),
				(
					numericBase: NumericBase.base10,
					behaviorResults: [
						(numericBehavior: NumericBehavior.Decimal, result: "-1897654321", separatedResult: "-1,897,654,321")
					]
				),
				(
					numericBase: NumericBase.base16,
					behaviorResults: [
						(numericBehavior: NumericBehavior.SignedInteger, result: "-711BE831", separatedResult: "-711B,E831"),
						(numericBehavior: NumericBehavior.UnsignedInteger, result: "FFFFFFFF8EE417CF", separatedResult: "FFFF,FFFF,8EE4,17CF")
					]
				)
			]
		)
	];

	public func testSeparators() {
		for testCase in numericTestArray {
			let decimalValue = NSDecimalNumber(string: testCase.inputNumber);
			
			// test 4 dfifferent conversions for each base
			for numericBaseResult in testCase.expectedResults {
				for behaviorCase in numericBaseResult.behaviorResults {
					let result1 = decimalValue.formattedNumberInBase(numericBase: numericBaseResult.numericBase, numericBehavior: behaviorCase.numericBehavior);
					XCTAssertEqual(behaviorCase.result, result1, "\(testCase.inputNumber) \(numericBaseResult.numericBase) \(behaviorCase.numericBehavior)");
					let result2 = decimalValue.formattedNumberInBase(numericBase: numericBaseResult.numericBase, numericBehavior: behaviorCase.numericBehavior, applySeparator: true);
					XCTAssertEqual(behaviorCase.separatedResult, result2, "\(testCase.inputNumber) \(numericBaseResult.numericBase)  \(behaviorCase.numericBehavior) separated")
				}
			}
		}
	}
}

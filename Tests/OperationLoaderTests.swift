//
//  OperationLoaderTests.swift
//  programmers_calculator_core
//
//  Created by Bradley Smith on 4/27/17.
//  Copyright © 2017 Bradley Smith, LLC. All rights reserved.
//

@testable import programmers_calculator_core
import XCTest

class OperationLoaderTests: XCTestCase {
	override func setUp() {
			super.setUp()
			// Put setup code here. This method is called before the invocation of each test method in the class.
	}
	
	override func tearDown() {
			// Put teardown code here. This method is called after the invocation of each test method in the class.
			super.tearDown()
	}
	
	func testLoadAdditionClass()
	{
		XCTAssertTrue(OperationLoader.loadOperationClassByName(name: "Addition") == AdditionOperation.self);
	}
	
	func testLoadDivisionClass()
	{
		XCTAssertTrue(OperationLoader.loadOperationClassByName(name: "Division") == DivisionOperation.self);
	}
	
	/*
	Verify that the system-defined compound operations are loading.
	*/
	func testLoadCompoundOperations()
	{
		// there are 13 known at this time
		XCTAssertTrue(OperationLoader.loadCompoundOperations(calculator: nil).count >= 13);
	}
}

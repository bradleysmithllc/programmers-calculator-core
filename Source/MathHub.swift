//
//  MathHub.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/20/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import Darwin

open class MathHub {
	private static var _mathHub: MathHub = MathHub();
	
	public static func mathHub() -> MathHub
	{
		return _mathHub;
	}

	private init()
	{
	}
	
	private var mathHandler: MathHandler = MathHandler();
	
	public func resetMessages()
	{
		return mathHandler.exceptionStack.removeAll();
	}

	public func lastMessage() -> (msg: String, failure: Bool)!
	{
		return mathHandler.exceptionStack.isEmpty ? nil : mathHandler.exceptionStack[mathHandler.exceptionStack.endIndex - 1];
	}

	open func modulus(base: NSDecimalNumber, modulus: NSDecimalNumber) -> NSDecimalNumber
	{
		if (modulus == NSDecimalNumber.zero) {
			return NSDecimalNumber.notANumber;
		} else {
			let quotient: NSDecimalNumber = divide(dividend: base, divisor: modulus);
			
			let integralQuotient: NSDecimalNumber = NSDecimalNumber(value: quotient.int32Value as Int32);
			
			let subtractAmount: NSDecimalNumber = multiply(left: integralQuotient, right: modulus);
			let remainder: NSDecimalNumber = subtract(left: base, right: subtractAmount);
			
			return remainder;
		}
	}

	// Add two numbers represented by the strings op1 and op2.  Return NaN
	// if a bad exception occurs.
	open func add(left: NSDecimalNumber, right: NSDecimalNumber) -> NSDecimalNumber {
		// Init badException to false.  It will be set to true if an
		// overflow, underflow, or divide by zero exception occur.
		mathHandler.reset();

		// Add the NSDecimalNumbers, passing ourselves as the implementor
		// of the NSDecimalNumbersBehaviors protocol.
		let dn3 = left.adding(right, withBehavior: mathHandler);

		// Return nil if a bad exception happened, otherwise return the result
		// of the add.
		return mathHandler.badException ? NSDecimalNumber.notANumber : dn3;
	}

	// divide dividend by the divisor.  Return NaN
	// if a bad exception occurs.
	open func divide(dividend: NSDecimalNumber, divisor: NSDecimalNumber) -> NSDecimalNumber {
		// Init badException to false.  It will be set to true if an
		// overflow, underflow, or divide by zero exception occur.
		mathHandler.reset();

		// Add the NSDecimalNumbers, passing ourselves as the implementor
		// of the NSDecimalNumbersBehaviors protocol.
		let dn3 = dividend.dividing(by: divisor, withBehavior: mathHandler);

		// Return nil if a bad exception happened, otherwise return the result
		// of the add.
		return mathHandler.badException ? NSDecimalNumber.notANumber : dn3;
	}

	// multiply left * right.  Return NaN
	// if a bad exception occurs.
	open func multiply(left: NSDecimalNumber, right: NSDecimalNumber) -> NSDecimalNumber {
		// Init badException to false.  It will be set to true if an
		// overflow, underflow, or divide by zero exception occur.
		mathHandler.reset();

		// Add the NSDecimalNumbers, passing ourselves as the implementor
		// of the NSDecimalNumbersBehaviors protocol.
		let dn3 = left.multiplying(by: right, withBehavior: mathHandler);

		// Return nil if a bad exception happened, otherwise return the result
		// of the add.
		return mathHandler.badException ? NSDecimalNumber.notANumber : dn3;
	}

	// subtract right from left.  Return NaN
	// if a bad exception occurs.
	open func subtract(left: NSDecimalNumber, right: NSDecimalNumber) -> NSDecimalNumber {
		// Init badException to false.  It will be set to true if an
		// overflow, underflow, or divide by zero exception occur.
		mathHandler.reset();

		// Add the NSDecimalNumbers, passing ourselves as the implementor
		// of the NSDecimalNumbersBehaviors protocol.
		let dn3 = left.subtracting(right, withBehavior: mathHandler);

		// Return nil if a bad exception happened, otherwise return the result
		// of the add.
		return mathHandler.badException ? NSDecimalNumber.notANumber : dn3;
	}

	// Raise base to the exponent (exp).  Negative exponents are handled
	// if a bad exception occurs.
	open func power(base: NSDecimalNumber, exponent: NSDecimalNumber) -> NSDecimalNumber {
		// Init badException to false.  It will be set to true if an
		// overflow, underflow, or divide by zero exception occur.
		mathHandler.reset();

		// handle a couple of special cases here.
		// check for 0 exponent
		if (exponent.isEqual(to: NSDecimalNumber.zero)) {
			return NSDecimalNumber.one;
		}
				// check for a whole number exponent
		else if exponent.isEqual(to: NSDecimalNumber(value: exponent.int32Value as Int32)) {
			let integerExp: Int = exponent.intValue;

			let result: NSDecimalNumber = base.raising(toPower: abs(integerExp), withBehavior: mathHandler);

			// check for negative exponent
			if (integerExp < 0) {
				// divide 1 / result
				return divide(dividend: NSDecimalNumber.one, divisor: result);
			} else {
				return result;
			}
		}

		// Convert both operands to Double, then invoke pow, then wrap back up in an NSDecimalNumber
		let dn3 = NSDecimalNumber(value: pow(base.doubleValue, exponent.doubleValue) as Double);

		// Return nil if a bad exception happened, otherwise return the result
		// of the add.
		return mathHandler.badException ? NSDecimalNumber.notANumber : dn3;
	}

	let radiansConversionFactor = NSDecimalNumber(value: Double.pi).dividing(by: NSDecimalNumber(value: 180 as Int32));

	func radiansFromDegrees(degrees: NSDecimalNumber) -> NSDecimalNumber {
		return multiply(left: degrees, right: radiansConversionFactor);
	}
	
	let degreesConversionFactor = NSDecimalNumber(value: 180 as Int).dividing(by: NSDecimalNumber(value: Double.pi));

	func degreesFromRadians(radians: NSDecimalNumber) -> NSDecimalNumber {
		return multiply(left: radians, right: degreesConversionFactor);
	}
	
	let degreesFromGradiansConversionFactor = NSDecimalNumber(value: 180 as Int).dividing(by: NSDecimalNumber(value: 200 as Int));
	
	func degreesFromGradians(gradians: NSDecimalNumber) -> NSDecimalNumber {
		return multiply(left: gradians, right: degreesFromGradiansConversionFactor);
	}

	let gradiansFromRadiansConversionFactor = NSDecimalNumber(value: 200 as Int).dividing(by: NSDecimalNumber(value: Double.pi));
	
	func gradiansFromRadians(radians: NSDecimalNumber) -> NSDecimalNumber {
		return multiply(left: radians, right: gradiansFromRadiansConversionFactor);
	}
	
	let gradiansFromDegreesConversionFactor = NSDecimalNumber(value: 200 as Int).dividing(by: NSDecimalNumber(value: 180 as Int));
	
	func gradiansFromDegrees(degrees: NSDecimalNumber) -> NSDecimalNumber {
		return multiply(left: degrees, right: gradiansFromDegreesConversionFactor);
	}
	
	let radiansFromGradiansConversionFactor = NSDecimalNumber(value: Double.pi).dividing(by: NSDecimalNumber(value: 200 as Int));

	func radiansFromGradians(gradians: NSDecimalNumber) -> NSDecimalNumber {
		return multiply(left: gradians, right: radiansFromGradiansConversionFactor);
	}
}

class MathHandler: NSDecimalNumberBehaviors {
	var badException = false;
	var exceptionStack: [(msg: String, failure: Bool)] = [];

	func reset() {
		badException = false;
	}

	// Required function of NSDecimalNumberBehaviors protocol
	@objc func roundingMode() -> NSDecimalNumber.RoundingMode {
		return NSDecimalNumber.RoundingMode.plain;
	}

	// Required function of NSDecimalNumberBehaviors protocol
	@objc func scale() -> CShort {
		return CShort(NSDecimalNoScale);
	}

	// Required function of NSDecimalNumberBehaviors protocol
	// Here we process the exceptions
	@objc func exceptionDuringOperation(_ operation: Selector, error: NSDecimalNumber.CalculationError, leftOperand: NSDecimalNumber, rightOperand: NSDecimalNumber?) -> NSDecimalNumber? {
		var errorstr = "";

		switch (error) {
		case NSDecimalNumber.CalculationError.noError:
			errorstr = "NoError";
		case NSDecimalNumber.CalculationError.lossOfPrecision:
			errorstr = "LossOfPrecision";
			exceptionStack.append((msg: errorstr, failure: true));
		case NSDecimalNumber.CalculationError.underflow:
			errorstr = "Underflow";
			badException = true;
			exceptionStack.append((msg: errorstr, failure: badException));
		case NSDecimalNumber.CalculationError.overflow:
			errorstr = "Overflow";
			badException = true;
			exceptionStack.append((msg: errorstr, failure: badException));
		case NSDecimalNumber.CalculationError.divideByZero:
			errorstr = "DivideByZero"
			badException = true;
			exceptionStack.append((msg: errorstr, failure: badException));
        default:
            break;
		}

		debugPrint("Exception called for operation \(operation) -> \(errorstr)");

		return nil
	}
}

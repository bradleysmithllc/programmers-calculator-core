//
//  DefaultProfile.swift
//  Programmers Calculator
//
//  Created by Bradley Smith on 10/25/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

open class JSONCompoundOperation: CompoundOperation
{
	fileprivate var stackEffect: [StackAffect]?;

	public init(calculator: Calculator!, id: String, url: URL)
	{
		let data = try! Data(contentsOf: url)
		
            let json = try! JSON(data: data);

            let desc = json["description"].string!;

            if let se = json["stack-affect"].array
            {
                stackEffect = [StackAffect]();

                for ef in se
                {
                    stackEffect!.append(StackAffect.mapValues[ef.string!]!)
                }
            }

            let formula: String? = json["formula"].string;

            let mnemonic = json["mnemonic"].string!;

            super.init(id: id, mnemonic: mnemonic, description: desc, formula: formula);

            // pull out the defaults
            for operationName in json["operations"].array!
            {
                let opid = operationName.string!
                
                // check for a string of numeric literals.
                let index = opid.range(of: "#")

                if index?.lowerBound == opid.startIndex
                {
                    // strip the optional here, otherwise the next function call looks horrible
                    let indexNotOptional = index!;

                    let subStr = String(opid[indexNotOptional.upperBound...]);
                    
                    // iterate over the rest of the string after the # and add as single character operation ids
                    for character in subStr
                    {
                        addOperation(String(character));
                    }
                }
                else
                {
                    addOperation(opid);
                }
            }
	}

	public required init(calculator: Calculator!) {
	    fatalError("init(calculator:) has not been implemented")
	}

	open override func stackAffect() -> [StackAffect]?
	{
		return stackEffect;
	}
}

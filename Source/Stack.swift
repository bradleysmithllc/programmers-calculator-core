//
//  Stack.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**A single element on the stack.  Currently can hold a number or a string.
*/
public struct StackElement
{
	fileprivate let _message: String?;
	fileprivate var _number: NSDecimalNumber?;

	public init(message: String)
	{
		_message = message;
		_number = nil;
	}

	public init(number: NSDecimalNumber)
	{
		_message = nil;
		_number = number;
	}
	
	init()
	{
		_message = nil;
		_number = nil;
	}

	/**Call isMessage before calling this!  It will force-unwrap the optional
	*/
	public func message() -> String
	{
		return _message!;
	}

	/**In keeping with the contract that the stack is infinite and every element not
	  *explicitly assigned a value contains 0, this method will return 0 if it
	  *represents nil, notANumber if it represents a message, or the number itself.
	  */
	public func number() -> NSDecimalNumber
	{
		if isNil
		{
			return NSDecimalNumber.zero;
		}
		else if isMessage
		{
			return NSDecimalNumber.notANumber;
		}
		else
		{
			return _number!;
		}
	}
	
	mutating func swapNumber(_ number: NSDecimalNumber)
	{
		_number = number;
	}

	public var isNil: Bool {
		get
		{
			return !isMessage && !isNumber;
		}
	}

	public var isNaN: Bool {
		return number() == NSDecimalNumber.notANumber
	}

	public var isMessage: Bool {
		get
		{
			return _message != nil;
		}
	}
	
	public var isNumber: Bool {
		get
		{
			return _number != nil;
		}
	}
}

let _nilElement: StackElement = StackElement();

public enum LiteralDigit {
	case _0
	case _1
	case _2
	case _3
	case _4
	case _5
	case _6
	case _7
	case _8
	case _9
	case _10
	case _11
	case _12
	case _13
	case _14
	case _15
	case decimalPoint
}

/*A stack of operations constituting application state.  The stack maintains a real size and data values,
 *but at read time every operation will return at least the number 0.  The size function reflects
 *the actual size of operands.
 */
public protocol Stack {
	var _nil: StackElement { get };

	/* Method removes all NaN and message entries from the stack, starting with the head element (X) and moving upwards until a numeric non-NaN entry is encountered. */
	func purgeNaNAndMessages();

	/**Push an element onto the stack.
    */
	func push(_ decNum: NSDecimalNumber);

	/**Push a message onto the stack.
	*/
	func push(_ message: String);
	
	/**Push a message onto the stack.
	*/
	func push(_ element: StackElement);
	
	/**Push an element onto the stack, but at the end, not at the head.
	*/
	func pushAtEnd(_ decNum: NSDecimalNumber);
	
	/**Push a message onto the stack, but at the end, not at the head.
	*/
	func pushAtEnd(_ message: String);
	
	/**Push an element onto the stack, but at the end, not at the head.
	*/
	func pushAtEnd(_ element: StackElement);
	
	/* Pops the most recently pushed element from the stack.
	*/
	func popAll() -> [StackElement];

	/* Pops the most recently pushed element from the stack.
	*/
	func pop() -> NSDecimalNumber;
	
	/* Pops the most recently pushed element from the stack.
	*/
	func popElement() -> StackElement;

	/* Removes the oldest element on the stack, I.E., the last one that would normally be popped.
	*/
	func popOldest() -> StackElement;

	/*The actual stack size.
	*/
	func size() -> Int;

	/** True if size == 0
	*/
	func empty() -> Bool;

	/*Peek at a specified location.  Will return 0 if there is nothing there.
	*/
    func peek(_ index: Int) -> NSDecimalNumber;
	
	/*Peek at a specified location.  Will return _nil if there is nothing there.
	*/
	func peekElement(_ index: Int) -> StackElement;

	/*Peek at the last operand.  Will return 0 if there is nothing there.
	*/
    func peek() -> NSDecimalNumber;

	/**Native peek function.
	*/
	func peekElement() -> StackElement;

	/*Clear actual stack state.
	 */
	func clear();

	/*Sets the value of the X register.  The X register is always the last objet on the stack, I.E.,
	 *the last operand pushed or the next one to be popped.
	 */
	func setX(_ number: NSDecimalNumber);
	
	/*Returns the contents of the X register, or 0.
	*/
	func getX() -> NSDecimalNumber;
	
	/*Returns the contents of the X register, or 0.
	*/
	func getXElement() -> StackElement;
	
	/*Gets the contents of the Y register.
	*/
	func getY() -> NSDecimalNumber;
	
	/*Gets the contents of the Y register.
	*/
	func getYElement() -> StackElement;
	
	/*Returns the contents of the Z register, or 0.
	*/
	func getZ() -> NSDecimalNumber;
	
	/*Returns the contents of the Z register, or 0.
	*/
	func getZElement() -> StackElement;
	
	/*Returns the contents of the T register, or 0.
	*/
	func getT() -> NSDecimalNumber;
	
	/*Returns the contents of the T register, or 0.
	*/
	func getTElement() -> StackElement;

	/**Gets the index register - a register that is not stack based and is used
	  *for indirection.
	  */
	func getIndex() -> NSDecimalNumber;
	
	/**Sets the index register.
	*/
	func setIndex(index: NSDecimalNumber);
	
	/**
	* A map of properties which represent calculator state, E.G., numeric base, etc.
	*/
	func properties() -> [String:Any];
	
	/** Read a property using the inferred type.  It is the callers responsibility to know
	* the type beforehand.
	*/
	func property<T>(_ propertyName: String) -> T?;
	
	/** Read a property using the inferred type.  It is the callers responsibility to know
	* the type beforehand.  If not set, use the default value as an in initial
	*/
	func propertyWithDefault<T>(_ propertyName: String, defaultValue: () -> T) -> T;
	
	/** Write a property.  The newly written property value is returned and is guaranteed to be the same object as the
	*  supplied.
	*/
	func setProperty<T>(_ propertyName: String, propertyValue: T) -> T;
	
	/** Clear a property.  Returns the existing value, if any.
	*/
	func clearProperty<T>(_ propertyName: String) -> T?;
	
	/**
	* A map of properties which represent calculator state, E.G., numeric base, etc.
	*/
	func stateProperties() -> [String:Any];
	
	/** Read a property using the inferred type.  It is the callers responsibility to know
	* the type beforehand.
	*/
	func stateProperty<T>(_ propertyName: String) -> T?;
	
	/** Read a property using the inferred type.  It is the callers responsibility to know
	* the type beforehand.  If not set, use the default value as an in initial
	*/
    func statePropertyWithDefault<T>(_ propertyName: String, defaultValue: () -> T) -> T;
	
	/** Write a property.  The newly written property value is returned and is guaranteed to be the same object as the
	*  supplied.
	*/
	func setStateProperty<T>(_ propertyName: String, propertyValue: T) -> T;
	
	/** Clear a property.  Returns the existing value, if any.
	*/
	func clearStateProperty<T>(_ propertyName: String) -> T?;

	/**
	* These are named memory areas.  Data is stored by a label for later use.
	*/
	func memory() -> [String:NSDecimalNumber]!;
	
	/** Read memory by name.
	*/
	func memory(_ slotName: String) -> NSDecimalNumber;
	
	/** Read memory by name.  Assign the default value if it is not set.
	*/
	mutating func memoryWithDefault(_ slotName: String, initialValue: NSDecimalNumber) -> NSDecimalNumber;
	
	/** Write to a named memory slot.
	*/
	mutating func assignMemory(_ slotName: String, value: NSDecimalNumber) -> NSDecimalNumber;
	
	/** Clear a memory slot.
	*/
	mutating func clearMemory(_ slotName: String) -> NSDecimalNumber?;

	/**Create a checkpoint which saves all state.
    */
	func checkPoint();
	
	/**Reverts to a previous checkpoint.  If there is none, this operation does nothing.
	*/
	func revertToCheckPoint();

	/**Tells checkpoints not to store more than depth states.
	*/
	func setCheckPointDepth(_ depth: Int);
	
	/**Size of the current integer mode.
	*/
	func getIntegerWordSize() -> IntegerWordSize;
	
	/**Size of the current integer mode.
	*/
	func setIntegerWordSize(_ size: IntegerWordSize);
	
	/**Decimal, or integer sign mode.
	*/
	func getNumericBehavior() -> NumericBehavior;
	
	/**Decimal, or integer sign mode.
	*/
	func setNumericBehavior(_ behavior: NumericBehavior);
	
	/**Gets the number of decimals to round to.
	*/
	func getDecimals() -> UInt8;
	
	/**Set the number of significant decimal places
	*/
	func setDecimals(_ num: UInt8);
	
	/**Gets the numeric base for data input and display.
	*/
	func getNumericBase() -> NumericBase;
	
	/**Set the numeric base.
	*/
	func setNumericBase(base: NumericBase);
	
	/**Reverses the sign of the current numeric entry
	*/
	func flipNumericEntrySignBit();
	
	/**Appends a digit to the numeric entry.  Use Calculator.decimalPoint for the decimal point.
	*/
	func addDigitToNumericEntry(_ digit: LiteralDigit) throws;
	
	/**Are we currently in numeric entry?
	*/
	func inNumericEntry() -> Bool;
	func numericEntryState() -> NumericEntryState;
	
	/**Clears the last added digit, or decimal point.  Does nothing once the end of input is reached.
	*/
	func clearLastDigitInNumericEntry();
	
	func getNumericEntrySignBit() -> Bool;
	
	/**Resets the data input buffer to default.
	*/
	func resetInputBuffer();
	
	/**Gets the current value of the numeric entry
	*/
	func currentInputNumericValue() -> NSDecimalNumber;
	
	/**Gets the current image of the numeric input.
	*/
	func currentInputBuffer() -> InputBuffer;
	
	/**Gets a printable version of the current image.  I.E., no blank strings, ".0", etc.
	*/
	func printableInputImage() -> String;
	
	/**Determine if the current image has a decimal applied.
	*/
	func currentInputBufferIncludesDecimal() -> Bool;
	
	func reset();
}

extension Stack
{
 public var _nil: StackElement {
		get
		{
			return _nilElement;
		}
	}
}

public enum StackError: Error {
	case invalidDigit(value: UInt8)
	case invalidDigitForNumericBase(numericBase: NumericBase, digit: UInt8)
	case invalidCharacterForDigit(character: Character)
}


/* Class holds the current state of input */
public protocol InputBuffer {
	/* If this is not the current numeric base, change to match this base.  This will not change the number, with the exception that if it ias base 10, and contains a decimal portion, the decimal part is truncated.*/
	func applyNumericBase(numericBase: NumericBase);
	func numericBase() -> NumericBase;

	/* Literal '.' has been pushed into the buffer */
	func hasDecimal() -> Bool;

	/* Literal '-' has been pushed into the buffer */
	func isNegated() -> Bool;
	
	/* Sets the negative state.  Either to negative if negative == true, or positivce otherwise. This can be called indefinitely without consequence */
	func negativeState(negative: Bool);

	/* Appends a decimal point at the current insertion.  If the decimal is already applied it raises an exception */
	func applyDecimal();

	/* Appends this digit to the current buffer.  Must be in the range [0, 15] - otherwise an exception is raised */
	func appendDigit(value: UInt8);

	/* Retrieves the integer part of the buffer, if any.  Returns a 0-length array if not present. */
	func hasIntegerPortion() -> Bool;
	func integralBuffer() -> [UInt8];
	/* Same as integralBuffer */
	func operativeIntegralDigits() -> [UInt8];

	/* Retrieves the decimal part of the buffer, if any.  Returns a 0-length array if not present */
	func hasDecimalPortion() -> Bool;
	func decimalBuffer() -> [UInt8];
	/* Gets the decimal buffer with all trailing 0s removed, or an empty array if there were no non-0 digits */
	func operativeDecimalDigits() -> [UInt8];

	/* Returns the closest number built so far.  E.G., a blank buffer is 0.  -0 is 0.  -.0 is 0.  00000000.0000000 is 0.  1.000000 is 1.  0000000.1 is 0.1, - is 0, etc.*/
	func numericValue() -> NSDecimalNumber;

	/* Returns the buffer as a properly formatted number.  numericValue() formatted. Note that leading 0s in the integer part, and trailing 0s in the decima are ignored. */
	func formattedTextBuffer(applySeparators: Bool) -> String;
	/*  Default is to use separators */
	func formattedTextBuffer() -> String;

	/* Gets the literal buffer as text, in the given numeric base and separator style.  In this case, - yields '0', empty buffer yields '0', . yields '0.'.,  */
	func progressTextBuffer(applySeparators: Bool) -> String;
	/*  Default is to use separators */
	func progressTextBuffer() -> String;
	
	/* Removes the last digit added, including if the last 'digit' was to apply the decimal point.  Affects the integral or decimal portion.  Does not affect the sign */
	func removeLastDigitOrDecimal();
	
	/* Create a new copy of this buffer with the same internal state as this one */
	func deepCloneInternalState() -> InputBuffer;
	
	/* Resets the digits in this buffer.  Clears the sign, integral and decimal portions, as well as the decimla point. */
	func resetDigits();
}

public class InputBufferImpl : InputBuffer
{
	var _hasDecimal: Bool = false;
	var _negated: Bool = false;
	var _numericBase: NumericBase = NumericBase.base10;

	var integralArray: [UInt8] = [];
	var decimalArray: [UInt8] = [];

	public func numericBase() -> NumericBase {
		return _numericBase;
	}

	public func applyNumericBase(numericBase: NumericBase) {
		if (numericBase != _numericBase) {
			// grab the current value to recreate in the other base
			var currentValue = numericValue();

			if (numericBase != NumericBase.base10) {
				// We are not going to base10, which means we might not be base 10 now.  Just in case truncate any decimal portion
				currentValue = currentValue.rounding(accordingToBehavior: NSDecimalNumberHandler(roundingMode: NSDecimalNumber.RoundingMode.down, scale: 0, raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: false))
			}

			// Record new base
			_numericBase = numericBase;

			// grab current value in the new base
			let formattedCurrentValueInNewBase: String;
			let requiredNumericBehavior: NumericBehavior;

			switch (_numericBase) {
				case .base2:
					requiredNumericBehavior = NumericBehavior.SignedInteger;
				case .base8:
					requiredNumericBehavior = NumericBehavior.SignedInteger;
				case .base10:
					requiredNumericBehavior = NumericBehavior.Decimal;
				case .base16:
					requiredNumericBehavior = NumericBehavior.SignedInteger;
			}
			
			formattedCurrentValueInNewBase = currentValue.formattedNumberInBase(numericBase: _numericBase, numericBehavior: requiredNumericBehavior);
			
			// reset state
			_hasDecimal = false;
			_negated = false;
			integralArray = [];
			decimalArray = [];
			
			// iterate over string and pull each digit out
			for ch in formattedCurrentValueInNewBase {
				if (ch == "-") {
					// assign negative.  This will always come first
					negativeState(negative: true);
				} else if (ch == ".") {
					// set the decimal
					applyDecimal();
				} else {
					// only digits allowed now.  Just push them as they come
					do {
						try appendDigit(value: InputBufferImpl.charToInt(character: ch));
					} catch {}
				}
			}
		}
	}

	public func negativeState(negative: Bool) {
		_negated = negative;
	}
	
	public func appendDigit(value: UInt8) {
		if hasDecimal() {
			decimalArray = decimalArray + [value];
		} else {
			// if there are no digits, and the appended digit is 0, ignore
			if integralArray.count > 0 || value != 0 {
				integralArray = integralArray + [value];
			}
		}
	}
	
	public func applyDecimal() {
		assert(_hasDecimal == false);
		//assert(numericBase() == NumericBase.base10);
		_hasDecimal = true;
	}
	
	public func hasDecimal() -> Bool {
		return _hasDecimal;
	}
	
	public func isNegated() -> Bool {
		return _negated;
	}
	
	public func hasIntegerPortion() -> Bool {
		return integralArray.count > 0;
	}
	
	public func integralBuffer() -> [UInt8] {
		return integralArray;
	}
	
	public func operativeIntegralDigits() -> [UInt8] {
		return integralBuffer();
	}
	
	public func hasDecimalPortion() -> Bool {
		return decimalArray.count > 0;
	}
	
	public func decimalBuffer() -> [UInt8] {
		return decimalArray;
	}
	
	public func operativeDecimalDigits() -> [UInt8] {
		let buffer = decimalBuffer();

		// start from the end, and mark the first non-zero digit
		var lastIndex = -1;
		for (index, value) in buffer.reversed().enumerated() {
			if (value != 0) {
				lastIndex = index;
				break;
			}
		}

		if (lastIndex == -1) {
			return [];
		} else {
			return Array(buffer[0..<(buffer.count - lastIndex)]);
		}
	}
	
	public func numericValue() -> NSDecimalNumber {
		// special cases here to avoid a lot of processing
		if (numericBase() == NumericBase.base10 && hasDecimal()) {
			// this is one case I do not want to handle. Base10 with fractional component
			let text = formattedTextBuffer(applySeparators: false);
			return NSDecimalNumber(string: text);
		}
		
		// for all others calcuate
		let power = NSDecimalNumber(value: NumericBaseConverter.numericBaseAsInteger(numericBase: numericBase()));
		var currentBase: NSDecimalNumber = NSDecimalNumber.one;
		
		var longLong: NSDecimalNumber = NSDecimalNumber.zero;

		for digit in integralBuffer().reversed() {
			longLong = longLong.adding(currentBase.multiplying(by: NSDecimalNumber(value: digit)));

			currentBase = currentBase.multiplying(by: power);
		}
		
		if (isNegated()) {
			longLong = longLong.decimalNumberByNegating();
		}

		return longLong;
	}

	public func formattedTextBuffer() -> String {
		return formattedTextBuffer(applySeparators: true);
	}

	public func formattedTextBuffer(applySeparators: Bool) -> String {
		let sign = isNegated() ? "-" : "";

		var buff: String = "";

		do {
			if (hasIntegerPortion()) {
				let digitsPerSeparator: UInt;

				switch (numericBase()) {
					case .base2:
						digitsPerSeparator = 8;
					case .base8:
						digitsPerSeparator = 4;
					case .base10:
						digitsPerSeparator = 3;
					case .base16:
						digitsPerSeparator = 4;
				}

				let integralDigits = operativeIntegralDigits();
				
				for (index, digit) in integralDigits.enumerated() {
					if (index > 0 && applySeparators) {
						let position = UInt(integralDigits.count - index);
						if (position % digitsPerSeparator == 0) {
							buff.append(",");
						}
					}

					try buff.append(intToChar(number: digit));
				}
			}

			if (hasDecimalPortion()) {
				let digits = operativeDecimalDigits();
				
				if (digits.count > 0) {
					buff.append(".");
				}

				for digit in digits {
					try buff.append(intToChar(number: digit));
				}
			}
		} catch {
		}

		// if our string is empty, it is '0'
		if (buff == "") {
			return "0";
		}

		return sign + buff;
	}
	
	public func progressTextBuffer() -> String {
		return progressTextBuffer(applySeparators: true);
	}

	public func progressTextBuffer(applySeparators: Bool) -> String {
		let sign = isNegated() ? "-" : "";

		var buff: String = "";

		do {
			if (hasIntegerPortion()) {
				let digitsPerSeparator: UInt;

				switch (numericBase()) {
					case .base2:
						digitsPerSeparator = 4;
					case .base8:
						digitsPerSeparator = 4;
					case .base10:
						digitsPerSeparator = 3;
					case .base16:
						digitsPerSeparator = 4;
				}

				let integralDigits = integralBuffer();
				
				for (index, digit) in integralBuffer().enumerated() {
					if (index > 0 && applySeparators) {
						let position = UInt(integralDigits.count - index);
						if (position % digitsPerSeparator == 0) {
							buff.append(",");
						}
					}

					try buff.append(intToChar(number: digit));
				}
			}

			if (hasDecimal()) {
				// check for no integer part, E.G., '.' becomes '0.'
				if buff.isEmpty {
					buff.append("0");
				}
						
				buff.append(".");
			}

			if (hasDecimalPortion()) {
				for digit in decimalBuffer() {
					try buff.append(intToChar(number: digit));
				}
			}
		} catch {
		}

		// if our string is empty, it is '0'
		if (buff == "") {
			return "0";
		}

		return sign + buff;
	}
	
	public func intToChar(number: UInt8) throws -> Character {
		let maxVal: UInt8;
		
		switch (numericBase()) {
			case .base2:
				maxVal = 1;
			case .base8:
				maxVal = 7;
			case .base10:
				maxVal = 9;
			case .base16:
				maxVal = 15;
		}
		
		if (number > maxVal) {
			throw StackError.invalidDigitForNumericBase(numericBase: numericBase(), digit: number);
		}

		switch (number) {
			case 0:
				return "0";
			case 1:
				return "1";
			case 2:
				return "2";
			case 3:
				return "3";
			case 4:
				return "4";
			case 5:
				return "5";
			case 6:
				return "6";
			case 7:
				return "7";
			case 8:
				return "8";
			case 9:
				return "9";
			case 10:
				return "A";
			case 11:
				return "B";
			case 12:
				return "C";
			case 13:
				return "D";
			case 14:
				return "E";
			case 15:
				return "F";
			default:
				throw StackError.invalidDigit(value: number)
		}
	}
	
	public static func charToInt(character: Character) throws -> UInt8 {
		switch (character) {
			case "0":
				return 0;
			case "1":
				return 1;
			case "2":
				return 2;
			case "3":
				return 3;
			case "4":
				return 4;
			case "5":
				return 5;
			case "6":
				return 6;
			case "7":
				return 7;
			case "8":
				return 8;
			case "9":
				return 9;
			case "A":
				return 10;
			case "B":
				return 11;
			case "C":
				return 12;
			case "D":
				return 13;
			case "E":
				return 14;
			case "F":
				return 15;
			default:
				throw StackError.invalidCharacterForDigit(character: character)
		}
	}
	
	public func removeLastDigitOrDecimal() {
		// process backwards.  Decimal portion first, then decimal point, and finall integral portion.  Failing those checks, do nothing
		if (hasDecimalPortion()) {
			// decimal digit must have been the last digit added...
			decimalArray.removeLast()
		} else if (hasDecimal()) {
			// since there are no decimal digits yet, the decimal point must have been the last thing added.  Just flip the state.
			_hasDecimal = false;
		} else if (hasIntegerPortion()) {
			integralArray.removeLast()
		}
	}

	public func deepCloneInternalState() -> InputBuffer
	{
		let newBuffer = InputBufferImpl();
		
		newBuffer._hasDecimal = _hasDecimal;
		newBuffer._negated = _negated;
		newBuffer.integralArray = integralArray;
		newBuffer.decimalArray = decimalArray;
		newBuffer._numericBase = _numericBase;

		return newBuffer;
	}
	
	public func resetDigits() {
		integralArray = [];
		decimalArray = [];
		_hasDecimal = false;
		_negated = false;
	}
}

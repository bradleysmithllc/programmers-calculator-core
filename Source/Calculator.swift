//
//  Calculator.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//
import Foundation

public enum NumericBehavior : String
{
	case Decimal = "Decimal";
	case SignedInteger = "SignedInteger";
	case UnsignedInteger = "UnsignedInteger";

	public static let allValues = [Decimal, SignedInteger, UnsignedInteger];
	public static let mapValues = [Decimal.rawValue: Decimal, SignedInteger.rawValue: SignedInteger, UnsignedInteger.rawValue: UnsignedInteger];
}

public enum IntegerWordSize : UInt8
{
	case none = 0;
	case byte = 1;
	case word_2 = 2;
	case word_4 = 4;
	case word_8 = 8;
	
	public static let allValues = [none, byte, word_2, word_4, word_8];
	public static let mapValues = ["None": none, "Byte": byte, "Word_2": word_2, "Word_4": word_4, "Word_8": word_8];
}

public enum NumericBase {
		case base2
		case base8
		case base10
		case base16
}

public class NumericBaseConverter {
	public static func numericBaseAsInteger(numericBase: NumericBase) -> UInt8 {
		switch (numericBase) {
			case .base2:
				return 2;
			case .base8:
				return 8;
			case .base10:
				return 10;
			case .base16:
				return 16;
		}
	}
}

public protocol OperationObserver {
	mutating func operationInvoked(_ calculator: Calculator, operation op: Operation);
}

/* Calculator version  1.1 */
public protocol Calculator {
	/**
	 * This stack is the basis for all operations.  It contains all operands.  These are pushed
	 * on and popped of in FILO manner, like you would expect from a stack.
	 */
	func stack() -> Stack;

	/**
	* Resets the transient state - all stack and named memory locations.  Also resets the numeric base.
	*/
	func reset();

	/**Get an operation by it's id.
		*/
	func operation(_ id: String) -> Operation?;

	/**
	* Add an operation.
	*/
	func addOperation(_ op: Operation)
	
	/**
	* A list of available operation ids.
	*/
	func operationIds() -> [String];

	/**
	 * A map of all available operations, my id.
   */
	func operations() -> [String:Operation];

	/**Invoke an operation by it's id
		*/
	func invoke(_ mnemonic: String) throws;
}

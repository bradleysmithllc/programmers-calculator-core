//
//  CalculatorConstants.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/15/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**Property names used by the data input operations.
  */

open class CalculatorProperties {
	/**
	*/
	public static let INTEGER_WORD_SIZE: String = "INTEGER_WORD_SIZE";
	
	/**
	*/
	public static let NUMERIC_BEHAVIOR: String = "NUMERIC_BEHAVIOR";
	
	/**Current numeric base.  This has a bearing on numeric display as well as input (gives each digit context).
	*/
	public static let SIGNIFICANT_DECIMALS: String = "SIGNIFICANT_DECIMALS";

	/**Current numeric base.  This has a bearing on numeric display as well as input (gives each digit context).
	*/
	public static let NUMERIC_BASE: String = "NUMERIC_BASE";

	/**Constant which designates that the user is entering data
	*/
	public static let NUMERIC_ENTRY_STATE: String = "NUMERIC_ENTRY_STATE";
	
	/**Constant which stores the integral portion of the current data input
	  */
	public static let NUMERIC_ENTRY_INTEGRAL: String = "NUMERIC_ENTRY_INTEGRAL";

	/**Constant which stores the decimal portion of the current data input
	  */
	public static let NUMERIC_ENTRY_DECIMAL: String = "NUMERIC_ENTRY_DECIMAL";

	/**Constant which identifies data input is in the decimal numeric portion.
    */
	public static let NUMERIC_ENTRY_IN_DECIMAL: String = "NUMERIC_ENTRY_IN_DECIMAL";

	/**Constant which identifies the decimal position for the decimal data entry
    */
	public static let NUMERIC_ENTRY_DECIMAL_POSITION: String = "NUMERIC_ENTRY_DECIMAL_POSITION";

	/**Constant which identifies input mode for trig functions
	*/
	public static let TRIGONOMERTIC_FUNCTIONS_MODE: String = "TRIGONOMERTIC_FUNCTIONS_MODE";
}

//
//  IntegerExtensions.swift
//  Calc
//
//  Created by Bradley Smith on 1/5/16.
//  Copyright © 2016 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**Some extensions for managing converting between various integer types.  E.G., Int64 to UInt8, etc.  Int64 i the base type for all conversions.
  */

// Conversions to UInt8 from Int64
public extension UInt8
{
	init(bitPattern: Int64)
	{
		self.init(bitPattern: Int8(bitPattern));
	}
}
//
//  NSDecimalNumberExtensions.swift
//  Calc
//
//  Created by Bradley Smith on 1/5/16.
//  Copyright © 2016 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/*
Extend NSDecimalNumber to include some functions
*/
extension NSDecimalNumber {
	/* Answers (aDecimal * -1) */
	public func decimalNumberByNegating() -> NSDecimalNumber {
		if self == NSDecimalNumber.notANumber
		{
			return self;
		}

		return self.multiplying(by: NSDecimalNumber(mantissa: 1, exponent: 0, isNegative: true));
	}
	
	/* Answers |x| */
	public func decimalNumberWithAbsoluteValue() -> NSDecimalNumber {
		if (doubleValue < 0)
		{
			return decimalNumberByNegating();
		}
		
		return self;
	}
	
	/**Returns a number that is appropriate for this base.
	*/
	public func decimalNumberInBase(_ base: Int) -> NSDecimalNumber
	{
		if (base == 10)
		{
			return self;
		}
		
		return NSDecimalNumber(value: safeLongLongValue as Int64);
	}
	
	public func formatWithBaseAndSign(numericBase: NumericBase, unsigned: Bool) -> String {
		return "";
	}

	public func decimalNumberTruncated() -> NSDecimalNumber
	{
		return decimalNumberRoundedToDigits(withDecimals: 0, roundingMode: NSDecimalNumber.RoundingMode.down);
	}

	public func decimalNumberRoundedToDigits(withDecimals: UInt8, roundingMode: NSDecimalNumber.RoundingMode = NSDecimalNumber.RoundingMode.plain) -> NSDecimalNumber
	{
		// round to required decimals
		let disp = rounding(accordingToBehavior: NSDecimalNumberHandler(roundingMode: roundingMode, scale: Int16(withDecimals), raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: false));
		
		return disp;
	}

	/**Returns a formatted string in the base.
	*/
	public func formattedNumberInBase(numericBase: NumericBase = NumericBase.base10, numericBehavior: NumericBehavior = NumericBehavior.Decimal, applySeparator: Bool = false) -> String
	{
		let formattedNumber: String;
		
		/* test for invalid states
		 NumericBehavior.Decimal only applies to base10
		*/
		
		if (numericBehavior == NumericBehavior.Decimal && numericBase != NumericBase.base10) {
			assert(false);
		}

		if (numericBase == NumericBase.base10)
		{
			formattedNumber = String(describing: self);
		}
		else
		{
			if (numericBehavior == NumericBehavior.UnsignedInteger)
			{
				formattedNumber = String(safeUnsignedLongLongValue, radix: Int(NumericBaseConverter.numericBaseAsInteger(numericBase: numericBase)));
			}
			else
			{
				formattedNumber = String(safeLongLongValue, radix: Int(NumericBaseConverter.numericBaseAsInteger(numericBase: numericBase)));
			}
		}

		let upperBuffer = formattedNumber.uppercased();

		// if separators are required, apply them now.
		let outputString: String;
		
		if applySeparator {
			let digitsPerSeparator: UInt8;

			switch (numericBase) {
				case .base2:
					digitsPerSeparator = 8;
				case .base8:
					digitsPerSeparator = 4;
				case .base10:
					digitsPerSeparator = 3;
				case .base16:
					digitsPerSeparator = 4;
			}

			let integralStringPart: String;
			let decimalStringPart: String;
			
			if let indexOfDecimal = upperBuffer.firstIndex(of: ".") {
				integralStringPart = String(formattedNumber[..<indexOfDecimal]);
				decimalStringPart = String(formattedNumber[indexOfDecimal...]);
			} else {
				integralStringPart = upperBuffer;
				decimalStringPart = "";
			}

			// iterate over the integral part and apply separators
			var buffer: String = "";

			// before doing this, remove the sign character if it is present
			let signString: String;
			let unsignedIntegerString: String;

			if let _ = integralStringPart.firstIndex(of: "-") {
				signString = "-";
				unsignedIntegerString = String(integralStringPart.dropFirst(1));
			} else {
				signString = "";
				unsignedIntegerString = integralStringPart;
			}

			let unsignedIntegerDigitCount = unsignedIntegerString.count;

			for (index, digit) in unsignedIntegerString.enumerated() {
				let position = UInt8(unsignedIntegerDigitCount - index);
				// only apply a separator if this is not digit number 1...
				if (index > 0 && position % digitsPerSeparator == 0) {
					buffer.append(",");
				}
				
				buffer.append(digit);
			}
			
			outputString = signString + buffer + decimalStringPart;
		} else {
			outputString = upperBuffer;
		}

		return outputString;
	}

	public var safeByteValue: Int8 {
		get {
			let bits = UInt8(decimalNumberTruncated().int64Value & 0xFF);

			return Int8(bitPattern: bits);
		}
	};

	public var safeUnsignedByteValue: UInt8 {
		get {
			let charValue = UInt8(decimalNumberTruncated().uint64Value & 0xFF);
			return charValue;
		}
	};

	public var safeShortValue: Int16 {
		get {
			let bits = UInt16(decimalNumberTruncated().int64Value & 0xFFFF);
			
			return Int16(bitPattern: bits);
		}
	};
	
	public var safeUnsignedShortValue: UInt16 {
		get {
			return UInt16(decimalNumberTruncated().uint64Value & 0xFFFF);
		}
	};

	public var safeIntValue: Int32 {
		get {
			let bits = UInt32(decimalNumberTruncated().int64Value & 0xFFFFFFFF);
			
			return Int32(bitPattern: bits);
		}
	};

	public var safeUnsignedIntValue: UInt32 {
		get {
			return UInt32(decimalNumberTruncated().uint64Value & 0xFFFFFFFF);
		}
	};
	
	public var safeLongLongValue: Int64 {
		get {
			return decimalNumberTruncated().int64Value;
		}
	};
	
	public var safeUnsignedLongLongValue: UInt64 {
		get {
			return decimalNumberTruncated().uint64Value;
		}
	};
}

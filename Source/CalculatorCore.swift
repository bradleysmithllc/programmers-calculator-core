//
//  CalculatorCore.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/14/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class CalculatorCore: Calculator {
	fileprivate var _stack: OperandStack!;
	fileprivate var _operations: [String: Operation]! = [String: Operation]();

	fileprivate var observers: [OperationObserver] = [];
	fileprivate var mathHub = MathHub.mathHub();
	
	public init() {
		_stack = OperandStack(calculator: self);
		
		// There is no way there isn't a better way to do this.
		addOperation(AdditionOperation());
		addOperation(SubtractionOperation());
		addOperation(DivisionOperation());
		addOperation(MultiplicationOperation());
		addOperation(EnterOperation());
		addOperation(XYTranspositionOperation());
		addOperation(StackRotateUpOperation());
		addOperation(StackRotateDownOperation());
		addOperation(ModulusOperation());
		addOperation(YExpXOperation());
		addOperation(XExpYOperation());
		addOperation(TenExpXOperation());
		addOperation(TwoExpXOperation());
		addOperation(XSquaredOperation());
		addOperation(XCubedOperation());
		addOperation(ZeroLiteralOperation());
		addOperation(OneLiteralOperation());
		addOperation(TwoLiteralOperation());
		addOperation(ThreeLiteralOperation());
		addOperation(FourLiteralOperation());
		addOperation(FiveLiteralOperation());
		addOperation(SixLiteralOperation());
		addOperation(SevenLiteralOperation());
		addOperation(EightLiteralOperation());
		addOperation(NineLiteralOperation());
		addOperation(TenLiteralOperation());
		addOperation(ElevenLiteralOperation());
		addOperation(TwelveLiteralOperation());
		addOperation(ThirteenLiteralOperation());
		addOperation(FourteenLiteralOperation());
		addOperation(FifteenLiteralOperation());
		addOperation(DecimalLiteralOperation());
		addOperation(DropXOperation());
		addOperation(SineXOperation());
		addOperation(CosineXOperation());
		addOperation(SecantXOperation());
		addOperation(CosecantXOperation());
		addOperation(TangentXOperation());
		addOperation(CotangentXOperation());
		addOperation(ArcSineXOperation());
		addOperation(ArcCosineXOperation());
		addOperation(ArcSecantXOperation());
		addOperation(ArcCosecantXOperation());
		addOperation(ArcTangentXOperation());
		addOperation(ArcCotangentXOperation());
		addOperation(HyperbolicSineXOperation());
		addOperation(HyperbolicCosineXOperation());
		addOperation(HyperbolicSecantXOperation());
		addOperation(HyperbolicCosecantXOperation());
		addOperation(HyperbolicTangentXOperation());
		addOperation(HyperbolicCotangentXOperation());
		addOperation(HyperbolicArcSineXOperation());
		addOperation(HyperbolicArcCosineXOperation());
		addOperation(HyperbolicArcSecantXOperation());
		addOperation(HyperbolicArcCosecantXOperation());
		addOperation(HyperbolicArcTangentXOperation());
		addOperation(HyperbolicArcCotangentXOperation());
		addOperation(ChangeSignXOperation());
		addOperation(ResetOperation());
		addOperation(ClearXOperation());
		addOperation(UseRadiansOperation());
		addOperation(UseDegreesOperation());
		addOperation(UseGradiansOperation());
		addOperation(ToggleDegreesOperation());
		addOperation(PiOperation());
		addOperation(TwoPiOperation());
		addOperation(EOperation());
		addOperation(EulerOperation());
		addOperation(GoldenRatioOperation());
		addOperation(ReciprocalXOperation());
		addOperation(SquareRootXOperation());
		addOperation(CubeRootXOperation());
		addOperation(NthRootXOperation());
		addOperation(CheckpointOperation());
		addOperation(RestoreCheckpointOperation());
		addOperation(EExpXOperation());
		addOperation(BackupXOperation());
		addOperation(PercentOperation());
		addOperation(ToPercentOperation());
		addOperation(DeltaPercentOperation());
		addOperation(StoreXOperation());
		addOperation(StorePlusXOperation());
		addOperation(StoreMinusXOperation());
		addOperation(RecallXOperation());
		addOperation(LogBase10Operation());
		addOperation(LnOperation());
		addOperation(LogBase2Operation());
		addOperation(TruncateFractionalOperation());
		addOperation(TruncateIntegerOperation());
		addOperation(Base2Operation());
		addOperation(Base8Operation());
		addOperation(Base10Operation());
		addOperation(Base16Operation());
		addOperation(SumOperation());
		addOperation(SumProductOperation());
		addOperation(MemoryStoreOperation());
		addOperation(MemoryPlusOperation());
		addOperation(MemoryMinusOperation());
		addOperation(RecallMemoryOperation());
		addOperation(ClearMemoryOperation());
		addOperation(DoubleZeroLiteralOperation());
		addOperation(DoubleFLiteralOperation());
		addOperation(AndOperation());
		addOperation(OrOperation());
		addOperation(UnaryNotOperation());
		addOperation(NorOperation());
		addOperation(XorOperation());
		addOperation(LeftShiftOperation());
		addOperation(RightShiftOperation());
		addOperation(YLeftShiftXOperation());
		addOperation(YRightShiftXOperation());
		addOperation(RandOperation());
		addOperation(FactorialOperation());
		addOperation(ReduceFractionOperation());
		addOperation(FactorQuadraticOperation());
		addOperation(AbsoluteValueXOperation());
		addOperation(ConvertToRadiansOperation());
		addOperation(ConvertToDegreesOperation());
		addOperation(ConvertToGradiansOperation());
		addOperation(DecimalOperation());
		addOperation(SupplementaryAngleOperation());
		addOperation(ComplementaryAngleOperation());
		addOperation(ToggleWordSizeOperation());
		addOperation(ToggleSignedMathOperation());
		addOperation(DisabledOperation());
		addOperation(RoundOperation());
		addOperation(ConvertDateToJulianDayOperation());
		addOperation(ConvertJulianTODateOperation());
		addOperation(PercentXYOperation());
		addOperation(LeastCommonMultipleOperation());
		
		// load all json-defined compound operations
		let compOps = OperationLoader.loadCompoundOperations(calculator: self);
		
		for op in compOps
		{
			addOperation(op);
		}
	}

	open func addOperation(_ operation: Operation)
	{
		let id = operation.id();
		
		if let _ = _operations[id]
		{
			//fail.  I don't know how to handle this yet.
			fatalError("Duplicated operation id: " + id);
		}
		
		operation.calculator(calculator: self);
		
		_operations[id] = operation;
		
		if (operation is OperationObserver) {
			observers.append(operation as! OperationObserver);
		}
	}

	open func stack() -> Stack {
		return _stack;
	}
	
	open func reset() {
		_stack.reset();
	}
	
	open func invoke(_ id: String) throws {
		mathHub.resetMessages();

		let op: Operation? = operation(id);
		
		if let ops: Operation = op {
			// broadcast change
			for var observer: OperationObserver in observers {
				observer.operationInvoked(self, operation: ops);
			}
			
			do {
				let currentNumericEntryState = _stack.numericEntryState()

				try ops.perform();
				
				let registerResultState = ops.registerResultState();
				
				if (registerResultState == NumericEntryState.in_NUMERIC_ENTRY)
				{
					// first, calculate current entry value
					let vcurrentInput: NSDecimalNumber = _stack.currentInputNumericValue();
					
					if (
						currentNumericEntryState == NumericEntryState.in_NUMERIC_ENTRY
							||
							currentNumericEntryState == NumericEntryState.waiting_FOR_NUMERIC_ENTRY
						)
					{
						// replace the x register with this value
						stack().setX(vcurrentInput);
					}
					else
					{
						// push current value onto the stack
						stack().push(vcurrentInput);
					}
				}
				else
				{
					_stack.resetInputBuffer();
				}
				
				_stack.setNumericEntryState(registerResultState);
			} catch {
				print("\(error)");
			}
			
		} else {
			throw BadOperationState();
		}
	}
	
	open func operation(_ id: String) -> Operation? {
		return _operations[id];
	}
	
	open 	func operationIds() -> [String]
	{
		return Array(_operations.keys).sorted(){ $1 > $0 };
	}

	open func operations() -> [String:Operation] {
		return _operations;
	}
}

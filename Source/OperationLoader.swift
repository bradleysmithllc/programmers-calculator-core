//
//  OperationLoader.swift
//  programmers_calculator_core
//
//  Created by Bradley Smith on 4/27/17.
//  Copyright © 2017 Bradley Smith, LLC. All rights reserved.
//

import Foundation

class OperationLoader
{
	fileprivate static let resourceRoot = Bundle(for: OperationLoader.self).resourceURL!.appendingPathComponent("CalculatorResources");

	public static func loadCompoundOperations(calculator: Calculator!) -> [Operation]
	{
		var operationList: [Operation] = [];
		
		// compound operations
		let compoundOperationsRoot = urlAtPath(["CompoundOperations"], root: resourceRoot);
		
		do
		{
			let pathItems = try FileManager.default.contentsOfDirectory(at: compoundOperationsRoot, includingPropertiesForKeys: nil, options: [.skipsSubdirectoryDescendants, .skipsPackageDescendants, .skipsHiddenFiles]);
			
			for pathItem in pathItems
			{
				let filename: String = pathItem.deletingPathExtension().lastPathComponent;

				let operation: Operation;
				
				if let _ = calculator {
					operation = JSONCompoundOperation(calculator: calculator, id: filename, url: pathItem);
				} else {
					operation = JSONCompoundOperation(calculator: nil, id: filename, url: pathItem);
				}
				
				operationList.append(operation);
			}
		}
		catch
		{
		}
		
		return operationList;
	}

	public static func urlAtPath(_ path: [String], root: URL) -> URL
	{
		var url = root;
		
		for pathElement in path
		{
			url = url.appendingPathComponent(pathElement);
		}
		
		return url;
	}
	
	public static func loadOperationClassByName(name: String) -> Operation.Type?
	{
		//let s = print(NSStringFromClass(AdditionOperation.self));
		//let t = NSClassFromString("programmers_calculator_core.AdditionOperation");
		//print(t);
		let clsName = "programmers_calculator_core.\(name)Operation";
		
		let cl: AnyClass? = NSClassFromString(clsName);
		
		if cl is Operation.Type
		{
			return cl as! Operation.Type?;
		}
		else
		{
			return nil;
		}
	}
}

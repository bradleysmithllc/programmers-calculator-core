//
//  CalculatorNode.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/5/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

public enum NumericEntryState
{
	case in_NUMERIC_ENTRY;
	case waiting_FOR_NUMERIC_ENTRY;
	case waiting_FOR_OPERATION;
}

public enum StackAffect: String
{
	case Push = "Push";
	case Pop = "Pop";
	case Peek = "Peek";

	public static let allValues = [Push, Pop, Peek];
	public static let mapValues = [Push.rawValue: Push, Pop.rawValue: Pop, Peek.rawValue: Peek];
}

public enum OperationType: String
{
	case state = "state";
	case behavior = "behavior";
	case unary = "unary";
	case binary = "binary";
	case enary = "enary";

	case arithmetic_operator = "arithmetic_operator";
	
	case literal = "literal";
	case constant = "constant";
	
	public static let allValues = [state, behavior, unary, binary, enary, arithmetic_operator, literal, constant];
	public static let mapValues = [state.rawValue: state, behavior.rawValue: behavior, unary.rawValue: unary, binary.rawValue: binary, enary.rawValue: enary, arithmetic_operator.rawValue: arithmetic_operator, literal.rawValue: literal, constant.rawValue: constant];
}

public protocol Operation {
	func calculator(calculator: Calculator);

	func validateStack() -> Bool;
	func perform() throws;
	func mnemonic() -> String;
	func decoratedMnemonic() -> String;
	func id() -> String;

	func registerResultState() -> NumericEntryState;
	
	func stackAffect() -> [StackAffect]?;
	func formula() -> String?;
	func description() -> String;

	func operationType() -> OperationType;
}

open class BadOperationState: Error {
}

//
//  Stack.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/12/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class OperandStack: Stack {
	fileprivate var mathHub: MathHub = MathHub.mathHub();

	fileprivate var elements: [StackElement] = [];
	fileprivate var _namedMemory: [String: NSDecimalNumber]! = [String: NSDecimalNumber]();
	fileprivate var _properties: [String: Any] = [String: Any]();
	fileprivate var _stateProperties: [String: Any] = [String: Any]();
	
	fileprivate var checkpoints = Array<(stack: Array<StackElement>, inputBuffer: InputBuffer, memory: [String: NSDecimalNumber], properties: [String: Any], stateProperties: [String: Any])>();
	
	fileprivate var calculator: Calculator;

	var inputBuffer: InputBuffer = InputBufferImpl()

	public init(calculator: Calculator) {
		self.calculator = calculator;
		clear();
	}
	
	open func pushAtEnd(_ decNum: NSDecimalNumber)
	{
		pushAtEnd(StackElement(number: decNum));
	}
	
	open func pushAtEnd(_ message: String)
	{
		pushAtEnd(StackElement(message: message));
	}

	/**Push an element onto the stack, but at the end, not at the head.
	*/
	open func pushAtEnd(_ element: StackElement)
	{
		push(element, atIndex: 0);
	}

	open func push(_ message: String)
	{
		push(StackElement(message: message));
	}

	/**Push a message onto the stack.
	*/
	open func push(_ element: StackElement)
	{
		push(element, atIndex: elements.count);
	}

	open func push(_ decNum: NSDecimalNumber) {
		push(StackElement(number: decNum));
	}
	
	fileprivate func push(_ _element: StackElement, atIndex: Int)
	{
		var mutElement = _element;

		// force the number to fit into the current base
		if mutElement.isNumber
		{
			switch(getNumericBehavior())
			{
			case .Decimal:
				// Constrain to decimal count
					mutElement.swapNumber(mutElement.number().decimalNumberRoundedToDigits(withDecimals: getDecimals()));
				break;
			case .UnsignedInteger:
				switch(getIntegerWordSize())
				{
				case .byte:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeUnsignedByteValue as UInt8));
				case .word_2:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeUnsignedShortValue as UInt16));
				case .word_4:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeUnsignedIntValue as UInt32));
				case .word_8:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeUnsignedLongLongValue as UInt64));
				case .none:
				break;
				}
			case .SignedInteger:
				switch(getIntegerWordSize())
				{
				case .byte:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeByteValue as Int8));
				case .word_2:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeShortValue as Int16));
				case .word_4:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeIntValue as Int32));
				case .word_8:
					mutElement.swapNumber(NSDecimalNumber(value: mutElement.number().safeLongLongValue as Int64));
				case .none:
					break;
				}
			}
		}

		elements.insert(mutElement, at: atIndex);
	}
	
	open func pop() -> NSDecimalNumber {
		return popElement().number();
	}

	open func popElement() -> StackElement {
		if (size() > 0) {
			return elements.removeLast();
		}
		else
		{
			return _nil;
		}
	}
	
	open func setX(_ number: NSDecimalNumber) {
		// implement as pop and push to make sure that push is the only place data can enter the stack
		let _ = pop();
		push(number);
		/*
		if (size() > 0) {
			elements[size() - 1] = StackElement(number: number);
		} else {
			elements.append(StackElement(number: number));
		}*/
	}
	
	open func getX() -> NSDecimalNumber {
		return getXElement().number();
	}

	open func getXElement() -> StackElement
	{
		return peekElement();
	}
	
	open func getYElement() -> StackElement
	{
		return peekElement(size() - 2);
	}
	
	open func getY() -> NSDecimalNumber {
		return getYElement().number();
	}
	
	open func getZElement() -> StackElement
	{
		return peekElement(size() - 3);
	}
	
	open func getZ() -> NSDecimalNumber {
		return getZElement().number();
	}
	
	open func getTElement() -> StackElement
	{
		return peekElement(size() - 4);
	}
	
	open func getT() -> NSDecimalNumber {
		return getTElement().number();
	}

	open func popOldest() -> StackElement {
		if (size() > 0) {
			return elements.removeFirst();
		}
		else
		{
			return _nil;
		}
	}
	
	open func size() -> Int {
		return elements.count;
	}
	
	open func empty() -> Bool
	{
		return size() == 0;
	}
	
	open func peek() -> NSDecimalNumber {
		return peek(size() - 1);
	}
	
	open func popAll() -> [StackElement] {
		let ele = elements;

		elements.removeAll();

		return ele;
	}

	open func peekElement() -> StackElement {
		return peekElement(size() - 1);
	}
	
	open func peek(_ index: Int) -> NSDecimalNumber {
		return peekElement(index).number();
	}
	
	open func peekElement(_ index: Int) -> StackElement {
		var offset: Int = size() - 1;
		
		if (index != -1) {
			offset = index;
		}
		
		if (index < 0 || index >= size())
		{
			return _nil;
		}
		
		return elements[offset];
	}

	open func clear() {
		_namedMemory.removeAll();
		elements.removeAll();
		indexRegister = NSDecimalNumber.zero;
		inputBuffer.resetDigits();
	}
	
	fileprivate var indexRegister: NSDecimalNumber = NSDecimalNumber.zero;
	
	open func getIndex() -> NSDecimalNumber
	{
		return indexRegister;
	}
	
	open func setIndex(index: NSDecimalNumber)
	{
		indexRegister = index;
	}
	
	open func checkPoint()
	{
		checkpoints.insert((stack: elements, inputBuffer: inputBuffer.deepCloneInternalState(), memory: _namedMemory, properties: _properties, stateProperties: _stateProperties), at: 0)

		while checkpoints.count > checkPointDepth
		{
			checkpoints.removeLast();
		}
	}
	
	var checkPointDepth: Int = 10;

	open func setCheckPointDepth(_ depth: Int)
	{
		checkPointDepth = depth;
	}

	open func revertToCheckPoint()
	{
		if (checkpoints.count == 0)
		{
			return;
		}

		let lastCheck = checkpoints.removeFirst();

		elements = lastCheck.stack;
		_namedMemory = lastCheck.memory;
		_properties = lastCheck.properties;
		_stateProperties = lastCheck.stateProperties;
		inputBuffer = lastCheck.inputBuffer;
	}

	open func memory(_ slotName: String) -> NSDecimalNumber {
		if let mem = _namedMemory[slotName]
		{
			return mem;
		}
		
		return NSDecimalNumber.zero;
	}
	
	open func memoryWithDefault(_ slotName: String, initialValue: NSDecimalNumber) -> NSDecimalNumber {
		if let mem = _namedMemory[slotName] {
			return mem;
		}
		
		return assignMemory(slotName, value: initialValue);
	}
	
	open func assignMemory(_ slotName: String, value: NSDecimalNumber) -> NSDecimalNumber {
		_namedMemory[slotName] = value;
		
		return value;
	}
	
	open func clearMemory(_ slotName: String) -> NSDecimalNumber? {
		return _namedMemory.removeValue(forKey: slotName);
	}
	
	open func memory() -> [String:NSDecimalNumber]! {
		return _namedMemory;
	}
	
	open func properties() -> [String:Any] {
		return _properties;
	}
	
	func property0(_ properties: [String:Any], propertyName: String) -> Any? {
		let v = properties[propertyName];
		
		return v;
	}
	
	fileprivate func _property<T>(_ properties: [String:Any], propertyName: String) -> T? {
		let v: Any? = property0(properties, propertyName: propertyName);
		
		if let tt: T = (v as? T) {
			return tt;
		}
		
		return nil;
	}
	
	fileprivate func _propertyWithDefault<T>(_ properties: inout [String:Any], propertyName: String, defaultValue: () -> T) -> T {
		let t: T? = _property(properties, propertyName: propertyName)
		
		if let tt: T = t {
			return tt;
		}
		
		return _setProperty(&properties, propertyName: propertyName, propertyValue: defaultValue());
	}
	
	fileprivate func _setProperty<T>(_ properties: inout [String:Any], propertyName: String, propertyValue: T) -> T {
		properties[propertyName] = propertyValue;
		
		return propertyValue;
	}
	
	fileprivate func _clearProperty<T>(_ properties: inout [String:Any], propertyName: String) -> T? {
		return properties.removeValue(forKey: propertyName) as? T;
	}
	
	open func property<T>(_ propertyName: String) -> T? {return _property(_properties, propertyName: propertyName);}

	open func propertyWithDefault<T>(_ propertyName: String, defaultValue: () -> T) -> T {
		return _propertyWithDefault(&_properties, propertyName: propertyName, defaultValue: defaultValue);
	}

	open func setProperty<T>(_ propertyName: String, propertyValue: T) -> T {return _setProperty(&_properties, propertyName: propertyName, propertyValue: propertyValue);}
	open func clearProperty<T>(_ propertyName: String) -> T? {return _clearProperty(&_properties, propertyName: propertyName);
	}
	
	open func reset() {
		clear();
		_properties.removeAll();
	}
	
	open func stateProperties() -> [String:Any] {
		return _stateProperties;
	}

	open func stateProperty<T>(_ propertyName: String) -> T? {return _property(_stateProperties, propertyName: propertyName);}
	
	open func statePropertyWithDefault<T>(_ propertyName: String, defaultValue: () -> T) -> T {return _propertyWithDefault(&_stateProperties, propertyName: propertyName, defaultValue: defaultValue);}
	
	open func setStateProperty<T>(_ propertyName: String, propertyValue: T) -> T {return _setProperty(&_stateProperties, propertyName: propertyName, propertyValue: propertyValue);}
	
	open func clearStateProperty<T>(_ propertyName: String) -> T? {return _clearProperty(&_stateProperties, propertyName: propertyName);}
	
	/**Gets the number of decimals to round to.
	*/
	open func getDecimals() -> UInt8
	{
		return statePropertyWithDefault(CalculatorProperties.SIGNIFICANT_DECIMALS, defaultValue: {return 5;});
	}
	
	/**Set the number of significant decimal places
	*/
	open func setDecimals(_ num: UInt8)
	{
		let _ = setStateProperty(CalculatorProperties.SIGNIFICANT_DECIMALS, propertyValue: num);
	}
	
	open func getIntegerWordSize() -> IntegerWordSize
	{
		return statePropertyWithDefault(CalculatorProperties.INTEGER_WORD_SIZE, defaultValue: {return IntegerWordSize.none; });
	}
	
	open func setIntegerWordSize(_ size: IntegerWordSize)
	{
		let _ = setStateProperty(CalculatorProperties.INTEGER_WORD_SIZE, propertyValue: size);
		
		// run through the stack and convert all to new size.  This may have to be done to memory as well
		let ssize = self.size()
		
		for _ in 0..<ssize
		{
			// rely on the push function to properly adjust the stack
			pushAtEnd(popElement());
		}
	}
	
	open func getNumericBehavior() -> NumericBehavior
	{
		return statePropertyWithDefault(CalculatorProperties.NUMERIC_BEHAVIOR, defaultValue: {return NumericBehavior.Decimal; });
	}
	
	open func setNumericBehavior(_ behavior: NumericBehavior)
	{
		let _ = setStateProperty(CalculatorProperties.NUMERIC_BEHAVIOR, propertyValue: behavior);
		
		// sanity checks.  If changing to decimal, no word size is selected.
		if behavior == .Decimal
		{
			setIntegerWordSize(.none);
		}
		else
		{
			// select a default word size if None is current
			if getIntegerWordSize() == .none
			{
				setIntegerWordSize(.word_8);
			}
			else
			{
				// run through the stack and convert all to new size.  This may have to be done to memory as well
				let ssize = self.size()
				
				for _ in 0..<ssize
				{
					// rely on the push function to properly adjust the stack
					pushAtEnd(popElement());
				}
			}
		}
	}
	
	/**Gets the numeric base for data input and display.
	*/
	open func getNumericBase() -> NumericBase
	{
		return inputBuffer.numericBase();
	}
	
	/**Set the numeric base.
	*/
	open func setNumericBase(base: NumericBase)
	{
		inputBuffer.applyNumericBase(numericBase: base);
	}
	
	open func purgeNaNAndMessages() {
		while (!empty() && getXElement().isNaN)
		{
			let _ = pop();
		}
	}

	open func addDigitToNumericEntry(_ digit: LiteralDigit) throws
	{
		// if the current X-value is NaN or a message, reset it to 0.0
		purgeNaNAndMessages();

		switch (digit) {
			case ._0:
				currentInputBuffer().appendDigit(value: 0);
			case ._1:
				currentInputBuffer().appendDigit(value: 1);
			case ._2:
				currentInputBuffer().appendDigit(value: 2);
			case ._3:
				currentInputBuffer().appendDigit(value: 3);
			case ._4:
				currentInputBuffer().appendDigit(value: 4);
			case ._5:
				currentInputBuffer().appendDigit(value: 5);
			case ._6:
				currentInputBuffer().appendDigit(value: 6);
			case ._7:
				currentInputBuffer().appendDigit(value: 7);
			case ._8:
				currentInputBuffer().appendDigit(value: 8);
			case ._9:
				currentInputBuffer().appendDigit(value: 9);
			case ._10:
				currentInputBuffer().appendDigit(value: 10);
			case ._11:
				currentInputBuffer().appendDigit(value: 11);
			case ._12:
				currentInputBuffer().appendDigit(value: 12);
			case ._13:
				currentInputBuffer().appendDigit(value: 13);
			case ._14:
				currentInputBuffer().appendDigit(value: 14);
			case ._15:
				currentInputBuffer().appendDigit(value: 15);
			case .decimalPoint:
				currentInputBuffer().applyDecimal();
		}
	}
	
	open func resetInputBuffer() {
		inputBuffer.resetDigits();
	}
	
	open func currentInputNumericValue() -> NSDecimalNumber {
		// iterate over the digits of the string and create the numeric representation
		return currentInputBuffer().numericValue();
	}
	
	open func flipNumericEntrySignBit()
	{
		currentInputBuffer().negativeState(negative: !currentInputBuffer().isNegated());
	}
	
	open func getNumericEntrySignBit() -> Bool
	{
		return currentInputBuffer().isNegated();
	}
	
	open func numericEntryState() -> NumericEntryState
	{
		return propertyWithDefault(CalculatorProperties.NUMERIC_ENTRY_STATE, defaultValue: {return NumericEntryState.waiting_FOR_OPERATION; });
	}
	
	open func setNumericEntryState(_ state: NumericEntryState)
	{
		let _ = setProperty(CalculatorProperties.NUMERIC_ENTRY_STATE, propertyValue: state);
	}
	
	open func inNumericEntry() -> Bool
	{
		return numericEntryState() == NumericEntryState.in_NUMERIC_ENTRY;
	}
	
	open func beginNumericEntry()
	{
		let _ = setProperty(CalculatorProperties.NUMERIC_ENTRY_STATE, propertyValue: NumericEntryState.in_NUMERIC_ENTRY);
	}
	
	open func currentInputBuffer() -> InputBuffer
	{
		return inputBuffer;
	}
	
	open func printableInputImage() -> String
	{
		return currentInputBuffer().formattedTextBuffer();
	}

	open func currentInputBufferIncludesDecimal() -> Bool
	{
		return currentInputBuffer().hasDecimal();
	}
	
	open func clearLastDigitInNumericEntry()
	{
		currentInputBuffer().removeLastDigitOrDecimal();
	}
}

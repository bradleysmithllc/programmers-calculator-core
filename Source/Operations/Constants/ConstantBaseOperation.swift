//
//  ConstantBaseOperation.swift
//  programmers_calculator_core
//
//  Created by Bradley Smith on 1/21/22.
//  Copyright © 2022 Bradley Smith, LLC. All rights reserved.
//

import Foundation

open class ConstantBaseOperation: BasicOperation {
	fileprivate let _id: String;
	fileprivate let _mnemonic: String;
	fileprivate let _description: String;
	fileprivate let _value: NSDecimalNumber;

	init(__id: String, __mnemonic: String, __description: String, __value: NSDecimalNumber) {
		_id = __id;
		_mnemonic = __mnemonic;
		_description = __description;
		_value = __value;
	}
	
	open override func perform() throws {
		let stack = calculator.stack();
		
		// if the current X-value is NaN or a message, reset it to 0.0
		stack.purgeNaNAndMessages();

		stack.push(_value);
	}
	
	open override func id() -> String {
		return _id;
	}

	open override func mnemonic() -> String {
		return _mnemonic;
	}
	
	open override func description() -> String
	{
		return _description;
	}
}

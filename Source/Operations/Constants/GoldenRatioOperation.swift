//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Push the golden ratio onto the stack
**/

open class GoldenRatioOperation: ConstantBaseOperation {
	public static let G = NSDecimalNumber(string: "1.61803398874989484820458683436563811772030917980576");
		
	public required init() {
		super.init(__id: "gr_constant", __mnemonic: "gr", __description: "Golden Ratio constant.  Roughly 1.61803...", __value: GoldenRatioOperation.G);
	}
}

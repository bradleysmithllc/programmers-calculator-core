//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Push PI onto the stack
**/

open class PiOperation: ConstantBaseOperation {
	public static let PI = NSDecimalNumber(value: Double.pi);
	
	public required init() {
		super.init(__id: "pi_constant", __mnemonic: "π", __description: "PI constant.  Roughly 3.1415926...", __value: PiOperation.PI);
	}
}

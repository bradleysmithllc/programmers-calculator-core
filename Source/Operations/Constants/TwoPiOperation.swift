//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Push 2*PI onto the stack
**/

open class TwoPiOperation: ConstantBaseOperation {
	public static let TWO_PI = NSDecimalNumber(value: Double.pi).multiplying(by: NSDecimalNumber(value: 2 as Int));
	
	public required init() {
		super.init(__id: "two_pi_constant", __mnemonic: "2*π", __description: "2 * PI constant.", __value: TwoPiOperation.TWO_PI);
	}
}

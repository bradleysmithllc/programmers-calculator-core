//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Push E onto the stack
**/

open class EOperation: ConstantBaseOperation {
	public static let E = NSDecimalNumber(value: M_E as Double);
	
	public required init() {
		super.init(__id: "e_constant", __mnemonic: "e", __description: "E constant.", __value: EOperation.E);
	}
}

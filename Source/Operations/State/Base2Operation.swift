//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Clears the X register.
  **/

open class Base2Operation: BasicOperation {
	open override func perform() throws {
		stack.setNumericBase(base: NumericBase.base2);
		
		if (stack.getNumericBehavior() == .Decimal)
		{
			stack.setNumericBehavior(.UnsignedInteger);
		}
	}
	
	open override func id() -> String {
		return "bin";
	}
	
	open override func description() -> String
	{
		return "Sets the calculator to use binary, or base-2, numerics.";
	}
	
	open override func validateStack() -> Bool {
		return stack.getNumericBase() != NumericBase.base2;
	}

	open override func decoratedMnemonic() -> String {
		return "<math><msub><mi>x</mi><mn>2</mn></msub></math>";
	}

	open override func mnemonic() -> String {
		return "x[2]";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}

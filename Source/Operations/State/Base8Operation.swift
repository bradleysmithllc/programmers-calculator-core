//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Clears the X register.
  **/

open class Base8Operation: BasicOperation {
	open override func perform() throws {
		stack.setNumericBase(base: NumericBase.base8);

		if (stack.getNumericBehavior() == .Decimal)
		{
			stack.setNumericBehavior(.UnsignedInteger);
		}
	}
	
	open override func id() -> String {
		return "oct";
	}
	
	open override func description() -> String
	{
		return "Sets the calculator to use octal, or base-8, numerics.";
	}

	open override func validateStack() -> Bool {
		return stack.getNumericBase() != NumericBase.base8;
	}

	open override func decoratedMnemonic() -> String {
		return "<math><msub><mi>x</mi><mn>8</mn></msub></math>";
	}
	
	open override func mnemonic() -> String {
		return "x[8]";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Clears the X register.
  **/

open class Base16Operation: BasicOperation {
	open override func perform() throws {
		stack.setNumericBase(base: NumericBase.base16);

		if (stack.getNumericBehavior() == .Decimal)
		{
			stack.setNumericBehavior(.UnsignedInteger);
		}
	}
	
	open override func description() -> String
	{
		return "Sets the calculator to use hexadecimal, or base-16, numerics.";
	}
	
	open override func id() -> String {
		return "hex";
	}
	
	open override func validateStack() -> Bool {
		return stack.getNumericBase() != NumericBase.base16;
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><msub><mi>x</mi><mn>16</mn></msub></math>";
	}
	
	open override func mnemonic() -> String {
		return "x[16]";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}

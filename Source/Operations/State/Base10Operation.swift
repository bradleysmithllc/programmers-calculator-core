//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Sets the numeric format to base 10.
  **/

open class Base10Operation: BasicOperation {
	open override func perform() throws {
		stack.setNumericBase(base: NumericBase.base10);
	}
	
	open override func id() -> String {
		return "dec";
	}
	
	open override func description() -> String
	{
		return "Sets the calculator to use decimal, or base-10, for display.";
	}
	
	open override func validateStack() -> Bool {
		return stack.getNumericBase() != NumericBase.base10;
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><msub><mi>x</mi><mn>10</mn></msub></math>";
	}
	
	open override func mnemonic() -> String {
		return "x[10]";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}

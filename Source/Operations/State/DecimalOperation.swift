//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Sets the number of significant digits to the right of the decimal point
  **/

open class DecimalOperation: BasicOperation {
	open override func perform() throws {
		let dec = calculator.stack().pop().intValue;

		if (dec < 0 || dec > 38)
		{
			calculator.stack().push(NSDecimalNumber.notANumber);
		}
		else
		{
			stack.setDecimals(UInt8(dec));
		}
	}
	
	open override func id() -> String {
		return "decimal";
	}
	
	open override func validateStack() -> Bool {
		return stack.getNumericBase() == NumericBase.base10;
	}

	open override func mnemonic() -> String {
		return "0.0N→";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
	
	open override func description() -> String
	{
		return "Sets the number of decimal places to use.";
	}
}

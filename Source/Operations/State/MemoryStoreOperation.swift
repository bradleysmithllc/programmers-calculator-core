//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Stores the X register in a dedicated memory slot.  Does not pop the stack.
  **/

open class MemoryStoreOperation: BasicOperation {
	open override func perform() throws {
		var stack = calculator.stack();

		let x = stack.peek();
		
		let _ = stack.assignMemory(MemoryPlusOperation.MEMORY_NAME, value: x);
	}
	
	open override func id() -> String {
		return "sm";
	}

	open override func mnemonic() -> String {
		return "sm";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
	
	open override func description() -> String
	 {
		 return "Stores X in memory";
	 }
}

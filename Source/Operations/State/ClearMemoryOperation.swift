//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Clears the memory register.
  **/

open class ClearMemoryOperation: BasicOperation {
	open override func perform() throws {
		var stack = calculator.stack();

		let _ = stack.clearMemory(MemoryPlusOperation.MEMORY_NAME);
	}
	
	open override func id() -> String {
		return "cm";
	}

	open override func mnemonic() -> String {
		return "cm";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
	
	open override func description() -> String
	 {
		 return "Clears memory";
	 }
}

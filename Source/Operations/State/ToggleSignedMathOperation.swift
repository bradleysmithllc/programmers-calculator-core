//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
*
**/

open class ToggleSignedMathOperation: BasicOperation {
	open override func perform() throws {
		switch (stack.getNumericBehavior())
		{
		case .Decimal:
			stack.setNumericBehavior(.SignedInteger);
		case .SignedInteger:
			stack.setNumericBehavior(.UnsignedInteger);
		case .UnsignedInteger:
			if (stack.getNumericBase() == NumericBase.base10)
			{
				stack.setNumericBehavior(.Decimal);
			}
			else
			{
				stack.setNumericBehavior(.SignedInteger);
			}
		}
	}
	
	open override func description() -> String
	{
		return "Toggles the numeric modes.<br />Available modes:  <table><thead><th>Mode</th><th>Meaning</th></thead><tbody><tr><td>signed</td><td>Signed integer math.</td></tr><tr><td>unsigned</td><td>Unsigned integer math</td></tr><tr><td>dec</td><td>Floating-point decimal math.</td></tr></tbody></table>";
	}
	
	open override func id() -> String {
		return "unsign";
	}
	
	open override func mnemonic() -> String {
		switch (stack.getNumericBehavior())
		{
		case .Decimal:
			return "dec";
		case .SignedInteger:
			return "signed";
		case .UnsignedInteger:
			return "unsigned";
		}
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}

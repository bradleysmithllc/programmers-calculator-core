//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Stores the X register in a dedicated memory slot.  Does not pop the stack.
  **/

open class MemoryPlusOperation: BasicOperation {
	public static let MEMORY_NAME = "~~Memory~~";
	
	open override func perform() throws {
		var stack = calculator.stack();

		let x = stack.peek();
		let mem = stack.memory(MemoryPlusOperation.MEMORY_NAME);
		
		let _ = stack.assignMemory(MemoryPlusOperation.MEMORY_NAME, value: mathHub.add(left: x, right: mem));
	}
	
	open override func id() -> String {
		return "m+";
	}

	open override func mnemonic() -> String {
		return "m+";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
	
	open override func description() -> String
	 {
		 return "Stores memory + X in memory";
	 }
}

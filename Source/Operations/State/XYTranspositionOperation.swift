//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
 * Pops the bottom two elements off the stack and pushes them back on in reverse order.  If there is no Y
 * element, (second position), 0.0 is used instead.
 **/

open class XYTranspositionOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x = stack.popElement();
		let y = stack.popElement();

		// push them back on in reverse order
		stack.push(x);
		stack.push(y);
	}
	
	open override func id() -> String {
		return "x<->y";
	}

	open override func description() -> String
	{
		return "Swap X and Y registers.";
	}

	open override func mnemonic() -> String {
		return "x/y";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}

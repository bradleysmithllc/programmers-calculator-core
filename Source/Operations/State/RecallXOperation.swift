//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Retrieves the value in the memory slot identified by the X register.  The memory
  * slot value is copied onto the X register, the slot identifier is popped off.
  **/

open class RecallXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let slotId: NSDecimalNumber = stack.pop();

		stack.push(stack.memory(String(slotId.intValue)));
	}
	
	open override func id() -> String {
		return "recall";
	}

	open override func mnemonic() -> String {
		return "rcl";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
	
	open override func description() -> String
	 {
		 return "Retrieves the value in the memory slot identified by the X register.  The memory slot value is copied onto the X register, the slot identifier is popped off";
	 }
}

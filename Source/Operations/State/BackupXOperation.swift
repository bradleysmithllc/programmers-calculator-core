//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Clears the X register.
  **/

open class BackupXOperation: BasicOperation {
	open override func perform() throws {
		// there are two modes here
		let inNumericEntry = stack.inNumericEntry();
		
		// 1 - If in numeric entry, remove the last character.
		if (inNumericEntry)
		{
			stack.clearLastDigitInNumericEntry();
		}
		else
		{
			// clear the x register
			stack.setX(NSDecimalNumber.zero);
		}
	}
	
	public final override func registerResultState() -> NumericEntryState
	{
		return NumericEntryState.in_NUMERIC_ENTRY;
	}

	open override func id() -> String {
		return "bckpx";
	}

	open override func mnemonic() -> String {
		return "←";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
	
	open override func description() -> String
	 {
		 return "Undoes the last input if in numeric entry, otherwise clears the X register";
	 }
}

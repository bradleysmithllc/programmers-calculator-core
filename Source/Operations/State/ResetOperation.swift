//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Resets the Calculator state
  **/

open class ResetOperation: BasicOperation {
	open override func perform() throws {
		calculator.reset();
	}
	
	open override func id() -> String {
		return "rst";
	}

	open override func mnemonic() -> String {
		return "AC";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
	
	open override func description() -> String
	 {
		 return "Resets calculator state";
	 }
}

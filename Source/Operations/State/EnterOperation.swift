//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Peeks the bottom of the stack and pops it back on - pushing the whole stack upwards.
**/

open class EnterOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		if (stack.size() >= 1)
		{
			let peek = stack.getXElement();
			
			// check for NaN or a message.  In this case, pop it off and push 0
			if (
				peek.isNaN
			) {
				stack.purgeNaNAndMessages();
				stack.push(NSDecimalNumber.zero);
			} else {
				stack.push(peek);
			}
		}
	}
	
	open override func id() -> String {
		return "ent";
	}

	open override func mnemonic() -> String {
		return "ent";
	}
	
	open override func description() -> String
	{
		return "Pushes the X register onto the stack, moving everything upwards.  Enter also completes numeric entry.  E.G., type a '1' then a '2', and the X register will contain 12.  Type 'ent', and the 12 is pushed onto the stack (so now the X and Y registers contain 12).  Press '3', and the X resgister contains 3, Y register contains 12, and the next stack element above Y will also contain 12.";
	}

	public final override func registerResultState() -> NumericEntryState
	{
		return NumericEntryState.waiting_FOR_NUMERIC_ENTRY;
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Recalls (pushes) the memory register to the stack.
  **/

open class RecallMemoryOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let mem = stack.memory(MemoryPlusOperation.MEMORY_NAME);

		stack.push(mem);
	}
	
	open override func id() -> String {
		return "rm";
	}

	open override func mnemonic() -> String {
		return "rm";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
	
	open override func description() -> String
	 {
		 return "Recalls memory into X";
	 }
}

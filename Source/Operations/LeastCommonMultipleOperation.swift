//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Finds the least common multiple of numbers on the stack
**/

open class LeastCommonMultipleOperation: BasicOperation {

	open override func perform() throws {
		let stack = calculator.stack();

		// check for special cases.  If stack is empty, result is 0
		if (stack.size() == 0)
		{
			stack.push(NSDecimalNumber.zero);
		}
		else
		{
			// loop through the list, factoring to primes.  If any number is 0, result is 0.  If any number is NaN, result is NaN.
			let stacks = stack.popAll();

			var primeFactors = [NSDecimalNumber: NSDecimalNumber]();

			for stackElement in stacks
			{
				if (stackElement.number() == 0)
				{
					// stop and record 0 as the result
					stack.push(NSDecimalNumber.zero);
					return;
				}
				else if (stackElement.number() == NSDecimalNumber.notANumber)
				{
					// stop and record 0 as the result
					stack.push(NSDecimalNumber.notANumber);
					return;
				}

				// factor into primes
				let pa = ReduceFractionOperation.factor(stackElement.number(), mathHub: mathHub)
				// if there is any remainder, result is NaN
				if (pa.remainder.int64Value > 1)
				{
					// stop and record 0 as the result
					stack.push(NSDecimalNumber.notANumber);
					return;
				}

				LeastCommonMultipleOperation.comparePrimeFactors(pa.factors, primeFactors: &primeFactors);
			}

			// remultiply factors and push onto stack
			stack.push(LeastCommonMultipleOperation.reconstitute(primeFactors, mathHub: mathHub));
		}
	}

	public static func reconstitute(_ factors: [NSDecimalNumber: NSDecimalNumber], mathHub: MathHub) -> NSDecimalNumber
	{
		var result = NSDecimalNumber.one;

		for (factor, exponent) in factors
		{
			result = mathHub.multiply(left: result, right: mathHub.power(base: factor, exponent: exponent));
		}

		return result;
	}

	public static func comparePrimeFactors(_ factors: [NSDecimalNumber], primeFactors: inout [NSDecimalNumber: NSDecimalNumber])
	{
		// run through each prime and compare to the current max power
		let primePowers: [NSDecimalNumber: NSDecimalNumber] = LeastCommonMultipleOperation.powerPrimes(factors);
		
		for (factor, power) in primePowers
		{
			// check for a new prime or a bigger power
			if let pow = primeFactors[factor]
			{
				if power.compare(pow) == .orderedDescending
				{
					primeFactors[factor] = power;
				}
			}
			else
			{
				primeFactors[factor] = power;
			}
		}
	}

	open override func id() -> String {
		return "lcm";
	}

	open override func mnemonic() -> String {
		return "lcm";
	}
	
	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Pop, .Push, .Push];
	}
	
	open override func description() -> String
	{
		return "Finds least common multiples of numbers on the stack.";
	}

	public static func powerPrimes(_ factors: [NSDecimalNumber]) ->  [NSDecimalNumber: NSDecimalNumber]
	{
		var p: [NSDecimalNumber: NSDecimalNumber] = [:];

		for f in factors
		{
			if let n = p[f]
			{
				p[f] = n.adding(NSDecimalNumber.one);
			}
			else
			{
				p[f] = NSDecimalNumber.one;
			}
		}

		return p;
	}
}

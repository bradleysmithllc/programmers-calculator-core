//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Remove the sign of the X register
  **/

open class AbsoluteValueXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let currX: NSDecimalNumber = stack.getX();
		stack.setX(currX.decimalNumberWithAbsoluteValue());
	}

	open override func id() -> String {
		return "absx";
	}
	
	open override func mnemonic() -> String {
		return "|x|";
	}

	open override func description() -> String
	{
		return "Replaces the value in the x register with the absolute value of the current value of x";
	}
}

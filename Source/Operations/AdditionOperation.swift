//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last two operands from the stack, add them together, then push the results back on the stack
**/

open class AdditionOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let num1: NSDecimalNumber! = stack.pop();
		let num2: NSDecimalNumber! = stack.pop();

		stack.push(mathHub.add(left: num1, right: num2));
		
		//Thread.sleep(forTimeInterval: 4);
	}
	
	open override func id() -> String {
		return "+";
	}

	open override func mnemonic() -> String {
		return "+";
	}
	
	open override func operationType() -> OperationType
	{
		return .arithmetic_operator;
	}

	open override func description() -> String
	{
		return "Replaces the value in the x register with the sum of the current value of the x and y registers";
	}
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Binary left-shift bits in Y X-times.  X popped, lshifted Y left in X
  **/

open class YLeftShiftXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let xstack = stack.pop();
		let x = xstack.safeLongLongValue;

		let ystack = stack.pop();
		let y = ystack.safeLongLongValue;
		
		// if x > 63 then result is 0 (because all bits fall off the right side)
		if (x > 63)
		{
			stack.push(NSDecimalNumber.zero);
		}
		else if (x == 0)
		{
			stack.push(ystack);
		}
		else if (x < 0)
		{
			stack.push(NSDecimalNumber.notANumber);
		}
		else
		{
			stack.push(NSDecimalNumber(value: y << x as Int64));
		}
	}

	open override func id() -> String {
		return "ylshiftx";
	}

	open override func mnemonic() -> String {
		return "y<<x";
	}
	
	open override func decoratedMnemonic() -> String {
		return "y&lt;&lt;x";
	}
	
	open override func description() -> String
	 {
		 return "Binary left-shift bits in Y X-times.  X popped, lshifted Y left in X";
	 }
}

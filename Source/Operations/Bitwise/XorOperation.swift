//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Binary y exclusive-or x.  Uses the longest possible bit field
  **/

open class XorOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		stack.push(NSDecimalNumber(value: stack.pop().safeLongLongValue ^ stack.pop().safeLongLongValue as Int64));
	}

	open override func id() -> String {
		return "xor";
	}
	
	open override func mnemonic() -> String {
		return "xor";
	}
	
	open override func description() -> String
	 {
		 return "Binary y exclusive-or x.  Result in X";
	 }
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Binary left-shift bits in Y X-times.  X popped, lshifted Y left in X
  **/

open class YRightShiftXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x = stack.pop().safeLongLongValue;
		let y = stack.pop().safeLongLongValue;
		
		// if x > 63 then result is 0 (because all bits fall off the left side		
		if (x > 63)
		{
			stack.push(NSDecimalNumber.zero);
		}
		else if (x < 0)
		{
			stack.push(NSDecimalNumber.notANumber);
		}
		else
		{
			stack.push(NSDecimalNumber(value: y >> x as Int64));
		}
	}

	open override func id() -> String {
		return "yrshiftx";
	}

	open override func decoratedMnemonic() -> String {
		return "y&gt;&gt;x";
	}

	open override func mnemonic() -> String {
		return "y>>x";
	}
	
	open override func description() -> String
	 {
		 return "Binary left-shift bits in Y X-times.  X popped, lshifted Y left in X";
	 }
}

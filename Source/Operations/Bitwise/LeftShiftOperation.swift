//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Binary shift bits left.  Replaces X register.
  **/

open class LeftShiftOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		stack.push(NSDecimalNumber(value: stack.pop().safeLongLongValue << 1 as Int64));
	}

	open override func id() -> String {
		return "lshift";
	}

	open override func mnemonic() -> String {
		return "<<";
	}
	
	open override func decoratedMnemonic() -> String {
		return "&lt;&lt;";
	}
	
	open override func description() -> String
	 {
		 return "Binary shift bits left.  Result in X";
	 }
}

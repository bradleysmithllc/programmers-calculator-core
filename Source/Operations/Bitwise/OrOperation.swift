//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Binary y or x.  Uses the longest possible bit field
  **/

open class OrOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		stack.push(NSDecimalNumber(value: stack.pop().safeLongLongValue | stack.pop().safeLongLongValue as Int64));
	}

	open override func id() -> String {
		return "or";
	}
	
	open override func mnemonic() -> String {
		return "or";
	}
	
	open override func description() -> String
	 {
		 return "Binary y or x.  Result in X";
	 }
}

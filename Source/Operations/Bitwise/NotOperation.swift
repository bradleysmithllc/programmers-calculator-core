//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Binary not x.  Uses the longest possible bit field
  **/

open class UnaryNotOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		stack.push(NSDecimalNumber(value: ~(stack.pop().safeLongLongValue) as Int64));
	}

	open override func id() -> String {
		return "not";
	}
	
	open override func formula() -> String?
	{
		return "~<stack-x />"
	}
 
	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Push];
	}

	open override func description() -> String
	{
		return "Unary bitwise not.  Flips all bits int the integer.";
	}

	open override func mnemonic() -> String {
		return "not";
	}
}

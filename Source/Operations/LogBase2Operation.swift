//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* y mod x
**/

open class LogBase2Operation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let mod: NSDecimalNumber = stack.pop();

		stack.push(NSDecimalNumber(value: log2(mod.doubleValue) as Double));
	}
	
	open override func id() -> String {
		return "log2x";
	}

	open override func decoratedMnemonic() -> String {
		return "<math><msub><mi>log</mi><mn>2</mn></msub></math>";
	}

	open override func mnemonic() -> String {
		return "log[2]";
	}
	
	open override func description() -> String
	{
		return "Base 2 Logarithm";
	}
}

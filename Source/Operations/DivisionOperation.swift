//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pops the dividend and divisor from the stack, divides them, then pushes the result back on.
**/

open class DivisionOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let divisor: NSDecimalNumber! = stack.pop();
		let dividend: NSDecimalNumber! = stack.pop();

		stack.push(mathHub.divide(dividend: dividend, divisor: divisor));
	}
	
	open override func id() -> String {
		return "/";
	}

	open override func mnemonic() -> String {
		return "÷";
	}
	
	open override func operationType() -> OperationType
	{
		return .arithmetic_operator;
	}
	
	open override func description() -> String
	 {
		 return "Push X / Y";
	 }
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last operand from the stack, push x raised to the second power.
**/

open class SquareRootXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.pop();

		stack.push(NSDecimalNumber(value: sqrt(x.doubleValue) as Double));
	}
	
	open override func id() -> String {
		return "sqrtx";
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><msqrt><mi>x</mi></msqrt></math>";
	}

	open override func mnemonic() -> String {
		return "{2}√x";
	}
	
	open override func description() -> String
	 {
		 return "Store the square root of X in X";
	 }
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop all operands off the stack, adding them together.  Push the result.
**/

open class SumOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		var sum: NSDecimalNumber = NSDecimalNumber.zero;

		while (!stack.empty())
		{
			sum = mathHub.add(left: sum, right: stack.pop());
		}

		stack.push(sum);
	}
	
	open override func id() -> String {
		return "sum";
	}

	open override func mnemonic() -> String {
		return "Σ(n)";
	}

	open override func operationType() -> OperationType
	{
		return .enary;
	}

	open override func description() -> String
	 {
		 return "Sums all stack values and pushes the result in X";
	 }
}

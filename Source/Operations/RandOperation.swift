//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pushes a random number between 0 and 1 onto the stack.
**/

open class RandOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		tgamma(1.0);
		stack.push(
			mathHub.divide(dividend: NSDecimalNumber(decimal: Decimal(arc4random())), divisor: NSDecimalNumber(value: Int.max as Int))
		);
	}

	open override func id() -> String {
		return "rand";
	}

	open override func mnemonic() -> String {
		return "rand";
	}
	
	open override func description() -> String
	{
		return "Generates a pseudo-random number.";
	}
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last operand from the stack, push x raised to the second power.
**/

open class XSquaredOperation: BasicOperation {
	fileprivate static let TWO: NSDecimalNumber = NSDecimalNumber(value: 2 as Int);

	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.pop();

		stack.push(mathHub.power(base: x, exponent: XSquaredOperation.TWO));
	}
	
	open override func id() -> String {
		return "x^2";
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><msup><mi>x</mi><mn>2</mn></msup></math>";
	}
	
	open override func mnemonic() -> String {
		return "x{2}";
	}
	
	open override func operationType() -> OperationType
	{
		return .binary;
	}
	
	open override func description() -> String
	{
		return "Pop last operand from the stack, push x raised to the second power";
	}
}

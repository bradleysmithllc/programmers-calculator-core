//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Removes fractional portion of a number
**/

open class TruncateFractionalOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let val: NSDecimalNumber! = stack.pop();

		let intx = val.decimalNumberTruncated();
		
		stack.push(intx);
	}

	open override func validateStack() -> Bool {
		return stack.getNumericBase() == NumericBase.base10;
	}

	open override func id() -> String {
		return "intx";
	}

	open override func mnemonic() -> String {
		return "intx";
	}
	
	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Push];
	}
	
	open override func description() -> String
	{
		return "Truncates fractional portion.  E.G., 5.2 becomes 5";
	}
	
	open override func operationType() -> OperationType
	{
		return .unary;
	}
}

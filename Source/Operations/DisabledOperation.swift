//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Does nothing but be disabled.
  **/

open class DisabledOperation: BasicOperation {
	open override func validateStack() -> Bool {
		return false;
	}

	open override func id() -> String {
		return "dis";
	}
	
	open override func mnemonic() -> String {
		return "␀";
	}
	
	open override func description() -> String
	 {
		 return ">> disabled <<";
	 }
}

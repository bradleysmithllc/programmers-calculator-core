//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pops X, multiplies X by 100 and pushes the result.
**/

open class ToPercentOperation: BasicOperation {
	static let _100 = NSDecimalNumber(value: 100 as Int);

	open override func perform() throws {
		let stack = calculator.stack();

		let base: NSDecimalNumber! = stack.pop();

		let percentDecimal: NSDecimalNumber = mathHub.divide(dividend: base, divisor: ToPercentOperation._100);

		stack.push(percentDecimal);
	}

	open override func id() -> String {
		return "to-percent";
	}

	open override func mnemonic() -> String {
		return "→%";
	}

	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Push];
	}
	
	open override func formula() -> String?
	{
		return
			"<math xmlns=\"http://www.w3.org/1998/Math/MathML\">" +
				"<mrow>" +
					"<mfrac>" +
						"<mi><stack-x /></mi>" +
						"<mn>100</mn>" +
					"</mfrac>" +
				"</mrow>" +
			"</math>";
	}
	
	open override func description() -> String
	{
		return "Converts printable percentage (NN%) into a decimal (.NN)";
	}
}

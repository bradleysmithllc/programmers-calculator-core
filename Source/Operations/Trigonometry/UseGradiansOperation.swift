//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pops the bottom stack, the X-register, off.
**/

open class UseGradiansOperation: BasicOperation {
	open override func perform() throws {
		TrigBaseOperation.operationsMode(calculator, setTo: TrigBaseOperation.TrigMode.gradians)
	}

	open override func id() -> String {
		return "grad";
	}

	open override func mnemonic() -> String {
		return "grad";
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
	
	open override func description() -> String
	{
		return "Changes trig functions to use gradians.";
	}
}

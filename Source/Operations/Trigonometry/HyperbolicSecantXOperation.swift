//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last two operands from the stack, push y raised to the x power.
**/

open class HyperbolicSecantXOperation: TrigBaseOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.getX();
		let x_input: NSDecimalNumber = getTrigInput(x, calculator: calculator);

		stack.setX(
		mathHub.divide(dividend: NSDecimalNumber.one, divisor: NSDecimalNumber(value: cosh(x_input.doubleValue) as Double))
		);
	}
	
	open override func id() -> String {
		return "sechx";
	}

	open override func mnemonic() -> String {
		return "sech";
	}
	
	open override func description() -> String
	{
		return "Hyperbolic secant of X";
	}
}

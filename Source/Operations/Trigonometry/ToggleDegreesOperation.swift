//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pops the bottom stack, the X-register, off.
**/

open class ToggleDegreesOperation: BasicOperation {
	open override func perform() throws {
		let mode = TrigBaseOperation.operationsMode(calculator);

		switch (mode)
		{
			case .degrees:
				TrigBaseOperation.operationsMode(calculator, setTo: .radians);
			case .radians:
				TrigBaseOperation.operationsMode(calculator, setTo: .gradians);
			case .gradians:
				TrigBaseOperation.operationsMode(calculator, setTo: .degrees);
		}
	}
	
	fileprivate func operationsMode() -> TrigBaseOperation.TrigMode
	{
		return TrigBaseOperation.operationsMode(calculator);
	}

	open override func id() -> String {
		return "degrad";
	}

	open override func mnemonic() -> String {
		switch (operationsMode())
		{
		case .radians:
			return "rad";
		case .gradians:
			return "grad";
		case .degrees:
			return "deg";
		}
	}
	
	open override func operationType() -> OperationType
	{
		return .state;
	}
	
	open override func description() -> String
	{
		return "Toggle degrees and radians";
	}
}

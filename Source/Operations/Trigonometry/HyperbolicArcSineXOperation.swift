//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last two operands from the stack, push y raised to the x power.
**/

open class HyperbolicArcSineXOperation: TrigBaseOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.getX();
		let x_input: NSDecimalNumber = getTrigInput(x, calculator: calculator);

		stack.setX(NSDecimalNumber(value: asinh(x_input.doubleValue) as Double));
	}
	
	open override func id() -> String {
		return "asinhx";
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><msup><mi>sinh</mi><mn>-1</mn></msup></math>";
	}
	
	open override func mnemonic() -> String {
		return "sinh{-1}";
	}
	
	open override func description() -> String
	{
		return "Hyperbolic arc sine of X";
	}
}

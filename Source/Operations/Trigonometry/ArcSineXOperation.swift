//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last two operands from the stack, push y raised to the x power.
**/
open class ArcSineXOperation: TrigBaseOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.getX();
		let x_result: NSDecimalNumber = NSDecimalNumber(value: asin(x.doubleValue) as Double);

		stack.setX(getTrigOutput(x_result, calculator: calculator));
	}
	
	open override func id() -> String {
		return "asinx";
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><msup><mi>sin</mi><mn>-1</mn></msup></math>";
	}
	
	open override func mnemonic() -> String {
		return "sin{-1}";
	}
	
	open override func description() -> String
	{
		return "Arc Sine of X";
	}
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Stores the X register value in the index register.
  **/

open class StoreIndexOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		// remove whatever is there and push onto the index
		stack.setIndex(index: stack.pop());
	}

	open override func mnemonic() -> String {
		return "stoi";
	}
	
	open override func description() -> String
	{
		return "Store X in the index register";
	}
}

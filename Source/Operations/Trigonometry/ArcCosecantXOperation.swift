//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last two operands from the stack, push y raised to the x power.
**/

open class ArcCosecantXOperation: TrigBaseOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.getX();
		let x_input: NSDecimalNumber = getTrigInput(x, calculator: calculator);

		stack.setX(
			mathHub.divide(dividend: NSDecimalNumber.one, divisor: NSDecimalNumber(value: asin(x_input.doubleValue) as Double))
		);
	}
	
	open override func id() -> String {
		return "acscx";
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><msup><mi>csc</mi><mn>-1</mn></msup></math>";
	}

	open override func mnemonic() -> String {
		return "csc{-1}";
	}
	
	open override func description() -> String
	{
		return "Arc Cosecant of X";
	}
}

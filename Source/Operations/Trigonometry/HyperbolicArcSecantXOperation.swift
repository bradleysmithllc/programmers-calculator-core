//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last two operands from the stack, push y raised to the x power.
**/

open class HyperbolicArcSecantXOperation: TrigBaseOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.getX();
		let x_input: NSDecimalNumber = getTrigInput(x, calculator: calculator);

		stack.setX(
		mathHub.divide(dividend: NSDecimalNumber.one, divisor: NSDecimalNumber(value: acosh(x_input.doubleValue) as Double))
		);
	}
	
	open override func id() -> String {
		return "asechx";
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><msup><mi>sech</mi><mn>-1</mn></msup></math>";
	}
	
	open override func mnemonic() -> String {
		return "sech{-1}";
	}
	
	open override func description() -> String
	{
		return "Hyperbolic arc secant of x";
	}
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last two operands from the stack, push y raised to the x power.
**/

open class CosecantXOperation: TrigBaseOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.getX();
		let x_input: NSDecimalNumber = getTrigInput(x, calculator: calculator);

		stack.setX(
		mathHub.divide(dividend: NSDecimalNumber.one, divisor: NSDecimalNumber(value: sin(x_input.doubleValue) as Double))
		);
	}
	
	open override func id() -> String {
		return "cscx";
	}

	open override func mnemonic() -> String {
		return "csc";
	}
	
	open override func description() -> String
	{
		return "Cosecant of X";
	}
}

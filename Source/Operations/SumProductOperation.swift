//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop all operands off the stack in pairs, adding the products together.  Push the result.
* If the stack contains un odd number of items, the last is unmodified.
* E.G., SumProduct(1,2,3,4) = (1*2) + (3*4) = 14 on the stack
* E.G., SumProduct(1,2,3,4,5) = (1*2) + (3*4) = 5, 14 on the stack
**/

open class SumProductOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		var sum: NSDecimalNumber = NSDecimalNumber.zero;

		while (stack.size() >= 2)
		{
			sum = mathHub.add(left: sum, right: mathHub.multiply(left: stack.pop(), right: stack.pop()));
		}

		stack.push(sum);
	}
	
	open override func id() -> String {
		return "sum-product";
	}

	open override func mnemonic() -> String {
		return "Σ(xy)";
	}
	
	open override func description() -> String
	 {
		 return "Pop all operands off the stack in pairs, adding the products together.  Push the result. If the stack contains un odd number of items, the last is unmodified";
	 }

	open override func operationType() -> OperationType
	{
		return .enary;
	}
}

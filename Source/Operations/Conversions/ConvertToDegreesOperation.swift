//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop x, convert to degrees from current mode, push
**/

open class ConvertToDegreesOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();
		
		let input: NSDecimalNumber! = stack.pop();
		
		switch(TrigBaseOperation.operationsMode(calculator))
		{
		case .radians:
			stack.push(mathHub.degreesFromRadians(radians: input));
		case .degrees:
			stack.push(input);
		case .gradians:
			stack.push(mathHub.degreesFromGradians(gradians: input));
		}
	}
	
	open override func id() -> String {
		return "to_degrees";
	}

	open override func mnemonic() -> String {
		return "→DEG";
	}
	
	open override func description() -> String
	 {
		 return "Pop x, convert to degrees from current mode, push";
	 }
}

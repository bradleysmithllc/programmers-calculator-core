//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Converts date in yyyymmdd format to a julian number
**/

open class ConvertJulianTODateOperation: BasicOperation {
	open override func perform() throws {
		let jd = stack.pop();

		let fmt = DateFormatter();
		fmt.dateFormat = "g";

		let jdl = jd.decimalNumberRoundedToDigits(withDecimals: 0, roundingMode: NSDecimalNumber.RoundingMode.down);
		
		if let date = fmt.date(from: String(describing: jdl))
		{
			let ifmt = DateFormatter();
			ifmt.dateFormat = "yyyyMMdd";
			
			stack.push(NSDecimalNumber(string: ifmt.string(from: date)));
		}
		else
		{
			stack.push("Invalid Date");
		}
	}
	
	
	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Push];
	}
	
	open override func description() -> String
	{
		return "Given an integral Julian date in the X register, pop the value off and convert to the Date and push that back into X.";
	}
	
	open override func id() -> String {
		return "to_date";
	}

	open override func mnemonic() -> String {
		return "→Date";
	}
}

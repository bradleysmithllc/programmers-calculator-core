//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop x, convert to radians from current mode, push
**/

open class ConvertToRadiansOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let input: NSDecimalNumber! = stack.pop();

		switch(TrigBaseOperation.operationsMode(calculator))
		{
			case .radians:
				stack.push(input);
			case .degrees:
				stack.push(mathHub.radiansFromDegrees(degrees: input));
			case .gradians:
				stack.push(mathHub.radiansFromGradians(gradians: input));
		}
	}
	
	open override func id() -> String {
		return "to_radians";
	}

	open override func mnemonic() -> String {
		return "→RAD";
	}
	
	open override func description() -> String
	 {
		 return "Pop x, convert to radians from current mode, push";
	 }
}

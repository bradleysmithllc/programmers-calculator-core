//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Converts date in yyyymmdd format to a julian number
**/

open class ConvertDateToJulianDayOperation: BasicOperation {
	open override func perform() throws {
		let cal = Calendar(identifier: Calendar.Identifier.gregorian);
		var comp = DateComponents();

		let dateSpec = stack.pop().decimalNumberTruncated();

		// populate the year, month and day from the 'spec'
		comp.year = dateSpec.intValue / 10000;
		comp.month = (dateSpec.intValue % 10000) / 100;
		comp.day = dateSpec.intValue % 100;

		if let date = cal.date(from: comp)
		{
			let fmt = DateFormatter();
			fmt.dateFormat = "g";
			let julianDays = fmt.string(from: date);
			
			stack.push(NSDecimalNumber(string: julianDays));
		}
		else
		{
			stack.push("Invalid Date");
		}
	}
	
	
	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Push];
	}
	
	open override func description() -> String
	{
		return "Given a date in the format yyyymmdd in the X register, pop the date off and convert to the Julian Day and push that back into X.";
	}
	
	open override func id() -> String {
		return "to_julian_day";
	}

	open override func mnemonic() -> String {
		return "→Jul";
	}
}

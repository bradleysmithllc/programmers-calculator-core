//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pops the bottom stack, the X-register, off.
**/

open class ReciprocalXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let divis: NSDecimalNumber = stack.pop();

		// put under 1
		stack.push(mathHub.divide(dividend: NSDecimalNumber.one, divisor: divis));
	}

	open override func id() -> String {
		return "1/x";
	}

	open override func mnemonic() -> String {
		return "1/x";
	}
	
	open override func description() -> String
	 {
		 return "Store 1/X in X";
	 }
}

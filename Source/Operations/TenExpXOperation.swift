//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last operand from the stack, push 10 raised to the x power.
**/

open class TenExpXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.pop();

		stack.push(mathHub.power(base: NSDecimalNumber(decimal: 10), exponent: x));
	}
	
	open override func id() -> String {
		return "10^x";
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><msup><mn>10</mn><mi>x</mi></msup></math>";
	}

	open override func mnemonic() -> String {
		return "10{x}";
	}
	
	open override func description() -> String
	{
		return "Pop last operand from the stack, push 10 raised to the x power";
	}
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop x, determine supplementary angle (sum to 180 degrees) and push
**/

open class SupplementaryAngleOperation: CombinatoryAngleOperation {
	fileprivate static let _180 = NSDecimalNumber(value: 180 as Int);
	fileprivate static let _200 = NSDecimalNumber(value: 200 as Int);
	public static let _π_reference = PiOperation.PI;

	public required init() {
		super.init(degreeReference: SupplementaryAngleOperation._180, radiansReference: SupplementaryAngleOperation._π_reference, gradiansReference: SupplementaryAngleOperation._200);
	}
	
	open override func id() -> String {
		return "suppl";
	}

	open override func mnemonic() -> String {
		return "suppl ∠";
	}
	
	override func getPiRadiansFactor() -> String
	{
		return "π";
	}

	open override func description() -> String
	{
		return "Computes the supplementary angle (adds to 180 degrees, π radians, or 200 gradians) for X in the current units: radians, degrees or gradians.";
	}
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop x, determine complementary angle (sum to 90 degrees or π/2 radians) and push
**/

open class CombinatoryAngleOperation: BasicOperation {
	fileprivate let degreeReference: NSDecimalNumber;
	fileprivate let radiansReference: NSDecimalNumber;
	fileprivate let gradiansReference: NSDecimalNumber;

	init(degreeReference: NSDecimalNumber, radiansReference: NSDecimalNumber, gradiansReference: NSDecimalNumber)
	{
		self.degreeReference = degreeReference;
		self.radiansReference = radiansReference;
		self.gradiansReference = gradiansReference;
	}

	open override func perform() throws {
		let stack = calculator.stack();

		let angle: NSDecimalNumber! = stack.pop();

		// determine angle reference - π/2 for radians, and 90 for degrees
		var angleRef = degreeReference;

		switch (TrigBaseOperation.operationsMode(calculator))
		{
		case .degrees:
			angleRef = degreeReference;
		case .radians:
			angleRef = radiansReference;
		case .gradians:
			angleRef = gradiansReference;
		}

		// adjust for calculator decimal value
		angleRef = angleRef.decimalNumberRoundedToDigits(withDecimals: stack.getDecimals())

		// special case.  If input is ref, the answer is 0
		if (angle == angleRef)
		{
			stack.push(NSDecimalNumber.zero);
		}
		else
		{
			// mod the angle by ref
			let simpleAngle = mathHub.modulus(base: angle, modulus: angleRef);
			
			// take absolute value before subtracting
			let absAngle = simpleAngle.decimalNumberWithAbsoluteValue();
			
			// subtract from ref
			stack.push(mathHub.subtract(left: angleRef, right: absAngle));
		}
	}
	
	func getPiRadiansFactor() -> String
	{
		return "?";
	}

	open override func formula() -> String?
	{
		let val: String;

		switch(TrigBaseOperation.operationsMode(calculator))
		{
		case .degrees:
			val = String(describing: degreeReference) + "°";
		case .gradians:
			val = String(describing: gradiansReference) + "g";
		case .radians:
			val = getPiRadiansFactor() + "r";
		}

		return "<mathml>angle = <mn>" + val + "</mn> <mo>-</mo> <mo>(</mo><mo><operation>abs</operation></mo><mo>(</mo><mi><stack-x /></mi> <mo><operation>mod</operation></mo> <mn>" + val + "</mn><mo>)</mo><mo>)</mo></mathml><br />Formula stated in current trig units.";
	}
	
	open override func description() -> String
	 {
		 return "Pop x, determine complementary angle (sum to 90 degrees or π/2 radians) and push";
	 }
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop x, determine complementary angle (sum to 90 degrees or π/2 radians) and push
**/

open class ComplementaryAngleOperation: CombinatoryAngleOperation {
	fileprivate static let _90 = NSDecimalNumber(value: 90 as Int);
	fileprivate static let _100 = NSDecimalNumber(value: 100 as Int);
	public static let _π_reference = PiOperation.PI.dividing(by: NSDecimalNumber(value: 2 as Int));

	public init() {
		super.init(degreeReference: ComplementaryAngleOperation._90, radiansReference: ComplementaryAngleOperation._π_reference, gradiansReference: ComplementaryAngleOperation._100);
	}
	
	open override func id() -> String {
		return "compl";
	}

	open override func mnemonic() -> String {
		return "compl ∠";
	}
	
	override func getPiRadiansFactor() -> String
	{
		return "π/2";
	}

	open override func description() -> String
	{
		return "Computes the complementary angle (adds to 90 degrees, π/2 radians, or 100 gradians) for X in the current units: radians, degrees or gradians.";
	}
}

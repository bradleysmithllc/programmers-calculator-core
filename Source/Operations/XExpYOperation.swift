//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last two operands from the stack, push x raised to the y power.
**/

open class XExpYOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.pop();
		let y: NSDecimalNumber! = stack.pop();

		stack.setX(mathHub.power(base: x, exponent: y));
	}
	
	open override func id() -> String {
		return "x^y";
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><msup><mi>x</mi><mi>y</mi></msup></math>";
	}
	
	open override func mnemonic() -> String {
		return "x{y}";
	}
	
	open override func operationType() -> OperationType
	{
		return .binary;
	}
	
	open override func description() -> String
	{
		return "Pop last two operands from the stack, push x raised to the y power";
	}
}

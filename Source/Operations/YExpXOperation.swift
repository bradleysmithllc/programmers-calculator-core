//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last two operands from the stack, push y raised to the x power.
**/

open class YExpXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.pop();
		let y: NSDecimalNumber! = stack.pop();

		stack.setX(mathHub.power(base: y, exponent: x));
	}
	
	open override func id() -> String {
		return "y^x";
	}

	open override func decoratedMnemonic() -> String {
		return "<math><msup><mi>y</mi><mi>x</mi></msup></math>";
	}
	
	open override func mnemonic() -> String {
		return "y{x}";
	}
	
	open override func operationType() -> OperationType
	{
		return .binary;
	}
	
	open override func description() -> String
	{
		return "Pop last two operands from the stack, push y raised to the x power";
	}
}

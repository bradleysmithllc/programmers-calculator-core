//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last operand from the stack, push x raised to the 1/3rd power.
**/

open class CubeRootXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.pop();

		stack.push(NSDecimalNumber(value: cbrt(x.doubleValue) as Double));
	}
	
	open override func id() -> String {
		return "cbrtx";
	}

	open override func mnemonic() -> String {
		return "{3}√x";
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><mroot><mi>x</mi><mn>3</mn></mroot></math>";
	}
	
	open override func description() -> String
	 {
		 return "Cube root of X";
	 }
}

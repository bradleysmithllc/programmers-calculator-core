//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
  * Flip the sign of the X register
  **/

open class ChangeSignXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		// there are two modes here
		let inNumericEntry = stack.inNumericEntry();

		// 1 - If in numeric entry, simply set the sign bit in the stack.
		if (inNumericEntry)
		{
			stack.flipNumericEntrySignBit();
		}
		else
		{
			// 2 - Otherwise, simply negate the X register
			let currX: NSDecimalNumber = stack.getX();
			stack.setX(currX.decimalNumberByNegating());
		}
	}

	public final override func registerResultState() -> NumericEntryState
	{
		if stack.inNumericEntry()
		{
			return NumericEntryState.in_NUMERIC_ENTRY;
		}
		else
		{
			return NumericEntryState.waiting_FOR_OPERATION;
		}
	}

	open override func id() -> String {
		return "chsx";
	}
	
	open override func mnemonic() -> String {
		return "±";
	}
	
	open override func validateStack() -> Bool {
		return stack.getNumericBehavior() != .UnsignedInteger;
	}
	
	open override func description() -> String
	 {
		 return "Flip the sign of the X register";
	 }
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pops A, then B, then C from the stack.  Attempts to factor into whole integers.
* If successful, pushes in order J, K, L, M. If not, pushes NaN.
**/

open class FactorQuadraticOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let c: Int = stack.pop().intValue;
		let b: Int = stack.pop().intValue;
		let a: Int = stack.pop().intValue;

		var solCount = 0;
		
		// special cases to avoid ridiculous solution sets
		if (c == 0 && b == 0 && a == 0)
		{
			stack.push("(0x + 0) · (0x + 0)");
			return;
		}

		/*Used to eliminate redundant solutions, E.G., (ab)*(cd) and (cd)*(ab)*/
		var solMap = [String]();

		/** Form is (Jx + K)(Lx + M)*/
		for j: Int in -10...10
		{
			for k: Int in -10...10
			{
				for l: Int in -10...10
				{
					for m: Int in -10...10
					{
						let a0 = j * l;
						let b0 = (j * m) + (k * l);
						let c0 = k * m;

						if (a == a0 && b == b0 && c == c0 && !solMap.contains("\(j) \(k) \(l) \(m)"))
						{
							solMap.append("\(j) \(k) \(l) \(m)");
							solMap.append("\(l) \(m) \(j) \(k)");

							// found solution. push
							stack.push("(\(a_format(j))x \(k > 0 ? "+" : "-") \(abs(k))) · (\(a_format(l))x \(m > 0 ? "+" : "-") \(abs(m)))");
							solCount += 1;
						}
					}
				}
			}
		}

		if (solCount == 0)
		{
			stack.push("No Solution");
		}
	}
	
	fileprivate func a_format(_ val: Int) -> String
	{
		if (val == 1)
		{
			return "";
		}
		else if (val == -1)
		{
			return "-";
		}
		else
		{
			return String(val);
		}
	}

	open override func id() -> String {
		return "fact_quad";
	}

	open override func mnemonic() -> String {
		return "factor";
	}
	
	open override func description() -> String
	 {
		 return "Pops A, then B, then C from the stack.  Attempts to factor into whole integers.  If successful, pushes in order J, K, L, M. If not, pushes NaN";
	 }
}

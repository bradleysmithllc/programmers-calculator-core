//
//  ComplexOperation.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/27/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**Operation consisting of many steps.
  */
open class CompoundOperation: BasicOperation
{
	fileprivate let mnemonicName: String;
	fileprivate let _description: String;
	fileprivate let _formula: String?;

	fileprivate let _id: String;
	fileprivate var _operations: [String] = [String]();
	
	public init(id: String, mnemonic: String, description: String, formula: String?)
	{
		mnemonicName = mnemonic;
		_id = id;

		_description = description;
		_formula = formula;
	}

	open func operations() -> [String]
	{
		return _operations;
	}

	open func addOperation(_ name: String)
	{
		_operations.append(name);
	}

	open override func perform() throws
	{
		// run through all mnemonics in order
		for mne in _operations
		{
			//print("Operation: \(_id).\(mne)");
			try calculator.invoke(mne);
			//print("Result: X: \(calculator.stack().getX()) Y: \(calculator.stack().getY()) depth: \(calculator.stack().size())");
		}
	}
	
	open override func id() -> String {
		return _id;
	}
	
	open override func mnemonic() -> String {
		return mnemonicName;
	}
	
	open override func formula() -> String?
	{
		return _formula;
	}
	
	open override func description() -> String
	{
		return _description;
	}
}

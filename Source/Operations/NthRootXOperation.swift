//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last operand from the stack, push x raised to the 1/y power.
**/

open class NthRootXOperation: BasicOperation {
	fileprivate static let ONE_OVER_TWO: NSDecimalNumber = NSDecimalNumber(value: 0.5 as Double);

	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.pop();
		let y: NSDecimalNumber! = stack.pop();

		stack.push(mathHub.power(base: x, exponent: mathHub.divide(dividend: NSDecimalNumber.one, divisor: y)));
	}

	open override func id() -> String {
		return "yrtx";
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><mroot><mi>x</mi><mi>y</mi></mroot></math>";
	}

	open override func mnemonic() -> String {
		return "{y}√x";
	}
	
	open override func description() -> String
	 {
		 return "Pop last operand from the stack, push x raised to the 1/y power";
	 }
}

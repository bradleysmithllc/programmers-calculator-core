//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Removes the integer portion of the number
**/

open class TruncateIntegerOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let val: NSDecimalNumber! = stack.pop();

		let integerPortion = val.decimalNumberRoundedToDigits(withDecimals: 0, roundingMode: NSDecimalNumber.RoundingMode.down);
		let frac = mathHub.subtract(left: val, right: integerPortion);

		stack.push(frac);
	}
	
	open override func validateStack() -> Bool {
		return stack.getNumericBase() == NumericBase.base10;
	}
	
	open override func id() -> String {
		return "fracx";
	}

	open override func mnemonic() -> String {
		return "fracx";
	}
	
	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Push];
	}
	
	open override func description() -> String
	{
		return "Truncates integral portion.  E.G., 5.2 becomes 0.2";
	}
	
	open override func operationType() -> OperationType
	{
		return .unary;
	}
}

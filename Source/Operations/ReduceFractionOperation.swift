//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Given the fraction Y/X, reduce the numerator and denominator to primes
* then cancel common factors
**/

open class ReduceFractionOperation: BasicOperation {
	fileprivate static let primes: [NSDecimalNumber] = [
		NSDecimalNumber(value: 2 as Int32), NSDecimalNumber(value: 3 as Int32), NSDecimalNumber(value: 5 as Int32),
		NSDecimalNumber(value: 7 as Int32), NSDecimalNumber(value: 11 as Int32), NSDecimalNumber(value: 13 as Int32),
		NSDecimalNumber(value: 17 as Int32), NSDecimalNumber(value: 19 as Int32), NSDecimalNumber(value: 23 as Int32),
		NSDecimalNumber(value: 29 as Int32), NSDecimalNumber(value: 31 as Int32), NSDecimalNumber(value: 37 as Int32),
		NSDecimalNumber(value: 41 as Int32), NSDecimalNumber(value: 43 as Int32), NSDecimalNumber(value: 47 as Int32),
		NSDecimalNumber(value: 53 as Int32), NSDecimalNumber(value: 59 as Int32), NSDecimalNumber(value: 61 as Int32),
		NSDecimalNumber(value: 67 as Int32), NSDecimalNumber(value: 71 as Int32), NSDecimalNumber(value: 73 as Int32),
		NSDecimalNumber(value: 79 as Int32), NSDecimalNumber(value: 83 as Int32), NSDecimalNumber(value: 89 as Int32),
		NSDecimalNumber(value: 97 as Int32), NSDecimalNumber(value: 101 as Int32), NSDecimalNumber(value: 103 as Int32),
		NSDecimalNumber(value: 107 as Int32), NSDecimalNumber(value: 109 as Int32), NSDecimalNumber(value: 113 as Int32),
		NSDecimalNumber(value: 121 as Int32), NSDecimalNumber(value: 127 as Int32), NSDecimalNumber(value: 131 as Int32),
		NSDecimalNumber(value: 137 as Int32), NSDecimalNumber(value: 139 as Int32), NSDecimalNumber(value: 143 as Int32),
		NSDecimalNumber(value: 149 as Int32), NSDecimalNumber(value: 151 as Int32), NSDecimalNumber(value: 157 as Int32),
		NSDecimalNumber(value: 163 as Int32), NSDecimalNumber(value: 167 as Int32), NSDecimalNumber(value: 169 as Int32),
		NSDecimalNumber(value: 173 as Int32), NSDecimalNumber(value: 179 as Int32), NSDecimalNumber(value: 181 as Int32),
		NSDecimalNumber(value: 187 as Int32), NSDecimalNumber(value: 191 as Int32), NSDecimalNumber(value: 193 as Int32),
		NSDecimalNumber(value: 197 as Int32), NSDecimalNumber(value: 199 as Int32), NSDecimalNumber(value: 209 as Int32),
		NSDecimalNumber(value: 211 as Int32), NSDecimalNumber(value: 221 as Int32), NSDecimalNumber(value: 223 as Int32),
		NSDecimalNumber(value: 227 as Int32), NSDecimalNumber(value: 229 as Int32), NSDecimalNumber(value: 233 as Int32),
		NSDecimalNumber(value: 239 as Int32), NSDecimalNumber(value: 241 as Int32), NSDecimalNumber(value: 247 as Int32),
		NSDecimalNumber(value: 251 as Int32), NSDecimalNumber(value: 253 as Int32), NSDecimalNumber(value: 257 as Int32),
		NSDecimalNumber(value: 263 as Int32), NSDecimalNumber(value: 269 as Int32), NSDecimalNumber(value: 271 as Int32),
		NSDecimalNumber(value: 277 as Int32), NSDecimalNumber(value: 281 as Int32), NSDecimalNumber(value: 283 as Int32),
		NSDecimalNumber(value: 289 as Int32), NSDecimalNumber(value: 293 as Int32), NSDecimalNumber(value: 299 as Int32),
		NSDecimalNumber(value: 307 as Int32), NSDecimalNumber(value: 311 as Int32), NSDecimalNumber(value: 313 as Int32),
		NSDecimalNumber(value: 317 as Int32), NSDecimalNumber(value: 319 as Int32), NSDecimalNumber(value: 323 as Int32),
		NSDecimalNumber(value: 331 as Int32), NSDecimalNumber(value: 337 as Int32), NSDecimalNumber(value: 341 as Int32),
		NSDecimalNumber(value: 347 as Int32), NSDecimalNumber(value: 349 as Int32), NSDecimalNumber(value: 353 as Int32),
		NSDecimalNumber(value: 359 as Int32), NSDecimalNumber(value: 361 as Int32), NSDecimalNumber(value: 367 as Int32),
		NSDecimalNumber(value: 373 as Int32), NSDecimalNumber(value: 377 as Int32), NSDecimalNumber(value: 379 as Int32),
		NSDecimalNumber(value: 383 as Int32), NSDecimalNumber(value: 389 as Int32), NSDecimalNumber(value: 391 as Int32),
		NSDecimalNumber(value: 397 as Int32), NSDecimalNumber(value: 401 as Int32), NSDecimalNumber(value: 403 as Int32),
		NSDecimalNumber(value: 407 as Int32), NSDecimalNumber(value: 409 as Int32), NSDecimalNumber(value: 419 as Int32),
		NSDecimalNumber(value: 421 as Int32), NSDecimalNumber(value: 431 as Int32), NSDecimalNumber(value: 433 as Int32),
		NSDecimalNumber(value: 437 as Int32), NSDecimalNumber(value: 439 as Int32), NSDecimalNumber(value: 443 as Int32),
		NSDecimalNumber(value: 449 as Int32), NSDecimalNumber(value: 451 as Int32), NSDecimalNumber(value: 457 as Int32),
		NSDecimalNumber(value: 461 as Int32), NSDecimalNumber(value: 463 as Int32), NSDecimalNumber(value: 467 as Int32),
		NSDecimalNumber(value: 473 as Int32), NSDecimalNumber(value: 479 as Int32), NSDecimalNumber(value: 481 as Int32),
		NSDecimalNumber(value: 487 as Int32), NSDecimalNumber(value: 491 as Int32), NSDecimalNumber(value: 493 as Int32),
		NSDecimalNumber(value: 499 as Int32), NSDecimalNumber(value: 503 as Int32), NSDecimalNumber(value: 509 as Int32),
		NSDecimalNumber(value: 517 as Int32), NSDecimalNumber(value: 521 as Int32), NSDecimalNumber(value: 523 as Int32),
		NSDecimalNumber(value: 527 as Int32), NSDecimalNumber(value: 529 as Int32), NSDecimalNumber(value: 533 as Int32),
		NSDecimalNumber(value: 541 as Int32), NSDecimalNumber(value: 547 as Int32), NSDecimalNumber(value: 551 as Int32),
		NSDecimalNumber(value: 557 as Int32), NSDecimalNumber(value: 559 as Int32), NSDecimalNumber(value: 563 as Int32),
		NSDecimalNumber(value: 569 as Int32), NSDecimalNumber(value: 571 as Int32), NSDecimalNumber(value: 577 as Int32),
		NSDecimalNumber(value: 583 as Int32), NSDecimalNumber(value: 587 as Int32), NSDecimalNumber(value: 589 as Int32),
		NSDecimalNumber(value: 593 as Int32), NSDecimalNumber(value: 599 as Int32), NSDecimalNumber(value: 601 as Int32),
		NSDecimalNumber(value: 607 as Int32), NSDecimalNumber(value: 611 as Int32), NSDecimalNumber(value: 613 as Int32),
		NSDecimalNumber(value: 617 as Int32), NSDecimalNumber(value: 619 as Int32), NSDecimalNumber(value: 629 as Int32),
		NSDecimalNumber(value: 631 as Int32), NSDecimalNumber(value: 641 as Int32), NSDecimalNumber(value: 643 as Int32),
		NSDecimalNumber(value: 647 as Int32), NSDecimalNumber(value: 649 as Int32), NSDecimalNumber(value: 653 as Int32),
		NSDecimalNumber(value: 659 as Int32), NSDecimalNumber(value: 661 as Int32), NSDecimalNumber(value: 667 as Int32),
		NSDecimalNumber(value: 671 as Int32), NSDecimalNumber(value: 673 as Int32), NSDecimalNumber(value: 677 as Int32),
		NSDecimalNumber(value: 683 as Int32), NSDecimalNumber(value: 689 as Int32), NSDecimalNumber(value: 691 as Int32),
		NSDecimalNumber(value: 697 as Int32), NSDecimalNumber(value: 701 as Int32), NSDecimalNumber(value: 703 as Int32),
		NSDecimalNumber(value: 709 as Int32), NSDecimalNumber(value: 713 as Int32), NSDecimalNumber(value: 719 as Int32),
		NSDecimalNumber(value: 727 as Int32), NSDecimalNumber(value: 731 as Int32), NSDecimalNumber(value: 733 as Int32),
		NSDecimalNumber(value: 737 as Int32), NSDecimalNumber(value: 739 as Int32), NSDecimalNumber(value: 743 as Int32),
		NSDecimalNumber(value: 751 as Int32), NSDecimalNumber(value: 757 as Int32), NSDecimalNumber(value: 761 as Int32),
		NSDecimalNumber(value: 767 as Int32), NSDecimalNumber(value: 769 as Int32), NSDecimalNumber(value: 773 as Int32),
		NSDecimalNumber(value: 779 as Int32), NSDecimalNumber(value: 781 as Int32), NSDecimalNumber(value: 787 as Int32),
		NSDecimalNumber(value: 793 as Int32), NSDecimalNumber(value: 797 as Int32), NSDecimalNumber(value: 799 as Int32),
		NSDecimalNumber(value: 803 as Int32), NSDecimalNumber(value: 809 as Int32), NSDecimalNumber(value: 811 as Int32),
		NSDecimalNumber(value: 817 as Int32), NSDecimalNumber(value: 821 as Int32), NSDecimalNumber(value: 823 as Int32),
		NSDecimalNumber(value: 827 as Int32), NSDecimalNumber(value: 829 as Int32), NSDecimalNumber(value: 839 as Int32),
		NSDecimalNumber(value: 841 as Int32), NSDecimalNumber(value: 851 as Int32), NSDecimalNumber(value: 853 as Int32),
		NSDecimalNumber(value: 857 as Int32), NSDecimalNumber(value: 859 as Int32), NSDecimalNumber(value: 863 as Int32),
		NSDecimalNumber(value: 869 as Int32), NSDecimalNumber(value: 871 as Int32), NSDecimalNumber(value: 877 as Int32),
		NSDecimalNumber(value: 881 as Int32), NSDecimalNumber(value: 883 as Int32), NSDecimalNumber(value: 887 as Int32),
		NSDecimalNumber(value: 893 as Int32), NSDecimalNumber(value: 899 as Int32), NSDecimalNumber(value: 901 as Int32),
		NSDecimalNumber(value: 907 as Int32), NSDecimalNumber(value: 911 as Int32), NSDecimalNumber(value: 913 as Int32),
		NSDecimalNumber(value: 919 as Int32), NSDecimalNumber(value: 923 as Int32), NSDecimalNumber(value: 929 as Int32),
		NSDecimalNumber(value: 937 as Int32), NSDecimalNumber(value: 941 as Int32), NSDecimalNumber(value: 943 as Int32),
		NSDecimalNumber(value: 947 as Int32), NSDecimalNumber(value: 949 as Int32), NSDecimalNumber(value: 953 as Int32),
		NSDecimalNumber(value: 961 as Int32), NSDecimalNumber(value: 967 as Int32), NSDecimalNumber(value: 971 as Int32),
		NSDecimalNumber(value: 977 as Int32), NSDecimalNumber(value: 979 as Int32), NSDecimalNumber(value: 983 as Int32),
		NSDecimalNumber(value: 989 as Int32), NSDecimalNumber(value: 991 as Int32), NSDecimalNumber(value: 997 as Int32)
	];

	open override func perform() throws {
		let stack = calculator.stack();

		let denominator: NSDecimalNumber = stack.pop().decimalNumberTruncated();
		let numerator: NSDecimalNumber = stack.pop().decimalNumberTruncated();

		// special case
		if denominator == 0
		{
			stack.push(NSDecimalNumber.notANumber);
			stack.push(NSDecimalNumber.notANumber);
		}
		else if numerator == 0
		{
			stack.push(NSDecimalNumber.zero);
			stack.push(NSDecimalNumber.zero);
		}
		// Special case. Check if the denominator is a multiple of the numerator
		else if mathHub.modulus(base: denominator, modulus: numerator) == 0
		{
			// denominator is a multiple of the numerator E.G., 2/4.  Reduce and stop
			stack.push(NSDecimalNumber.one);
			stack.push(mathHub.divide(dividend: denominator, divisor: numerator));
		}
		else
		{
			var numFactors = ReduceFractionOperation.factor(numerator, mathHub: mathHub);
			var denomFactors = ReduceFractionOperation.factor(denominator, mathHub: mathHub);
			
			ReduceFractionOperation.cancelCommonFactors(&numFactors.factors, denomFactors: &denomFactors.factors);
			
			stack.push(ReduceFractionOperation.reconstitute(numFactors, mathHub: mathHub));
			stack.push(ReduceFractionOperation.reconstitute(denomFactors, mathHub: mathHub));
		}
	}

	public static func cancelCommonFactors(_ numFactors: inout [NSDecimalNumber], denomFactors: inout [NSDecimalNumber])
	{
		let factors = numFactors

		for numeratorFactor in factors
		{
			let dindex = denomFactors.firstIndex(of: numeratorFactor);

			if let sindex = dindex
			{
				// remove first one from both sides
				denomFactors.remove(at: sindex);

				// must be found
				let nindex = numFactors.firstIndex(of: numeratorFactor);
				numFactors.remove(at: nindex!);
			}
		}
	}
	
	public static func reconstitute(_ result: (factors: [NSDecimalNumber], remainder: NSDecimalNumber), mathHub: MathHub) ->NSDecimalNumber
	{
		var total = result.remainder != NSDecimalNumber.zero ? result.remainder :  NSDecimalNumber.one;
		
		for factor in result.factors
		{
			total = mathHub.multiply(left: total, right: factor);
		}

		return total;
	}

	public static func factor(_ input: NSDecimalNumber, mathHub: MathHub) -> (factors: [NSDecimalNumber], remainder: NSDecimalNumber)
	{
		// run through the list of primes, and if a factor is found push it into the list of factors
		// and reduce the number.  Continue until a complete pass is made without a factor being found.
		var factors = [NSDecimalNumber]();
		var remainder = input;

		var continueSearch: Bool;

		repeat
		{
			continueSearch = false;

			if (remainder.int64Value > 1)
			{
				for prime in ReduceFractionOperation.primes
				{
					// first check if this prime number IS this number
					if (prime == remainder)
					{
						factors.append(prime);
						remainder = NSDecimalNumber.zero;
						break;
					}
					else if mathHub.modulus(base: remainder, modulus: prime) == 0
					{
						continueSearch = true;
						// this is a prime factor
						factors.append(prime);

						// reduce the remainder
						remainder = mathHub.divide(dividend: remainder, divisor: prime);
					}
				}
			}
		}
		while (continueSearch)

		// sort the list before returning
		factors = factors.sorted(){$0.compare($1) == .orderedAscending};

		return (factors: factors, remainder: remainder);
	}

	open override func id() -> String {
		return "reduce";
	}

	open override func mnemonic() -> String {
		return "↓y/x";
	}
	
	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Pop, .Push, .Push];
	}
	
	open override func formula() -> String?
	{
		return "Given the fraction intx(Y)/intx(X), fully simplify and push the numerator into Y and the denominator into X";
	}
	
	open override func description() -> String
	{
		return "Factor the numerator and denominator into primes, then cancel common factors.  The inputs are treated as integers only for this operation.";
	}
}

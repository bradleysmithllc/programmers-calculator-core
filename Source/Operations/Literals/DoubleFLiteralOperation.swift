//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
 * Pushes a literal to the right side of the number in the x register.
 **/

open class DoubleFLiteralOperation: BasicOperation {
	open override func perform() throws {
		try stack.addDigitToNumericEntry(LiteralDigit._15);
		try stack.addDigitToNumericEntry(LiteralDigit._15);
	}

	public final override func registerResultState() -> NumericEntryState
	{
		return NumericEntryState.in_NUMERIC_ENTRY;
	}

	open override func validateStack() -> Bool {
		return BaseLiteralOperation.validateStack(calculator.stack(), doubleDigit: true, digitToCheck: LiteralDigit._15);
	}

	open override func id() -> String {
		return "FF";
	}

	open override func mnemonic() -> String {
		return "FF";
	}
	
	open override func description() -> String
	{
		return "Literal FF as a double-digit";
	}

	open override func operationType() -> OperationType
	{
		return .literal;
	}
}

//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
 * Pushes a literal to the right side of the number in the x register.
 **/

open class NineLiteralOperation: BaseLiteralOperation {
	public required init() {
		super.init(value: LiteralDigit._9);
	}
	
	open override func description() -> String
	{
		return "Literal digit 9";
	}
}

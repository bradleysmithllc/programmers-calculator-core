//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation;

/**
 * Pushes a literal to the right side of the number in the x register.
 **/

open class BaseLiteralOperation: BasicOperation {
	fileprivate static let negativeOne: NSDecimalNumber = NSDecimalNumber(value: -1 as Int);

	fileprivate let value: LiteralDigit;
	fileprivate let _mnemonic: String;

	init(value v: LiteralDigit) {
		let mnemonic: String;

		switch (v) {
			case ._0:
				mnemonic = "0";
			case ._1:
				mnemonic = "1";
			case ._2:
				mnemonic = "2";
			case ._3:
				mnemonic = "3";
			case ._4:
				mnemonic = "4";
			case ._5:
				mnemonic = "5";
			case ._6:
				mnemonic = "6";
			case ._7:
				mnemonic = "7";
			case ._8:
				mnemonic = "8";
			case ._9:
				mnemonic = "9";
			case ._10:
				mnemonic = "A";
			case ._11:
				mnemonic = "B";
			case ._12:
				mnemonic = "C";
			case ._13:
				mnemonic = "D";
			case ._14:
				mnemonic = "E";
			case ._15:
				mnemonic = "F";
			case .decimalPoint:
				mnemonic = ".";
		}

		value = v;
		_mnemonic = mnemonic;
	}

	init(value v: LiteralDigit, mnemonic: String) {
		value = v;
		_mnemonic = mnemonic;
	}

	public final override func registerResultState() -> NumericEntryState
	{
		return NumericEntryState.in_NUMERIC_ENTRY;
	}

	open override func validateStack() -> Bool {
		return BaseLiteralOperation.validateStack(calculator.stack(), doubleDigit: false, digitToCheck: value);
	}

	public static func validateStack(_ stack: Stack, doubleDigit: Bool, digitToCheck: LiteralDigit) -> Bool {
		var digitsToAllow: UInt8;
		
		// reasonable allowances for different bases
		if (stack.getNumericBase() == NumericBase.base16)
		{
			// constrain to the word size
			switch(stack.getIntegerWordSize())
			{
			case .none:
				fatalError();
			case .byte:
				digitsToAllow = 2;
			case .word_2:
				digitsToAllow = 4;
			case .word_4:
				digitsToAllow = 8;
			case .word_8:
				digitsToAllow = 16;
			}
		}
		else if (stack.getNumericBase() == NumericBase.base10)
		{
			// constrain to the word size.  These are approximate - still possible to enter greater than max
			switch(stack.getIntegerWordSize())
			{
			case .none:
				digitsToAllow = 38;
			case .byte:
				digitsToAllow = 3;
			case .word_2:
				digitsToAllow = 5;
			case .word_4:
				digitsToAllow = 10;
			case .word_8:
				digitsToAllow = 20;
			}
		}
		else if (stack.getNumericBase() == NumericBase.base8)
		{
			// constrain to the word size.  These are approximate - still possible to enter greater than max
			switch(stack.getIntegerWordSize())
			{
			case .none:
				fatalError();
			case .byte:
				digitsToAllow = 3;
			case .word_2:
				digitsToAllow = 6;
			case .word_4:
				digitsToAllow = 11;
			case .word_8:
				digitsToAllow = 22;
			}
		}
		else //if (stack.getNumericBase() == 2)
		{
			// constrain to the word size
			switch(stack.getIntegerWordSize())
			{
			case .none:
				fatalError();
			case .byte:
				digitsToAllow = 8;
			case .word_2:
				digitsToAllow = 16;
			case .word_4:
				digitsToAllow = 32;
			case .word_8:
				digitsToAllow = 64;
			}
		}

		if doubleDigit
		{
			digitsToAllow -= 1;
		}

		return BaseLiteralOperation.validateStack(stack, digitsToAllow: digitsToAllow, digitToCheck: digitToCheck);
	}

	public static func validateStack(_ stack: Stack, digitsToAllow: UInt8, digitToCheck: LiteralDigit) -> Bool {
		let currentInput = stack.currentInputBuffer();

		// rule here is number of integer digits + number of decimal digits can't exceed digitsToAllow
		let digitCount = currentInput.integralBuffer().count + currentInput.decimalBuffer().count;

		// early fail
		if (digitCount >= digitsToAllow) {
			return false;
		}

		// next validate that this digit is allowed in this numeric base
		switch (digitToCheck) {
			// 0 and 1 are always good
			case ._0:
				return true;
			case ._1:
				return true;

				// 2-7 are available as long as we are not in base2
			case ._2:
				return stack.getNumericBase() != NumericBase.base2;
			case ._3:
				return stack.getNumericBase() != NumericBase.base2;
			case ._4:
				return stack.getNumericBase() != NumericBase.base2;
			case ._5:
				return stack.getNumericBase() != NumericBase.base2;
			case ._6:
				return stack.getNumericBase() != NumericBase.base2;
			case ._7:
				return stack.getNumericBase() != NumericBase.base2;

			// similarly, 8, 9 are available if the numeric base is base 10 or base 16
			case ._8:
				return stack.getNumericBase() == NumericBase.base10 || stack.getNumericBase() == NumericBase.base16;
			case ._9:
				return stack.getNumericBase() == NumericBase.base10 || stack.getNumericBase() == NumericBase.base16;

			// remaining cases are only allowed for base 16
			case ._10:
				return stack.getNumericBase() == NumericBase.base16;
			case ._11:
				return stack.getNumericBase() == NumericBase.base16;
			case ._12:
				return stack.getNumericBase() == NumericBase.base16;
			case ._13:
				return stack.getNumericBase() == NumericBase.base16;
			case ._14:
				return stack.getNumericBase() == NumericBase.base16;
			case ._15:
				return stack.getNumericBase() == NumericBase.base16;
			case .decimalPoint:
				// only allowed if there is no decimal point yet
				return !currentInput.hasDecimal();
		}
	}

	open override func perform() throws {
		// just append to the numeric entry
		try stack.addDigitToNumericEntry(value);
	}
	
	open override func id() -> String {
		return _mnemonic;
	}
	
	open override func mnemonic() -> String {
		return _mnemonic;
	}

	open override func operationType() -> OperationType
	{
		return .literal;
	}
}

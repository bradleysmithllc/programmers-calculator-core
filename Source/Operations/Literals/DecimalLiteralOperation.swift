//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
 * Inserts the decimal point in the numeric input.
 **/
open class DecimalLiteralOperation: BaseLiteralOperation {
	public required init() {
		super.init(value: LiteralDigit.decimalPoint);
	}

	open override func validateStack() -> Bool {
		let numericBehaviorIsDecimal = stack.getNumericBehavior() == .Decimal
		let numericBaseIs10 = stack.getNumericBase() == NumericBase.base10
		let inputBufferDoesNotIncludeDecimal = !stack.currentInputBufferIncludesDecimal()
		let stackAllowsDecimals = stack.getDecimals() > 0
		
		return numericBehaviorIsDecimal && numericBaseIs10 && inputBufferDoesNotIncludeDecimal && stackAllowsDecimals;
	}
	
	open override func id() -> String {
		return ".";
	}

	open override func description() -> String
	{
		return "Literal decimal point";
	}
}

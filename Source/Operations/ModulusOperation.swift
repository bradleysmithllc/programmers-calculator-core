//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* y mod x
**/

open class ModulusOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let mod: NSDecimalNumber! = stack.pop();
		let base: NSDecimalNumber! = stack.getX();

		if (mod == NSDecimalNumber.zero) {
			stack.setX(NSDecimalNumber.notANumber);
		} else {
			let quotient: NSDecimalNumber = mathHub.divide(dividend: base, divisor: mod);

			let integralQuotient: NSDecimalNumber = NSDecimalNumber(value: quotient.int32Value as Int32);

			let subtractAmount: NSDecimalNumber = mathHub.multiply(left: integralQuotient, right: mod);
			let remainder: NSDecimalNumber = mathHub.subtract(left: base, right: subtractAmount);

			stack.setX(remainder);
		}
	}
	
	open override func id() -> String {
		return "ymodx";
	}

	open override func mnemonic() -> String {
		return "ymodx";
	}
	
	open override func description() -> String
	{
		return "Modulo.  Calculates the integer remainder of dividing X by Y";
	}
}

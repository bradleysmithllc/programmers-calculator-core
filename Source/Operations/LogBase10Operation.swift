//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* y mod x
**/

open class LogBase10Operation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let mod: NSDecimalNumber = stack.pop();

		stack.push(NSDecimalNumber(value: log10(mod.doubleValue) as Double));
	}
	
	open override func id() -> String {
		return "log10x";
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><msub><mi>log</mi><mn>10</mn></msub></math>";
	}
	
	open override func mnemonic() -> String {
		return "log[10]";
	}
	
	open override func description() -> String
	{
		return "Base 10 Logarithm";
	}
}

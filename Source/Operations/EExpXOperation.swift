//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Pop last two operands from the stack, push y raised to the x power.
**/

open class EExpXOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let x: NSDecimalNumber! = stack.pop();

		stack.push(mathHub.power(base: EOperation.E, exponent: x));
	}
	
	open override func id() -> String {
		return "e^x";
	}
	
	open override func decoratedMnemonic() -> String {
		return "<math><msup><mi>e</mi><mn>x</mn></msup></math>";
	}

	open override func mnemonic() -> String {
		return "e{x}";
	}
	
	open override func description() -> String
	 {
		 return "Pop last two operands from the stack, push y raised to the x power";
	 }
}

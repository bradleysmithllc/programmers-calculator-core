//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* Rounds a number to a specified number of digits
**/

open class RoundOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let digits: NSDecimalNumber! = stack.pop();
		let value: NSDecimalNumber! = stack.pop();

		// if the requested number of digits is greater than or equal to the current decimal
		// count, do nothing
		let requestedDigits = digits.safeUnsignedByteValue;

		let newy: NSDecimalNumber;
		
		if (requestedDigits < stack.getDecimals())
		{
			newy = value.decimalNumberRoundedToDigits(withDecimals: requestedDigits, roundingMode: NSDecimalNumber.RoundingMode.plain);
		}
		else
		{
			newy = value
		}
		
		stack.push(newy);
	}

	open override func validateStack() -> Bool {
		return stack.getNumericBase() == NumericBase.base10;
	}

	open override func id() -> String {
		return "rndy";
	}

	open override func mnemonic() -> String {
		return "rndy";
	}
	
	open override func stackAffect() -> [StackAffect]?
	{
		return [.Pop, .Pop, .Push];
	}
	
	open override func description() -> String
	{
		return "Rounds Y register to X number of decimals";
	}
	
	open override func operationType() -> OperationType
	{
		return .binary;
	}
}

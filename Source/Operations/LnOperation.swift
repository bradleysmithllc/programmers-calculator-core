//
//  Addition.swift
//  CalculatorCore
//
//  Created by Bradley Smith on 10/13/15.
//  Copyright © 2015 Bradley Smith, LLC. All rights reserved.
//

import Foundation

/**
* y mod x
**/

open class LnOperation: BasicOperation {
	open override func perform() throws {
		let stack = calculator.stack();

		let mod: NSDecimalNumber = stack.pop();

		stack.push(NSDecimalNumber(value: log(mod.doubleValue) as Double));
	}
	
	open override func id() -> String {
		return "lnx";
	}

	open override func mnemonic() -> String {
		return "ln";
	}
	
	open override func description() -> String
	{
		return "Natural Logarithm";
	}
}
